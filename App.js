// Import libraries to create a Component
import { View, Text, LogBox } from 'react-native';
import Config from 'react-native-config';
import React, { Component } from 'react';

//Async Storage Library
import AsyncStorage from '@react-native-community/async-storage';

// Navigation
import NavigationService from './src/navigation/navigationService';

// Constant
import RootContainer from './src/pages/RootContainer';

// Redux and Redux Thunk Libraries
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/es/integration/react';
import { store, persistor } from './src/config/ReduxStore';

// Jail Monkey
import JailMonkey from 'jail-monkey';

import 'react-native-gesture-handler';


// Ignore outdated warnings of react native
LogBox.ignoreAllLogs()

// Condition to remove the console logs in release mode
if (!__DEV__) {
  console.log();
  if (!window.console) window.console = {};
  var methods = ['log', 'debug', 'warn', 'info', 'table'];
  for (var i = 0; i < methods.length; i++) {
    console[methods[i]] = function () { };
  }
}

Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;

// Create a App Component
export default class App extends Component {
  constructor(properties) {
    super(properties);
    this.state = {
      isJailBroken: false,
    };
  }

  // Component Lifecycle
  componentDidMount = async () => {
    console.log(process.env);
    console.log(Config.ENV);
    // if (JailMonkey.isJailBroken()) {
    //   this.setState({ isJailBroken: true });
    // } else {

    // }
  };

  render() {
    return (
      <React.Fragment>
        {this.state.isJailBroken ? (
          <View style={{}}>
            <Text>Jailbroken</Text>
          </View>
        ) : (
          <Provider store={store}>
            <PersistGate loading={null} persistor={persistor}>
              <RootContainer />
            </PersistGate>
          </Provider>
        )}
      </React.Fragment>
    );
  }
}