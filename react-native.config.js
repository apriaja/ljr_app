module.exports = {
    assets: [
        "./src/assets/fonts/Roboto",
        "./src/assets/fonts/NotoSans",
        "./src/assets/fonts/Poppins",
        "./src/assets/fonts/Sora",
    ],
};