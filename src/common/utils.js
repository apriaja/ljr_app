// React Native Library
import { Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';
import moment from 'moment';

// Method to check the Platform of the device
export const isIOS = () => {
  return Platform.OS === 'ios';
};

// Tab mode flag
export let isTableted = DeviceInfo.isTablet();
// App version
export let appVersion = DeviceInfo.getVersion();
// Notch flag
export const hasNotch = DeviceInfo.hasNotch();

// Method to format the mobile number (0812-345-678, 0812-3456-789)
export const formatPhoneNumber = value => {
  if (value !== '') {
    let val = value.replace(/[^\d]/g, '');
    if (val.length <= 6 && val.length > 3) {
      return val.slice(0, 3) + '-' + val.slice(3, val.length);
    } else if (val.length === 7) {
      return val.slice(0, 4) + '-' + val.slice(4, 7);
    } else if (val.length < 11 && val.length > 7) {
      return val.slice(0, 4) + '-' + val.slice(4, 7) + '-' + val.slice(7, val.length);
    } else if (val.length >= 11) {
      return val.slice(0, 4) + '-' + val.slice(4, 8) + '-' + val.slice(8, val.length);
    } else {
      return val;
    }
  }
};

// Method to format date and time (Do = 17th, DD = 17, MMMM = Oktober, MM = 09, MMM = Okt)
export const formatDateTime = (val, type) => {
  let m = moment(val);
  if (m.isValid()) {
    if (type == "line-minute") {
      return m.format('DD/MM/YYYY | H:mm');
    } else if (type == "dash-minute") {
      return m.format('DD/MM/YYYY, H:mm');
    } else if (type == "comma-minute") {
      return m.format('DD MMM YYYY, H:mm');
    } else if (type == "comma") {
      return m.format('DD MMM YYYY');
    } else if (type == "date") {
      return m.format('DD/MM/YYYY');
    } else if (type == "month-dash") {
      return m.format('DD-MM-YYYY');
    } else if (type == "year-month-day-dash") {
      return m.format('YYYY-MM-DD');
    } else if (type == "full-month") {
      return m.format('DD MMMM YYYY');
    } else if (type == 'time') {
      return m.format('HH.mm');
    } else if (type == 'time-colon') {
      return m.format('HH:mm');
    } else if (type == 'full-month-time') {
      return m.format('DD MMMM YYYY HH.mm');
    } else if (type == 'timestamp') {
      return m.format('YYYY-MM-DD HH:mm:ss.SSS');
    } else {
      return m.format('MMMM DD YYYY, h:mm:ss');
    }
  } else {
    return '';
  }
};

// Make the Utils available to other parts of the application
export default { isIOS, formatPhoneNumber, formatDateTime };
