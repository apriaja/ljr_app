const constants = {
    accNumberCriteria: /^[0-9]+$/,
    alphabetOnlyWithSpace: /^[a-zA-Z ]*$/,
    fromScreen: {
        REQUESTSTTB: 'REQUESTSTTB',
        REQUESTPU: 'REQUESTPU',
    }
}

// Make the Utils available to other parts of the application
export default constants;