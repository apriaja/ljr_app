const dummy = {
    dataFAQ: [
        { title: 'Berapa lama waktu pengiriman pesanan saya?' },
        { title: 'Bagaimana cara kerja claim LJR' },
        { title: 'Bagaimana cara kontak CS LJR' },
    ],
    dataHistorySTTB: [
        {
            id: 'Order000011',
            date: "17-05-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Sedang Diproses',
        },
        {
            id: 'Order00002',
            date: "12-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Selesai',
        },
        {
            id: 'Order00001',
            date: "02-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Gagal',
        },
        {
            id: 'Order000011',
            date: "17-05-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Sedang Diproses',
        },
        {
            id: 'Order00002',
            date: "12-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Selesai',
        },
        {
            id: 'Order00001',
            date: "02-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Gagal',
        },
        {
            id: 'Order000011',
            date: "17-05-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Sedang Diproses',
        },
        {
            id: 'Order00002',
            date: "12-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Selesai',
        },
        {
            id: 'Order00001',
            date: "02-03-2021",
            jumlah: "30",
            cust: 'PT customer A',
            alamat: 'Jl. Graha anggrek 63, palmerah Jakarta barat',
            status: 'Gagal',
        }
    ],
    dataDaftarSTTB: [
        {
            id: 'STTB00001',
            status: 'Sudah digunakan'
        },
        {
            id: 'STTB00001',
            status: 'Sudah digunakan'
        },
        {
            id: 'STTB00001',
            status: 'Sudah digunakan'
        },
        {
            id: 'STTB00001',
            status: 'Belum digunakan'
        },
        {
            id: 'STTB00001',
            status: 'Belum digunakan'
        },
        {
            id: 'STTB00001',
            status: 'Rusak'
        },
        {
            id: 'STTB00001',
            status: 'Rusak'
        },
        {
            id: 'STTB00001',
            status: 'Rusak'
        },
        {
            id: 'STTB00001',
            status: 'Rusak'
        }
    ],
    dataTrackingOrder: [
        {
            deliveryNumber: 'DV0931230923',
            sttbNumber: 'STTB00001',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sedang Dikirim',
            orderTracking: [
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        },
        {
            deliveryNumber: 'DV0931230923',
            sttbNumber: 'STTB00001',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sedang Dikirim',
            orderTracking: [
                {
                    date: `11 mei\n11.12`,
                    status: 'Sedang diambil oleh agent'
                },
                {
                    date: `10 mei\n12.00`,
                    status: 'Arrival origin'
                },
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        },
        {
            deliveryNumber: 'DV0931230924',
            sttbNumber: 'STTB00002',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sedang Dikirim',
            orderTracking: [
                {
                    date: `11 mei\n11.12`,
                    status: 'Sedang diambil oleh agent'
                },
                {
                    date: `10 mei\n12.00`,
                    status: 'Arrival origin'
                },
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        },
        {
            deliveryNumber: 'DV0931230925',
            sttbNumber: 'STTB00003',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sedang Dikirim',
            orderTracking: [
                {
                    date: `11 mei\n11.12`,
                    status: 'Sedang diambil oleh agent'
                },
                {
                    date: `10 mei\n12.00`,
                    status: 'Arrival origin'
                },
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        },
        {
            deliveryNumber: 'DV0931230926',
            sttbNumber: 'STTB00004',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sampai Tujuan',
            orderTracking: [
                {
                    date: `12 mei\n11.12`,
                    status: 'Sampai Tujuan'
                },
                {
                    date: `11 mei\n11.12`,
                    status: 'Sedang diambil oleh agent'
                },
                {
                    date: `10 mei\n12.00`,
                    status: 'Arrival origin'
                },
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        },
        {
            deliveryNumber: 'DV0931230927',
            sttbNumber: 'STTB00005',
            date: '2021-05-09',
            from: {
                name: 'PT Adidas Indonesia',
                address: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910'
            },
            destination: {
                name: "Toko sepatu",
                address: 'Jl . Ciledug raya No.8, RT.02/RW.02, Ciledug Raya, Kec. Karang Tengah, Kota Tangerang, Tangerang 15157'
            },
            status: 'Sampai Tujuan',
            orderTracking: [
                {
                    date: `12 mei\n11.12`,
                    status: 'Sampai Tujuan'
                },
                {
                    date: `11 mei\n11.12`,
                    status: 'Sedang diambil oleh agent'
                },
                {
                    date: `10 mei\n12.00`,
                    status: 'Arrival origin'
                },
                {
                    date: `09 mei\n12.00`,
                    status: 'Behasil dipickup'
                }
            ]
        }
    ],
    daftarPenjemputan: [
        {
            no: 'PUG0000001',
            alamat: [
                {
                    nama: 'Cabang Sentosa',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-99889-887 (Arief)'
                },
                {
                    nama: 'Cabang Anugerah',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-23459-887 (Banyu)'
                },
            ],
            tanggalJemput: '2021-05-21T17:00:00+07:00',
            kendaraan: "Mitsubishi CDD",
            driver: null,
        }
    ],
    jenisPickup: [
        {
            title: "Instant Service",
            desc: 'Pengiriman barang via darat tanpa konsolidasi',
        },
        {
            title: 'Cargo Service',
            desc: 'Pengiriman barang via darat, laut dan udara'
        }
    ],
    alamat: [
        {
            nama: 'PT Anugrah Sentosa',
            alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
            contact: '021-99889-887 (Arief)',
        },
        {
            nama: 'PT Adidas Indonesia',
            alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
            contact: '021-99889-887 (Arief)',
        },
        // {
        //     nama: 'PT Anugrah Sentosa',
        //     alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
        //     contact: '021-99889-887 (Arief)',
        // },
        // {
        //     nama: 'PT Adidas Indonesia',
        //     alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
        //     contact: '021-99889-887 (Arief)',
        // }
    ],
    jenisKendaraan: [
        {
            id: 1,
            jenis: 'Mitshubishi CDD',
            kapasitas: '5 Ton',
        },
        {
            id: 2,
            jenis: 'Mitshubishi CDD',
            kapasitas: '5 Ton',
        },
        {
            id: 3,
            jenis: 'Mitshubishi CDD',
            kapasitas: '5 Ton',
        }
    ],
    jumlahBerat: [
        {
            title: '1 Kg',
            value: "1",
        },
        {
            title: '10 Kg',
            value: "10",
        },
        {
            title: '100 Kg',
            value: "100",
        },
        {
            title: '1000 Kg',
            value: "1000",
        }
    ],
    jumlahVolume: [
        {
            title: '1 L',
            value: "1",
        },
        {
            title: '10 L',
            value: "10",
        },
        {
            title: '100 L',
            value: "100",
        },
        {
            title: '1000 L',
            value: "1000",
        }
    ],
    jumlahPalet: [
        {
            title: '1 Palet',
            value: "1",
        },
        {
            title: '10 Palet',
            value: "10",
        },
        {
            title: '100 Palet',
            value: "100",
        },
        {
            title: '1000 Palet',
            value: "1000",
        }
    ],
    historyPUSTTB: [
        {
            no: 'PUG0000001',
            alamat: [
                {
                    nama: 'PT Anugrah Sentosa',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-99889-887 (Arief)',
                    statusID: 'Sedang Diproses',
                    statusDesc: 'Driver sedang dijalan'
                },
                {
                    nama: 'PT Anugrah Sentosa',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-99889-887 (Arief)',
                    statusID: 'Sedang Diproses',
                    statusDesc: 'Driver sedang dijalan',
                },
            ],
            tglPickUp: '2021-05-10 17:00 WIB',
            mobilPesanan: [
                {
                    id: 1,
                    jenis: 'Mitshubishi CDD',
                    kapasitas: '5 Ton',
                }
            ],
            driver: '',
            status: 'Sedang Diproses'
        },
        {
            no: 'PUG0000001',
            alamat: [
                {
                    nama: 'PT Anugrah Sentosa',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-99889-887 (Arief)',
                    statusID: 'Sedang Diproses',
                    statusDesc: 'Driver sedang dijalan'
                },
                {
                    nama: 'PT Anugrah Sentosa',
                    alamat: 'Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910',
                    contact: '021-99889-887 (Arief)',
                    statusID: 'Sedang Diproses',
                    statusDesc: 'Driver sedang dijalan',
                },
            ],
            tglPickUp: '2021-05-10 17:00 WIB',
            mobilPesanan: [
                {
                    id: 1,
                    jenis: 'Mitshubishi CDD',
                    kapasitas: '5 Ton',
                }
            ],
            driver: '',
            status: 'Sedang Diproses'
        }
    ],
    newHistory: [
        {
            "noPu": "PUG001",
            "data": [
                {
                    "jenisPengirimian": {
                        "title": "Instant Service",
                        "desc": "Pengiriman barang via darat tanpa konsolidasi"
                    },
                    "alamat": {
                        "PU": {
                            "nama": "PT Anugrah Sentosa",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "PU"
                        },
                        "DROP": {
                            "nama": "PT Adidas Indonesia",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "DROP"
                        }
                    },
                    "detailInformasi": {
                        "jenisBarang": "Jenis barang",
                        "satuan": "Berat",
                        "nilai": "12",
                        "deskripsi": "Deskripsi Barang"
                    },
                    "dataKendaraan": [
                        {
                            "id": 1,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:37.277Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        },
                        {
                            "id": 2,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:38.105Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        }
                    ],
                    "status": "Failed"
                },
                {
                    "jenisPengirimian": {
                        "title": "Instant Service",
                        "desc": "Pengiriman barang via darat tanpa konsolidasi"
                    },
                    "alamat": {
                        "PU": {
                            "nama": "PT Anugrah Sentosa",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "PU"
                        },
                        "DROP": {
                            "nama": "PT Adidas Indonesia",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "DROP"
                        }
                    },
                    "detailInformasi": {
                        "jenisBarang": "Jenis barang",
                        "satuan": "Berat",
                        "nilai": "12",
                        "deskripsi": "Deskripsi Barang"
                    },
                    "dataKendaraan": [
                        {
                            "id": 1,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:37.277Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        },
                        {
                            "id": 2,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:38.105Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        }
                    ],
                    "status": "OnProgress"
                }
            ],
        },
        {
            "noPu": "PUG002",
            "data": [
                {
                    "jenisPengirimian": {
                        "title": "Instant Service",
                        "desc": "Pengiriman barang via darat tanpa konsolidasi"
                    },
                    "alamat": {
                        "PU": {
                            "nama": "PT Anugrah Sentosa",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "PU"
                        },
                        "DROP": {
                            "nama": "PT Adidas Indonesia",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "DROP"
                        }
                    },
                    "detailInformasi": {
                        "jenisBarang": "Jenis barang",
                        "satuan": "Berat",
                        "nilai": "12",
                        "deskripsi": "Deskripsi Barang"
                    },
                    "dataKendaraan": [
                        {
                            "status: ": "Failed",
                            "id": 1,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:37.277Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        },
                        {
                            "status: ": "OnProgress",
                            "id": 2,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:38.105Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        }
                    ],
                    "status": "OnProgress"
                },
                {
                    "jenisPengirimian": {
                        "title": "Instant Service",
                        "desc": "Pengiriman barang via darat tanpa konsolidasi"
                    },
                    "alamat": {
                        "PU": {
                            "nama": "PT Anugrah Sentosa",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "PU"
                        },
                        "DROP": {
                            "nama": "PT Adidas Indonesia",
                            "alamat": "Jl . Tipar Cakung No.6, RT.6/RW.5,Cakung Barat, Kec. Cakung, Kota Jakarta Timur,  DKI Jakarta 13910",
                            "contact": "021-99889-887 (Arief)",
                            "typeAlamat": "DROP"
                        }
                    },
                    "detailInformasi": {
                        "jenisBarang": "Jenis barang",
                        "satuan": "Berat",
                        "nilai": "12",
                        "deskripsi": "Deskripsi Barang"
                    },
                    "dataKendaraan": [
                        {
                            "status: ": "Failed",
                            "id": 1,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:37.277Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        },
                        {
                            "status: ": "OnProgress",
                            "id": 2,
                            "jenis": "Mitshubishi CDD",
                            "kapasitas": "5 Ton",
                            "jumlah": 1,
                            "date": "2021-11-23T06:11:38.105Z",
                            "driver": "Asep",
                            "krani": "Dodi",
                            "status": "Failed"
                        }
                    ],
                    "status": "OnProgress"
                }
            ],
        },
    ]
}

// Make the Utils available to other parts of the application
export default dummy;