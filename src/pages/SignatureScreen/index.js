import React, { Component } from 'react'
import { View, Text, BackHandler, TouchableWithoutFeedback, TouchableOpacity, Image } from 'react-native'
import SignatureCapture from 'react-native-signature-capture';
import Modal from 'react-native-modal';

// Library
import { connect } from 'react-redux'

// Component
import { Header } from '../../components';
import { Colors, Metrics } from '../../themes';

// Style
import styles from './styles'

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signature: '',
            showModal: false,
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    saveSign() {
        this.refs["sign"].saveImage();
    }

    resetSign() {
        this.refs["sign"].resetImage();
    }

    saveEvent = (result) => {
        // console.log(result);
        this.setState({ signature: result ? result.encoded : "", showModal: true })
    }

    _onDragEvent() {
        // This callback will be called when the user enters signature
        // console.log("dragged");
    }

    renderModal = () => {
        var base64Icon = `data:image/png;base64, ${this.state.signature}`
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback onPress={() => this.setState({ showModal: false })}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-end', margin: 0, }}
                isVisible={this.state.showModal}>
                <View style={[styles.justifyAlignCenter, { maxHeight: Metrics.screenHeight / 2, backgroundColor: Colors.white, paddingTop: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10 }]}>
                    <Image style={{ width: 300, height: 300, resizeMode: 'contain', borderWidth: 1, borderColor: Colors.black }} source={{ uri: base64Icon }} />
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title="Signature" onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <SignatureCapture
                        style={[{ flex: 1 }, styles.signature]}
                        ref="sign"
                        onSaveEvent={(result) => this.saveEvent(result)}
                        onDragEvent={this._onDragEvent}
                        saveImageFileInExtStorage={false}
                        showNativeButtons={false}
                        showTitleLabel={false}
                        backgroundColor={Colors.white}
                        strokeColor={Colors.black}
                        minStrokeWidth={4}
                        maxStrokeWidth={4}
                        viewMode={"portrait"} />
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <TouchableOpacity activeOpacity={1} style={styles.buttonStyle}
                            onPress={() => { this.saveSign() }} >
                            <Text>Save</Text>
                        </TouchableOpacity>

                        <TouchableOpacity activeOpacity={1} style={styles.buttonStyle}
                            onPress={() => { this.resetSign() }} >
                            <Text>Reset</Text>
                        </TouchableOpacity>
                    </View>

                </View>
                {this.renderModal()}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
