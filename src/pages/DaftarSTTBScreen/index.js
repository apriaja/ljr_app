import React, { Component } from 'react'
import { View, Text, BackHandler, FlatList, TouchableOpacity } from 'react-native'

// Library
import { connect } from 'react-redux'
import dummy from '../../common/dummy';

// Component
import { Header } from '../../components';

// Style
import styles from './styles'
import { Colors } from '../../themes';

import IconFilter from '../../assets/images/ico.filter.svg';

let filterItem = [
    {
        title: "Semua data",
        value: "all"
    },
    {
        title: "STTB yang telah digunakan",
        value: "Sudah digunakan"
    },
    {
        title: "STTB yang belum digunakan",
        value: "Belum digunakan"
    },
    {
        title: "STTB yang rusak",
        value: "Rusak"
    },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: dummy.dataDaftarSTTB,
            showFilter: false,
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    renderMain = () => {
        return (
            <FlatList
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                contentContainerStyle={{ marginTop: 6 }}
                renderItem={({ item, index }) => (
                    <View style={[styles.flexHorizontal, { justifyContent: 'space-between', alignItems: 'center', paddingVertical: 8, paddingHorizontal: 11, borderBottomWidth: 2, borderColor: 'rgba(0, 0, 0, 0.2)' }]}>
                        <Text style={styles.idTxt}>{item.id}</Text>
                        <Text style={styles.statusTxt}>{item.status}</Text>
                    </View>
                )}
                keyExtractor={(item, index) => index.toString()}
                extraData={this.state}
            />
        )
    }

    onPressFilter = (item) => {
        this.setState({ showFilter: false })
        if (item.value == "all") {
            this.setState({ data: dummy.dataDaftarSTTB })
        } else {
            let result = [];
            dummy.dataDaftarSTTB.map(elm => {
                elm.status.toLowerCase().includes(item.value.toLowerCase()) && result.push(elm)
            })
            this.setState({ data: result })
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title="Daftar STTB" onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, marginHorizontal: 23, marginVertical: 10, backgroundColor: "rgba(196, 224, 227, 0.52)", borderRadius: 10, }}>
                    {this.renderMain()}
                    <View style={{ position: 'absolute', top: 0, right: 0, marginTop: -37 }}>
                        <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ showFilter: !this.state.showFilter })} style={[styles.flexHorizontal, { alignSelf: 'flex-end', paddingVertical: 6, marginBottom: 5 }]}>
                            <Text style={[styles.filterTxt, { marginRight: 4 }]}>{"Filter daftar"}</Text>
                            <IconFilter />
                        </TouchableOpacity>
                    </View>
                    {
                        this.state.showFilter &&
                        <View style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
                            <TouchableOpacity activeOpacity={1} onPress={() => this.setState({ showFilter: !this.state.showFilter })} style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} />
                            <View style={{ backgroundColor: Colors.white, marginTop: -10, paddingHorizontal: 10, paddingHorizontal: 5, alignSelf: 'flex-end', borderRadius: 5, borderWidth: 1, borderColor: "rgba(165, 164, 164, 1)" }}>
                                <FlatList
                                    style={{ flexGrow: 0 }}
                                    data={filterItem}
                                    renderItem={({ item, index }) => (
                                        <TouchableOpacity activeOpacity={1} onPress={() => this.onPressFilter(item)} style={{ paddingVertical: 5, paddingRight: 40 }}>
                                            <Text>{item.title}</Text>
                                        </TouchableOpacity>
                                    )}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        </View>
                    }
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
