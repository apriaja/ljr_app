// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
  ...applicationStyles,
  ...Fonts,
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  idTxt: {
    ...Fonts.notoSans,
    fontSize: 15,
    letterSpacing: -0.33,
    color: Colors.black,
  },
  statusTxt: {
    ...Fonts.notoSans,
    fontSize: 13,
    letterSpacing: -0.33,
    color: 'rgba(0, 0, 0, 0.57)',
  },
  filterTxt: {
    ...Fonts.notoSansBold,
    fontSize: 13,
    letterSpacing: -0.333,
    color: Colors.black,
  }
});

// Make the styles available for ActivityScreens
export default styles;