import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { Button, Header, Tab } from '../../components';

// Redux
import { setLoading } from '../../actions';

// Style
import styles from './styles'

// Constans
import constants from '../../common/constants';

// Assets
import IconSuccess from '../../assets/images/ico.Success.svg'

// Navigation
import navigationService from '../../navigation/navigationService';

// Page
import PAGE from './page';

import { AddressListScreen } from './AddressList';
import { AddAddressScreen } from './AddAddress';

import dummy from '../../common/dummy'

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: PAGE.LISTADDRESS
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        if (this.state.page === PAGE.ADDADDRESS) {
            this.setState({ page: PAGE.LISTADDRESS })
        } else {
            this.props.navigation.goBack();
        }
    }

    renderList = () => {
        return (
            <AddressListScreen
                data={dummy.alamat}
                onPressAdd={() => {
                    this.setState({ page: PAGE.ADDADDRESS })
                }}
                onPressNext={(data) => {
                    navigationService.navigate("RequestSTBScreen", { data: data })
                }}
                fromPage={constants.fromScreen.REQUESTSTTB}
            />
        )
    }

    renderAdd = () => {
        return (
            <AddAddressScreen
                onPressSave={(values) => {
                    console.log("values: ", values)
                    this.props.setLoading(true)
                    setTimeout(() => {
                        this.props.setLoading(false)
                        this.onBack()
                    }, 2000)

                }}
                fromPage={constants.fromScreen.REQUESTSTTB}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header horizontal={true} title={this.state.page} onBackPress={() => this.onBack()} />
                {
                    this.state.page === PAGE.ADDADDRESS ? this.renderAdd() :
                        this.state.page === PAGE.LISTADDRESS ? this.renderList() : null
                }
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)