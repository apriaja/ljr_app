// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    container: {
        flex: 1,
        backgroundColor: Colors.white
    },
    txtSuccess: {
        ...Fonts.soraRegular,
        fontSize: 25,
        textAlign: 'center',
        letterSpacing: -0.177,
        color: '#1D438A',
    }
});

// Make the styles available for ActivityScreens
export default styles;