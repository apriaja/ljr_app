import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, KeyboardAvoidingView, ScrollView } from 'react-native';

import { TextInputBase } from '../../../components';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../themes';

import constants from '../../../common/constants';

// Assets
import IconAddressBlue from '../../../assets/images/ico.AddressBlue.svg';
import IconAddressGreen from '../../../assets/images/ico.AddressGreen.svg';
import IconAddressRed from '../../../assets/images/ico.AddressRed.svg';

let TYPEALAMAT = {
    PU: "PU",
    DROP: 'DROP',
}

const AddressListScreen = ({ data, onPressAdd, onPressNext, fromPage, typeAlamat, onSelectAlamat }) => {
    const [selectedAlamat, setSelectedAlamat] = useState(null)
    const [textAlamat, setTextAlamat] = useState("")
    const [tempData, setTempData] = useState([])

    const onSelectedAlamat = (data) => {
        if (fromPage == constants.fromScreen.REQUESTSTTB) {
            setSelectedAlamat(data)
        } else if (fromPage == constants.fromScreen.REQUESTPU) {
            onSelectAlamat({ ...data, typeAlamat })
        }
    }

    useEffect(() => {
        setTempData(data)
    }, [data])

    useEffect(() => {
        // setTempData()
        console.log(textAlamat)
        if (textAlamat !== "") {
            const dataTemp = data.filter(function (e) { 
                return ( e.alamat && e.alamat.toLowerCase().includes(textAlamat.toLowerCase()) ) || e.nama.toLowerCase().includes(textAlamat.toLowerCase())
            })
            setTempData(dataTemp)
        }
    }, [textAlamat]);


    let type = typeAlamat == TYPEALAMAT.PU ? "Tambah Alamat Pickup" : typeAlamat == TYPEALAMAT.DROP ? "Tambah Alamat Delivery" : ""
    return (
        <View style={{ flex: 1, paddingTop: 5, }}>
            <View style={{ flex: 1 }}>
                <View style={[styles.flexHorizontal, { paddingTop: 5, paddingBottom: 5, paddingHorizontal: 10, alignItems: 'center', marginHorizontal: 20, borderWidth: 1, borderRadius: 10, borderColor: 'rgba(0, 0, 0, 0.5)' }]}>
                    {
                        fromPage == constants.fromScreen.REQUESTPU ? typeAlamat == TYPEALAMAT.PU ? <IconAddressRed /> : <IconAddressGreen /> :
                            <IconAddressGreen />
                    }
                    <TextInputBase
                        styleContainer={{ borderColor: "rgba(0, 0, 0, 0)", marginLeft: 10, width: '90%', }}
                        placeholder="Set Lokasi Pickup"
                        value={textAlamat}
                        keyboardType={'ascii-capable'}
                        onChangeText={(input) => {setTextAlamat(input)}}
                        onBlur={(input) => {}}
                        error={null}
                        styleText={{color: "black", fontSize: 18}}
                        // image={fromPage == constants.fromScreen.REQUESTPU ? typeAlamat == TYPEALAMAT.PU ? <IconAddressRed width={24} height={24} /> : <IconAddressGreen width={24} height={24} /> : <IconAddressGreen width={24} height={24} />}
                    />
                </View>
                <View style={{ marginTop: 11, marginBottom: 15, marginLeft: 34, marginRight: 10 }}>
                    <Text style={styles.chooseAddressTxt}>{"Lokasi Pickup"}</Text>
                </View>
                {/* <ScrollView style={[ { flex: 1 }]}> */}
                    <FlatList
                        data={tempData}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity activeOpacity={1} onPress={() => onSelectedAlamat(item)} style={{ paddingLeft: 37, paddingRight: 15 }}>
                                <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                    {
                                        fromPage == constants.fromScreen.REQUESTPU ? typeAlamat == TYPEALAMAT.PU ? <IconAddressRed width={12} height={12} /> : <IconAddressGreen width={12} height={12} /> :
                                            <IconAddressGreen width={12} height={12} />
                                    }
                                    <Text style={[styles.nameTxt, { marginLeft: 4 }]}>{item.nama}</Text>
                                </View>
                                <View style={{ paddingLeft: 16, marginTop: 3 }}>
                                    <Text style={[styles.addressTxt, { marginBottom: 3 }]}>{item.alamat}</Text>
                                    <Text style={styles.cpTxt}>{item.contact}</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                        ItemSeparatorComponent={() => (
                            <View style={{ height: 2, width: '100%', marginVertical: 13 }} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={tempData}
                        style={[ { flex: 1 }]}
                    />
                {/* </ScrollView> */}
            </View>
            <View style={[{ alignContent: "flex-end" }]}>
                {
                    fromPage == constants.fromScreen.REQUESTSTTB ?
                        <View style={[styles.containerBtn]}>
                            <View style={{ paddingHorizontal: 9, marginBottom: 16 }}>
                                {
                                    selectedAlamat && <>
                                        <Text style={[styles.textLocBtn, { marginLeft: 19, marginBottom: 3, }]}>{"Lokasi Terima STTB"}</Text>
                                        <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                            <IconAddressGreen width={12} height={12} />
                                            <Text style={[styles.textAddressBtn, { marginLeft: 7 }]}>{selectedAlamat.nama}</Text>
                                        </View>
                                    </>
                                }
                            </View>
                            <View style={[styles.flexHorizontal, { overflow: 'hidden', borderWidth: 1, borderRadius: 5, borderColor: "#2C4074" }]}>
                                <TouchableOpacity onPress={() => onPressAdd()} activeOpacity={1} style={[styles.flexHorizontal, { alignItems: 'center', flex: 0.5, paddingHorizontal: 10, paddingVertical: 7 }]}>
                                    <IconAddressBlue />
                                    <Text style={[styles.btnTxt, { color: '#2C4074', marginLeft: 5, }]}>{"Tambah Alamat Terima STTB"}</Text>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => onPressNext(selectedAlamat)} activeOpacity={1} style={[styles.justifyAlignCenter, { flex: 0.5, backgroundColor: "#2C4074", paddingHorizontal: 10, paddingVertical: 7 }]}>
                                    <Text style={[styles.btnTxt, { color: Colors.white }]}>{"Berikutnya"}</Text>
                                </TouchableOpacity>
                            </View>
                        </View> : fromPage == constants.fromScreen.REQUESTPU ?
                            <View style={[{ borderWidth: 1, borderRadius: 5, borderColor: "#2C4074", marginTop: 17, marginHorizontal: 10, marginBottom: 19 }]}>
                                <TouchableOpacity onPress={() => onPressAdd()} activeOpacity={1} style={[styles.flexHorizontal, { justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 12 }]}>
                                    <IconAddressBlue />
                                    <Text style={[styles.btnTxt, { color: '#2C4074', marginLeft: 5, }]}>{type}</Text>
                                </TouchableOpacity>
                            </View> : null
                }
            </View>
        </View>
    )
}

AddressListScreen.propTypes = {
    data: PropTypes.array,
    onPressAdd: PropTypes.func,
    onPressNext: PropTypes.func,
    fromPage: PropTypes.string,
    typeAlamat: PropTypes.string,
    onSelectAlamat: PropTypes.func,
};

export { AddressListScreen };