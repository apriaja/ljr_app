// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    containerBtn: {
        paddingTop: 17,
        paddingBottom: 19,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(230, 237, 250, 0.46)',
        borderTopLeftRadius: 31,
        borderTopRightRadius: 31,
    },
    textLocBtn: {
        ...Fonts.soraRegular,
        ...Fonts.subTag,
        lineHeight: 13,
        color: 'rgba(0, 0, 0, 0.66)',
        letterSpacing: -0.333
    },
    textAddressBtn: {
        ...Fonts.soraMedium,
        fontSize: 13,
        lineHeight: 16,
        color: Colors.black,
        letterSpacing: -0.333,
    },
    btnTxt: {
        ...Fonts.robotoBold,
        fontSize: 13,
        lineHeight: 15,
        letterSpacing: -0.333,
        textAlign: 'center',
    },
    setLocTxt: {
        ...Fonts.soraBold,
        ...Fonts.tag,
        color: "rgba(0, 0, 0, 0.56)",
        letterSpacing: -0.333,
    },
    chooseAddressTxt: {
        ...Fonts.soraBold,
        ...Fonts.tag,
        color: Colors.black,
        letterSpacing: -0.333,
    },
    nameTxt: {
        ...Fonts.soraMedium,
        fontSize: 13,
        color: Colors.black,
        letterSpacing: -0.333,
    },
    addressTxt: {
        ...Fonts.soraRegular,
        ...Fonts.tag,
        color: "rgba(0, 0, 0, 0.66)",
        letterSpacing: -0.333,
    },
    cpTxt: {
        ...Fonts.soraMedium,
        ...Fonts.tag,
        color: Colors.black,
        letterSpacing: -0.333,
    }
});

// Make the styles available for ActivityScreens
export default styles;