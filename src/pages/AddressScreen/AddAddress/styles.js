// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    btnLabelSelect: {
        borderWidth: 1,
        borderColor: '#2C4074',
        backgroundColor: '#2C4074',
        borderRadius: 15,
        paddingVertical: 11,
    },
    btnLabelUnselect: {
        borderWidth: 1,
        borderColor: 'rgba(44, 64, 116, 0.55)',
        backgroundColor: Colors.white,
        borderRadius: 15,
        paddingVertical: 11,
    },
    txtLabelSelect: {
        ...Fonts.soraBold,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: Colors.white,
    },
    txtLabelUnselect: {
        ...Fonts.soraBold,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: "rgba(44, 64, 116, 0.62)",
    },
    labelTxt: {
        ...Fonts.soraBold,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;