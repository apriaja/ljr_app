import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, ScrollView, Text, TouchableOpacity, View, KeyboardAvoidingView } from 'react-native';
import api from '../../../services/api';
import Config from 'react-native-config';

// Components
import { TextInputBase, Button } from '../../../components';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../themes';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';

import constants from '../../../common/constants';

// Assets
import IconAddressWhite from '../../../assets/images/ico.AddressWhite.svg';
import IconAddressGreen from '../../../assets/images/ico.AddressGreen.svg';
import IconAddressRed from '../../../assets/images/ico.AddressRed.svg';
import IconContact from '../../../assets/images/ico.Contact.svg';
import IconMagnify from '../../../assets/images/ico.Magnify.svg';

let labelAlamat = {
    RUMAH: 'Rumah',
    KANTOR: 'Kantor',
    APARTMENT: 'Apartemen',
}

let TYPEALAMAT = {
    PU: "PU",
    DROP: 'DROP',
}

const AddAddressScreen = ({ onPressSave, fromPage, typeAlamat }) => {

    const [listKota, setListKota] = useState([]);
    const [listKecamatan, setListKecamatan] = useState([]);
    const [listKelurahan, setListKelurahan] = useState([]);

    useEffect(async () => {
        if (listKota.length == 0) {
            const resKota = await api.fetchAPI(`${Config.API_NGROK}/kotaKab`, null, 'GET');
            setListKota(resKota.data.data);
        }
        if (listKecamatan.length == 0) {
            const resKec = await api.fetchAPI(`${Config.API_NGROK}/kecamatan`, null, 'GET');
            setListKecamatan(resKec.data.data);
        }
        if (listKelurahan.length == 0) {
            const resKel = await api.fetchAPI(`${Config.API_NGROK}/kelurahan`, null, 'GET');
            setListKelurahan(resKel.data.data);
        }
    }, [])

    return (
        <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : ""} style={{ flex: 1 }}>
            <Formik
                initialValues={{
                    label: labelAlamat.RUMAH,
                    alamat: "",
                    detailAlamat: "",
                    kota: "",
                    kecamatan: "",
                    kelurahan: "",
                    cp: "",
                    telp: "",
                }}
                validationSchema={validationSchema}
                validateOnBlur={true}
                onSubmit={(values) => onPressSave(values)}
            >
                {formikProps => (
                    <View style={{ flex: 1 }}>
                        <View style={{ flex: 1, paddingLeft: 25, paddingRight: 21 }}>
                            <ScrollView>
                                <Text style={[styles.labelTxt, { marginLeft: 9 }]}>{"Label Alamat"}</Text>
                                <View style={[styles.flexHorizontal, { justifyContent: 'space-around', paddingHorizontal: 20, marginTop: 8, marginBottom: 7 }]}>
                                    <Button width={84} textStyle={formikProps.values.label === labelAlamat.RUMAH ? styles.txtLabelSelect : styles.txtLabelUnselect} containerStyle={formikProps.values.label === labelAlamat.RUMAH ? styles.btnLabelSelect : styles.btnLabelUnselect} onPress={() => formikProps.setFieldValue("label", labelAlamat.RUMAH)} text={labelAlamat.RUMAH} />
                                    <Button width={84} textStyle={formikProps.values.label === labelAlamat.KANTOR ? styles.txtLabelSelect : styles.txtLabelUnselect} containerStyle={formikProps.values.label === labelAlamat.KANTOR ? styles.btnLabelSelect : styles.btnLabelUnselect} onPress={() => formikProps.setFieldValue("label", labelAlamat.KANTOR)} text={labelAlamat.KANTOR} />
                                    <Button width={84} textStyle={formikProps.values.label === labelAlamat.APARTMENT ? styles.txtLabelSelect : styles.txtLabelUnselect} containerStyle={formikProps.values.label === labelAlamat.APARTMENT ? styles.btnLabelSelect : styles.btnLabelUnselect} onPress={() => formikProps.setFieldValue("label", labelAlamat.APARTMENT)} text={labelAlamat.APARTMENT} />
                                </View>
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Pilih Alamat Maps"
                                    value={formikProps.values.alamat}
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("alamat")}
                                    onBlur={formikProps.handleBlur("alamat")}
                                    error={formikProps.touched.alamat ? formikProps.errors.alamat : null}
                                    styleText={styles.labelTxt}
                                    image={fromPage == constants.fromScreen.REQUESTPU ? typeAlamat == TYPEALAMAT.PU ? <IconAddressRed width={24} height={24} /> : <IconAddressGreen width={24} height={24} /> : <IconAddressGreen width={24} height={24} />}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    isMultiline={true}
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    value={formikProps.values.detailAlamat}
                                    placeholder="Detail Alamat"
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("detailAlamat")}
                                    onBlur={formikProps.handleBlur("detailAlamat")}
                                    error={formikProps.touched.detailAlamat ? formikProps.errors.detailAlamat : null}
                                    styleText={styles.labelTxt}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Nama Kota/kab"
                                    value={formikProps.values.kota}
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("kota")}
                                    onBlur={formikProps.handleBlur("kota")}
                                    error={formikProps.touched.kota ? formikProps.errors.kota : null}
                                    styleText={styles.labelTxt}
                                    image={<IconMagnify />}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Nama Kecamatan"
                                    value={formikProps.values.kecamatan}
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("kecamatan")}
                                    onBlur={formikProps.handleBlur("kecamatan")}
                                    error={formikProps.touched.kecamatan ? formikProps.errors.kecamatan : null}
                                    styleText={styles.labelTxt}
                                    image={<IconMagnify />}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Nama Kelurahan"
                                    value={formikProps.values.kelurahan}
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("kelurahan")}
                                    onBlur={formikProps.handleBlur("kelurahan")}
                                    error={formikProps.touched.kelurahan ? formikProps.errors.kelurahan : null}
                                    styleText={styles.labelTxt}
                                    image={<IconMagnify />}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Nama Contact Person"
                                    value={formikProps.values.cp}
                                    keyboardType={'ascii-capable'}
                                    onChangeText={formikProps.handleChange("cp")}
                                    onBlur={formikProps.handleBlur("cp")}
                                    error={formikProps.touched.cp ? formikProps.errors.cp : null}
                                    styleText={styles.labelTxt}
                                />
                                <View style={{ paddingTop: 3 }} />
                                <TextInputBase
                                    styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)", borderRadius: 10, }}
                                    placeholder="Telpon Contact Person"
                                    value={formikProps.values.telp}
                                    keyboardType={'number-pad'}
                                    onChangeText={formikProps.handleChange("telp")}
                                    onBlur={formikProps.handleBlur("telp")}
                                    error={formikProps.touched.telp ? formikProps.errors.telp : null}
                                    styleText={styles.labelTxt}
                                    image={<IconContact />}
                                />
                                <View style={{ paddingTop: 3 }} />
                            </ScrollView>
                        </View>
                        <View style={{ paddingLeft: 3, paddingRight: 5, paddingVertical: 17, backgroundColor: Colors.white }} >
                            <Button disabled={!formikProps.isValid} width={'100%'} btnImage={<IconAddressWhite />} onPress={formikProps.handleSubmit} text="Simpan Alamat" />
                        </View>
                    </View>
                )}
            </Formik>
        </KeyboardAvoidingView>
    )
}


const validationSchema = yup.object().shape({
    label: yup
        .string()
        .required("Silahkan masukkan subject email")
        .test('isPhone', 'Masukkan subject email dengan benar',
            value => {
                return value
            }),
    alamat: yup
        .string()
        .required("Silahkan masukkan alamat")
        .test('isPhone', 'Masukkan alamat dengan benar',
            value => {
                return value
            }),
    detailAlamat: yup
        .string()
        .required("Silahkan masukkan detail alamat")
        .test('isPhone', 'Masukkan detail alamat dengan benar',
            value => {
                return value
            }),
    kota: yup
        .string()
        .required("Silahkan masukkan kota/kabupaten")
        .test('isPhone', 'Masukkan kota/kabupaten dengan benar',
            value => {
                return value
            }),
    kecamatan: yup
        .string()
        .required("Silahkan masukkan kecamatan")
        .test('isPhone', 'Masukkan kecamatan dengan benar',
            value => {
                return value
            }),
    kelurahan: yup
        .string()
        .required("Silahkan masukkan kelurahan")
        .test('isPhone', 'Masukkan kelurahan dengan benar',
            value => {
                return value
            }),
    cp: yup
        .string()
        .required("Silahkan masukkan contact person")
        .test('isPhone', 'Masukkan contact person dengan benar',
            value => {
                return value
            }),
    telp: yup
        .string()
        .required("Silahkan masukkan telpon contact person")
        .test('isPhone', 'Masukkan telpon contact person dengan benar',
            value => {
                return value
            }),
})

AddAddressScreen.propTypes = {
    onPressSave: PropTypes.func,
    fromPage: PropTypes.string,
    typeAlamat: PropTypes.string,
};

export { AddAddressScreen };