import React, { Component } from 'react'
import { View, Image, Text, TouchableOpacity, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { ScaledImage } from '../../components'

// Navigation
import navigationService from '../../navigation/navigationService'

// Style
import styles from './styles'
import { Metrics } from '../../themes'

// Assets
import IconMainScreen from '../../assets/images/ico.MainScreen.png'

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        BackHandler.exitApp()
    }

    gotoLogin = () => {
        navigationService.navigate("LoginScreen")
    }

    render() {
        return (
            <View style={[styles.container, { alignItems: 'center', justifyContent: 'space-between' }]}>
                <View>
                    <ScaledImage width={Metrics.screenWidth} uri={Image.resolveAssetSource(IconMainScreen).uri} />
                    <Text style={[styles.titleTxt, { marginHorizontal: 45, marginTop: 17 }]}>{"Expert in Handle Warehouse & Distribution Services"}</Text>
                    <Text style={[styles.descTxt, { marginHorizontal: 41, marginTop: 3 }]}>{"Pelayanan Service +1 memberikan pelayanan lebih maksimal kepada pelanggan, termasuk ketika kita berkerja kita memberikan sesuatu yang lebih demi mencapai taget yang maksimal."}</Text>
                </View>
                <TouchableOpacity activeOpacity={1} onPress={() => this.gotoLogin()} style={{ paddingHorizontal: 25, paddingVertical: 12, backgroundColor: "#DEDEDE", borderRadius: 20, marginBottom: 38 }}>
                    <Text style={styles.SignInTxt}>{"Sign In"}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
