// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
  ...applicationStyles,
  ...Fonts,
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  titleTxt: {
    ...Fonts.notoSansBold,
    ...Fonts.subContent,
    lineHeight: 25,
    letterSpacing: -0.33,
    color: Colors.black,
    textAlign: 'center',
  },
  descTxt: {
    ...Fonts.notoSans,
    fontSize: 11,
    letterSpacing: -0.33,
    textAlign: 'center',
    lineHeight: 15,
    color: 'rgba(0, 0, 0, 0.8)'
  },
  SignInTxt: {
    ...Fonts.notoSansBold,
    ...Fonts.description,
    letterSpacing: -0.33,
    color: '#2C4074',
  }
});

// Make the styles available for ActivityScreens
export default styles;