// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
  ...applicationStyles,
  ...Fonts,
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  noAccTxt: {
    ...Fonts.robotoRegular,
    fontSize: 13,
    lineHeight: 15,
    color: 'rgba(0, 0, 0, 0.52)',
    letterSpacing: -0.015,
  },
  contactUsTxt: {
    ...Fonts.robotoBold,
    fontSize: 13,
    lineHeight: 15,
    color: '#487596',
    letterSpacing: -0.015,
  }
});

// Make the styles available for ActivityScreens
export default styles;