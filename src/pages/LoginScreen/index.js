import React, { Component } from 'react'
import { View, Text, BackHandler, TouchableOpacity } from 'react-native'

// Library
import { connect } from 'react-redux'
import { Formik } from 'formik';
import * as yup from 'yup';

// Redux
import { loginActions, setLoading, setProfile } from '../../actions';

// Navigation
import navigationService from '../../navigation/navigationService';

// Component
import { Button, Header, TextInputBase } from '../../components';

// Styles
import styles from './styles'
import { LOGIN_FAILED, LOGIN_SUCCESS } from '../../actions/types';

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props != prevProps) {
            if (this.props.duringActions === LOGIN_SUCCESS) {
                this.props.setLoading(false)
                navigationService.resetNavigation("HomeTab")
            } else if (this.props.duringActions === LOGIN_FAILED) {
                this.props.setLoading(false)
                this.formikRef.setFieldError("password", "username salah")
                this.formikRef.setFieldError("username", "username salah")
            }
        }
    }

    onSubmit = (data) => {
        this.props.setLoading(true)
        this.props.login({
            email: data.username,
            password: data.password
        })
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title="Sign In" onBackPress={() => this.onBack()} />

                <Formik
                    innerRef={(c) => this.formikRef = c}
                    initialValues={{ username: "", password: "" }}
                    validationSchema={validationSchema}
                    validateOnBlur={true}
                    onSubmit={(values) => { this.onSubmit(values) }}
                >
                    {formikProps => (
                        <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 12 }}>
                            <TextInputBase
                                placeholder="Email/Username"
                                value={formikProps.values.username}
                                keyboardType={'ascii-capable'}
                                onChangeText={formikProps.handleChange("username")}
                                onBlur={formikProps.handleBlur("username")}
                                error={formikProps.touched.username ? formikProps.errors.username : null}
                            />
                            <View style={{ padding: 7 }} />
                            <TextInputBase
                                value={formikProps.values.password}
                                placeholder="Password"
                                isPassword={true}
                                onChangeText={formikProps.handleChange("password")}
                                onBlur={formikProps.handleBlur("password")}
                                error={formikProps.touched.password ? formikProps.errors.password : null}
                            />
                            <View style={{ padding: 6 }} />
                            <Button onPress={formikProps.handleSubmit} text="LOGIN" width={'100%'} />
                            <View style={{ marginTop: 58 }}>
                                <Text style={styles.noAccTxt}>{"Belum punya akun?"}</Text>
                                <Text style={styles.contactUsTxt}>{`Harap Menghubungi Customer Service LJR.\n(021) 5440 822\n+62 81-1400-1800`}</Text>
                            </View>
                        </View>
                    )}
                </Formik>
            </View>
        )
    }
}

const validationSchema = yup.object().shape({
    username: yup
        .string()
        .required("Silahkan masukkan username")
        .test('isPhone', 'Masukkan username dengan benar',
            value => {
                return value
            }),
    password: yup
        .string()
        .required("Silahkan masukkan password")
        .test('isPhone', 'Masukkan password dengan benar',
            value => {
                return value
            }),
})

const mapStateToProps = state => {
    return {
        duringActions: state.authentication.duringActions,
    }
}

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
    setProfile: (data) => dispatch(setProfile(data)),
    login: (data) => dispatch(loginActions(data)),
});


export default connect(mapStateToProps, mapDispatchToProps)(index)
