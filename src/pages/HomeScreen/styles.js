// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    fonta: {
        ...Fonts.robotoBold,
        fontSize: 16,
    },
    fontb: {
        ...Fonts.robotoRegular,
        fontSize: 16,
    },
    fontc: {
        ...Fonts.notoSans,
        fontSize: 16,
    },
    fontd: {
        ...Fonts.notoSansBold,
        fontSize: 16,
    },
    topMenuBar: {
        justifyContent: 'center',
        width: 72,
        height: 5,
        borderRadius: 10,
        backgroundColor: 'rgba(0, 0, 0, 0.41)',
        marginBottom: Metrics.basePaddingNew * 1.5
    },
    containerMenu: {
        position: 'absolute',
        width: Metrics.screenWidth,
        height: Metrics.screenHeight * 0.8,
        backgroundColor: Colors.white,
        top: Metrics.screenHeight * 0.2,
        borderTopLeftRadius: 35,
        borderTopRightRadius: 35,
        paddingHorizontal: 28,
        paddingVertical: 11,
    },
    containerSearch: {
        alignItems: 'center',
        borderRadius: 10,
        paddingHorizontal: 12,
        backgroundColor: '#F3F3F3',
        height: 41,
        width: '100%'
    },
    textInputSearch: {
        ...Fonts.robotoMediumItalic,
        ...Fonts.tag,
        color: "rgba(0, 0, 0, 0.56)",
        letterSpacing: -0.33,
    },
    titleMenuTxt: {
        ...Fonts.notoSansBold,
        fontSize: 15,
        color: Colors.black,
        letterSpacing: -0.33,
    },
    menuTitleTxt: {
        ...Fonts.poppinsSemBold,
        fontSize: 11,
        color: 'rgba(31, 28, 28, 0.75)',
        letterSpacing: -0.33,
        textAlign: 'center',
    },
    menuContainer: {
        backgroundColor: '#FDFEFF',
        flex: 1,
        alignItems: 'flex-start',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        paddingTop: 6,
        paddingHorizontal: 6,
        borderRadius: 10
    }
});

// Make the styles available for ActivityScreens
export default styles;