import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image, TextInput, FlatList, BackHandler } from 'react-native'

// Library
import CardView from 'react-native-cardview';
import { connect } from 'react-redux'

// Theme
import styles from './styles'

// Navigation
import navigationService from '../../navigation/navigationService'

// Assets
import IconHomePNG from '../../assets/images/ico.Home.png'
import IconSearch from '../../assets/images/ico.search.svg'
import IconReqSTTB from '../../assets/images/HomeMenu/ico.ReqSTTB.svg'
import IconReqPU from '../../assets/images/HomeMenu/ico.ReqPickUp.svg'
import IconDaftarSTTB from '../../assets/images/HomeMenu/ico.DaftarSTTB.svg'
import IconFaqEmail from '../../assets/images/HomeMenu/ico.FaqMail.svg'
import IconLiveChat from '../../assets/images/HomeMenu/ico.LiveChat.svg'


import { Colors, Metrics } from '../../themes'
import { ScaledImage } from '../../components'


let menu = [
    {
        icon: <IconReqSTTB />,
        title: `Request\nSTTB`,
        navigation: 'AddressScreen',
    },
    {
        icon: <IconReqPU />,
        title: `Request\nPickup`,
        navigation: 'RequestPickUpScreen',
    },
    {
        icon: <IconDaftarSTTB />,
        title: `Daftar\nSTTB`,
        navigation: 'DaftarSTTBScreen',
    },
    {
        icon: <IconFaqEmail />,
        title: `Faq & \nEmail`,
        navigation: 'FAQScreen',
    },
    {
        icon: <IconLiveChat />,
        title: `Live Chat`,
        navigation: 'LiveChatScreen',
    },
    {
        title: ``,
        // icon: <IconLiveChat />,
        // title: `Signature`,
        // navigation: 'SignatuseScreen',
    },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            slider1ActiveSlide: 1,
            searchText: "",
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        BackHandler.exitApp()
    }

    onPressMenu = (navigation) => {
        navigationService.navigate(navigation)
    }

    renderMenu = () => {
        return (
            <View style={styles.containerMenu}>
                <View style={[{ flex: 1, alignItems: 'center' }]}>
                    <View style={styles.topMenuBar} />
                    <View style={[styles.flexHorizontal, styles.containerSearch]}>
                        <IconSearch />
                        <TextInput
                            style={[styles.textInputSearch, { width: '100%', marginLeft: 23 }]}
                            placeholder={"Silahkan Masukan Nomor STTB"}
                        />
                    </View>
                    <View style={{ marginTop: 19, alignSelf: 'stretch' }}>
                        <Text style={[styles.titleMenuTxt, { marginBottom: 12 }]}>{"Pilih tipe menu"}</Text>
                        <FlatList
                            scrollEnabled={false}
                            columnWrapperStyle={{ marginBottom: 10, justifyContent: 'space-between', padding: 2 }}
                            numColumns={3}
                            data={menu}
                            renderItem={({ item, index }) => {
                                return (
                                    <>
                                        {
                                            item.title != "" ?
                                                <TouchableOpacity activeOpacity={1} onPress={() => item.navigation ? this.onPressMenu(item.navigation) : null} style={{ flex: 0.28 }}>
                                                    <CardView cardElevation={3} cardMaxElevation={3} style={styles.menuContainer}>
                                                        {item.icon && item.icon}
                                                        <Text style={[styles.menuTitleTxt, { alignSelf: 'stretch', marginTop: 5, marginBottom: 7 }]}>{item.title}</Text>
                                                    </CardView>
                                                </TouchableOpacity>
                                                :
                                                <View style={{ flex: 0.28 }} />
                                        }
                                    </>
                                )
                            }}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            </View>
        )
    }

    render() {
        return (
            <View style={[{ flex: 1 }]}>
                <ScaledImage width={Metrics.screenWidth} uri={Image.resolveAssetSource(IconHomePNG).uri} />
                {this.renderMenu()}
                {/* <TouchableOpacity onPress={() => navigationService.navigate("RequestSTBScreen")}>
                    <Text>Request STB</Text>
                </TouchableOpacity> */}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
