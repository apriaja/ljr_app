// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
});

// Make the styles available for ActivityScreens
export default styles;