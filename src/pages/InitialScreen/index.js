import React, { Component } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'

// Redux Library
import { connect } from 'react-redux';
import { setLoading } from '../../actions';
import { } from '../../actions/types';

// Theme
import styles from './styles'

// Navigation
import navigationService from '../../navigation/navigationService'

// Assets
import IconHomePNG from '../../assets/images/ico.Home.png'

import { Colors, Metrics } from '../../themes'
import { ScaledImage } from '../../components'

import { StackActions, NavigationActions } from 'react-navigation';


export class index extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        this.props.setLoading(true)
        setTimeout(() => {
            this.props.setLoading(false)
            // navigationService.resetNavigation("HomeTab")
            navigationService.resetNavigation("MainScreen")
        }, 2000)
    }


    render() {
        return (
            <View style={[{ flex: 1, backgroundColor: Colors.white }]}>
                {/* <TouchableOpacity onPress={() => navigationService.navigate("RequestSTBScreen")}>
                    <Text>Request STB</Text>
                </TouchableOpacity> */}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
