import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { Button, Header, Tab } from '../../components';

// Redux
import { setLoading } from '../../actions';

// Style
import styles from './styles'

// Constans
import constants from '../../common/constants';

// Assets
import IconSuccess from '../../assets/images/ico.Success.svg'

// Navigation
import navigationService from '../../navigation/navigationService';

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            from: this.props.navigation.getParam('from'),
            text: this.props.navigation.getParam('text') ? this.props.navigation.getParam('text') : "Order Berhasil!!"
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        if (this.state.from === constants.fromScreen.REQUESTSTTB) {
            navigationService.resetNavigation("HomeTab")
        } else if (this.state.from === constants.fromScreen.REQUESTPU) {
            navigationService.resetNavigation("HomeTab")
        } else {
            this.props.navigation.goBack();
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={[styles.justifyAlignCenter, { flex: 1 }]}>
                    <Text style={[styles.txtSuccess, { marginBottom: 40 }]}>{this.state.text}</Text>
                    <IconSuccess />
                </View>
                <View style={{ paddingHorizontal: 4, paddingVertical: 17 }}>
                    <Button width={"100%"} onPress={() => this.onBack()} text="OK" />
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(index)