import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import { FlatList, Text, TouchableOpacity, View, } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconArrowRight from '../../../assets/images/ico.ArrowRight.svg';

const FAQMainScreen = ({ data, onPressItem }) => (
    <View style={{ flex: 1 }}>
        <FlatList
            data={data}
            renderItem={({ item, index }) => (
                <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item)} style={[styles.flexHorizontal, { borderBottomWidth: 2, borderBottomColor: 'rgba(0, 0, 0, 0.2)', alignItems: 'center', paddingLeft: 12, paddingRight: 6, paddingVertical: 10 }]}>
                    <View style={{ flex: 0.9, }}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                    </View>
                    <View style={{ flex: 0.1, justifyContent: 'center', alignItems: 'flex-end' }}>
                        <IconArrowRight />
                    </View>
                </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
)

FAQMainScreen.propTypes = {
    data: PropTypes.array,
    onPressItem: PropTypes.func,
};

export { FAQMainScreen };