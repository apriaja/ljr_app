import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import { Text, TouchableOpacity, View, } from 'react-native';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';

// Component
import { Button, TextInputBase } from '../../../components';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../themes';

// Assets

const EmailMainScreen = ({ onSendPress }) => (
    <Formik
        initialValues={{ subject: "", body: "" }}
        validationSchema={validationSchema}
        validateOnBlur={true}
        onSubmit={(values) => onSendPress(values)}
    >
        {formikProps => (
            <View style={{ flex: 1, paddingHorizontal: 9 }}>
                <Text style={[styles.titleTxt, { marginBottom: 28, marginTop: 9 }]}>{"Masukan kritik dan saran anda"}</Text>
                <TextInputBase
                    styleContainer={{ borderColor: 0 }}
                    placeholder="Subject.."
                    value={formikProps.values.subject}
                    keyboardType={'ascii-capable'}
                    onChangeText={formikProps.handleChange("subject")}
                    onBlur={formikProps.handleBlur("subject")}
                    error={formikProps.touched.subject ? formikProps.errors.subject : null}
                />
                <View style={{ padding: 4 }} />
                <TextInputBase
                    isMultiline={true}
                    styleContainer={{ borderWidth: 0 }}
                    value={formikProps.values.body}
                    placeholder="Text.."
                    keyboardType={'ascii-capable'}
                    onChangeText={formikProps.handleChange("body")}
                    onBlur={formikProps.handleBlur("body")}
                    error={formikProps.touched.body ? formikProps.errors.body : null}
                />
                <View style={{ padding: 6.5 }} />
                <View style={[styles.flexHorizontal, { alignSelf: 'flex-end', marginRight: 10, }]}>
                    <Button onPress={formikProps.handleReset} text="Batal" containerStyle={{ backgroundColor: '#343D53' }} />
                    <View style={{ padding: 4.5 }} />
                    <Button onPress={formikProps.handleSubmit} text="Kirim" />
                </View>
            </View>
        )}
    </Formik>
)

const validationSchema = yup.object().shape({
    subject: yup
        .string()
        .required("Silahkan masukkan subject email")
        .test('isPhone', 'Masukkan subject email dengan benar',
            value => {
                return value
            }),
    body: yup
        .string()
        .required("Silahkan masukkan pesan email")
        .test('isPhone', 'Masukkan pesan email dengan benar',
            value => {
                return value
            }),
})


EmailMainScreen.propTypes = {
    onSendPress: PropTypes.func,
};

export { EmailMainScreen };