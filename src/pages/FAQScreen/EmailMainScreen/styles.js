// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 15,
        letterSpacing: -0.33,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;