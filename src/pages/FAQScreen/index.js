import React, { Component } from 'react'
import { View, Text, BackHandler, TouchableWithoutFeedback } from 'react-native'

// Library
import { connect } from 'react-redux'
import Modal from 'react-native-modal';

// Redux
import { setLoading } from '../../actions';

// Navigation
import navigationService from '../../navigation/navigationService';

// Components
import { Button, Header, Tab } from '../../components';

// Style
import styles from './styles'
import { Colors, Metrics } from '../../themes';


import { EmailMainScreen } from './EmailMainScreen';
import { FAQMainScreen } from './FAQMainScreen';

import IconChecklist from '../../assets/images/ico.ChecklistGreen.svg'

import dummy from '../../common/dummy'


let TAB = [
    { id: 0, title: 'FAQ' },
    { id: 1, title: 'EMAIL' },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 0,
            modalSendEmail: false
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    onSelectedTab = (data) => {
        const setActiveData = {
            '0': () => { this.setState({ page: 0 }) },
            '1': () => { this.setState({ page: 1 }) },
        };
        setActiveData[data.id]();
    }

    renderPage = (page) => {
        let itemToRender = null
        const setActiveData = {
            '0': () => { itemToRender = this.renderFirstPage() },
            '1': () => { itemToRender = this.renderSecondPage() },
        };
        setActiveData[page]();

        return (
            <View style={{ flex: 1, marginTop: 5, marginBottom: 13, borderRadius: 10, backgroundColor: 'rgba(196, 224, 227, 0.52)' }}>
                {itemToRender}
            </View>
        )
    }

    renderFirstPage = () => {
        return (
            <FAQMainScreen
                data={dummy.dataFAQ}
                onPressItem={this.onSelectFAQ}
            />
        )
    }
    renderSecondPage = () => {
        return (
            <EmailMainScreen
                onSendPress={this.sendEmail}
            />
        )
    }

    sendEmail = (values) => {
        console.log("values: ", values)
        this.props.setLoading(true)
        setTimeout(() => {
            this.props.setLoading(false)
            this.setState({ modalSendEmail: true })
        }, 2000)
    }

    onSelectFAQ = (item) => {
        navigationService.navigate("FAQDetailScreen", { data: item })
    }

    onPressModalBtn = () => {
        this.setState({ modalSendEmail: false })
    }

    renderModalSuccess = () => {
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-start' }}
                onModalHide={() => this.onBack()}
                isVisible={this.state.modalSendEmail}>
                <View style={[{ marginTop: Metrics.screenHeight / 4 }]}>
                    <View style={{ alignItems: 'center', marginHorizontal: 70, paddingTop: 7, paddingHorizontal: 35, paddingBottom: 18, borderRadius: 10, backgroundColor: Colors.white }}>
                        <IconChecklist />
                        <Text style={[styles.modalTitleTxt, { alignSelf: 'center', marginBottom: 9, marginTop: 4 }]}>{`Email telah terkirim`}</Text>
                        <View style={[styles.flexHorizontal, { alignSelf: 'center', }]}>
                            <Button width={60} containerStyle={{ paddingVertical: 8 }} onPress={() => this.onPressModalBtn()} text="Ya" />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title={"FAQ & Email"} onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 9 }}>
                    <Tab data={TAB} onSelectTab={this.onSelectedTab} />
                    {this.renderPage(this.state.page)}
                </View>
                {this.renderModalSuccess()}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});


export default connect(mapStateToProps, mapDispatchToProps)(index)
