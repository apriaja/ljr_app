import React, { Component } from 'react'
import { FlatList, Text, TouchableOpacity, View, BackHandler, Image } from 'react-native';

// Library
import { connect } from 'react-redux'

// Navigation
import navigationService from '../../navigation/navigationService';

// Component
import { Button, ScaledImage, Header } from '../../components';

// Style
import styles from './styles';
import { ApplicationStyles, Colors } from '../../themes';
import IconProfileNew from '../../assets/images/ico.BgUser.png'
import IconLogout from '../../assets/images/ico.SignOut.svg';

// Redux
import { setLoading, setProfile } from '../../actions';

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            menu: [
                {
                    icon: <IconLogout height="25" width="25"/>,
                    title: `Log Out`,
                    menu: 'LOGOUT',
                },
            ]
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        navigationService.navigate("HomeScreen")
        // this.props.navigation.goBack();
    }

    onPressLogout = () => {
        console.log("awdawdnjanwdj")
        this.props.setLoading(true)
        this.props.setProfile(null)
        setTimeout(() => {
            this.props.setLoading(false)
            navigationService.resetNavigation("MainScreen")
        }, 2000)
    }

    renderHeader = () => {
        return (
            <View style={{ backgroundColor: Colors.white, paddingTop: ApplicationStyles.headerHeight.paddingTop }}>
                <Text style={styles.berandaTxt}>{'Profil'}</Text>
                <View style={[styles.flexHorizontal, styles.bannerHeader, { marginTop: 5 }]}>
                    <View style={[styles.justifyAlignCenter, { overflow: 'hidden', marginRight: 15, width: 70, height: 70, borderRadius: 70 / 2, borderWidth: 3, borderColor: Colors.white }]}>
                        {/* <IconProfile width={70} height={70} /> */}
                        <ScaledImage width={70} uri={Image.resolveAssetSource(IconProfileNew).uri} />
                    </View>
                    <View style={{ marginTop: 5, flexShrink: 1 }}>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.txtTitle, { marginBottom: 5, flexShrink: 1, flexWrap: 'wrap' }]}>{this.props.profile.name}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.txtDesk, {flexShrink: 1, flexWrap: 'wrap'}]}>{"Perusahaan : " + this.props.profile.name_customer}</Text>
                        </View>
                        <View style={{flexDirection: 'row'}}>
                            <Text style={[styles.txtDesk, { marginBottom: 10,flexShrink: 1, flexWrap: 'wrap' }]}>{"Alamat : " + this.props.profile.address}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }


    renderMenu = () => {
        return (
            <View style={{ backgroundColor: Colors.white, marginTop: 10 }}>
                <FlatList
                    scrollEnabled={false}
                    data={this.state.menu}
                    renderItem={({ item, index }) => {
                        return (
                            <TouchableOpacity onPress={ () => item.menu === 'LOGOUT' ? this.onPressLogout() : this.gotoRateDriver() } style={[ styles.flexHorizontal, { alignItems: 'center', paddingRight: 7, paddingVertical: 10 }]}>
                                <View style={{ ...item.style, paddingLeft: 5, paddingRight: 15 }}>
                                    {item.icon && item.icon}
                                </View>
                                <Text>{item.title}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    ItemSeparatorComponent={() => {
                        return (
                            <View style={{ height: 1, width: '100%', backgroundColor: '#DADADA' }} />
                        )
                    }}
                    keyExtractor={(item, index) => index.toString()}
                />
                <View style={{ height: 1, width: '100%', backgroundColor: '#DADADA' }} />
            </View>
            
        )
    }

    render() {
        return (
            <View style={[styles.headerContainer, { flex: 1, paddingTop: 10, backgroundColor: 'white' }]}>
                {this.renderHeader()}
                {this.renderMenu()}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.persistReducer.profile
    }
}

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
    setProfile: (data) => dispatch(setProfile(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
