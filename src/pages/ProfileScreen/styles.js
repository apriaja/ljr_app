// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    berandaTxt: {
        ...Fonts.poppinsMedium,
        ...Fonts.description,
        color: '#2C2C2C',
        lineHeight: 24,
    },
    taskTxt: {
        ...Fonts.poppinsMedium,
        ...Fonts.subDescription,
        color: '#2C2C2C',
        lineHeight: 24,
    },
    notifTxt: {
        ...Fonts.soraSemiBold,
        ...Fonts.tag,
        color: "rgba(43, 53, 255, 1)",
        lineHeight: 20,
    },
    welcomeTxt: {
        ...Fonts.poppinsMedium,
        ...Fonts.content,
        color: Colors.white,
        lineHeight: 30,
    },
    txt1: {
        ...Fonts.notoSansBold,
        fontSize: 11,
        lineHeight: 15,
        letterSpacing: -0.33,
    },
    txt2: {
        ...Fonts.notoSansBold,
        fontSize: 7,
        lineHeight: 10,
        letterSpacing: -0.33,
    },
    headerContainer: {
        paddingTop: 16,
        paddingHorizontal: 16
    },
    bannerHeader: {
        paddingTop: 8,
        paddingHorizontal: 10,
        paddingBottom: 6,
        backgroundColor: "rgba(88, 124, 244, 1)",
        // justifyContent: 'space-between',
        // alignItems: 'flex-end',
        borderRadius: 4,
    },
    bannerHeaderSecondary: {
        paddingHorizontal: 10,
        backgroundColor: "rgba(196, 224, 227, 0.52)",
        borderRadius: 10,
    },
    menuTitleTxt: {
        ...Fonts.notoSansBold,
        fontSize: 7,
        lineHeight: 10,
        letterSpacing: -0.33,
        color: Colors.white,
        textAlign: 'center',
    },
    menuContainer: {
        flex: 1,
        alignItems: 'flex-start',
        alignSelf: 'stretch',
        justifyContent: 'space-between',
        paddingTop: 6,
        paddingHorizontal: 5,
        paddingBottom: 5,
    },
    notifContainer: {

    },
    notifTitle: {
        ...Fonts.soraSemiBold,
        fontSize: 9,
        lineHeight: 12,
    },
    ratingTxt: {
        ...Fonts.robotoBold,
        fontSize: 13,
        color: Colors.white,
    },
    txtTitle: {
        ...Fonts.soraRegular,
        fontSize: 17,
        lineHeight: 21,
        color: Colors.white,
    },
    txtDesk: {
        ...Fonts.soraRegular,
        fontSize: 11,
        lineHeight: 14,
        color: Colors.white,
    }
});

// Make the styles available for ActivityScreens
export default styles;