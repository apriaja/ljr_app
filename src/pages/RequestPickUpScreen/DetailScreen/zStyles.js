// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    separator: {
        height: 1,
        width: '100%',
        backgroundColor: "rgba(0, 0, 0, 0.18)",
        marginTop: 8,
        marginBottom: 8,
    },
    titleTxt: {
        ...Fonts.notoSansBold,
        fontSize: 11,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    nomorPUTxt: {
        ...Fonts.notoSansBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    alamatTxt: {
        ...Fonts.notoSans,
        ...Fonts.tag,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.66)',
    },
    descTxt: {
        ...Fonts.notoSansBold,
        fontSize: 11,
        lineHeight: 15,
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.66)',
    }
});

// Make the styles available for ActivityScreens
export default styles;