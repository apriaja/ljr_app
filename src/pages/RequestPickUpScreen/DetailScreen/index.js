import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView, SafeAreaView } from 'react-native';
import CardView from 'react-native-cardview';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../themes';

// Component
import { Button, Tab } from '../../../components';
import utils from '../../../common/utils';

// PAGE
import PAGE from '../page';

// Assets
import IconBox from '../../../assets/images/ico.Box.svg';
import IconCars from '../../../assets/images/ico.Cars.svg';
import IconProfile from '../../../assets/images/ico.Profile.svg';


let valueLabel = {
    berat: "Kg",
    volume: "L",
    vallet: 'Palet'
}

const DetailScreen = ({ data, noPU, fromPage }) => {
    useEffect(() => {
    }, []);

    const renderFromOrder = () => {
        return (
            <View>
                <View style={[styles.flexHorizontal, { backgroundColor: "#C4E0E3", borderTopLeftRadius: 10, borderTopRightRadius: 10, justifyContent: 'space-between', paddingTop: 11, paddingBottom: 14, paddingLeft: 11, paddingRight: 8, marginHorizontal: 12 }]}>
                    <Text style={styles.noPuTxt}>{noPU}</Text>
                    {/* <CardView cardElevation={3} cardMaxElevation={3} style={[styles.statusContainer]}>
                        <Text style={styles.statusTxt}>{"onprogress"}</Text>
                    </CardView> */}
                </View>
                <View style={{ marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", paddingVertical: 8, paddingHorizontal: 11 }}>
                    <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                        <View style={[styles.justifyAlignCenter, { width: 10, height: 10, borderWidth: 1, borderRadius: 10 / 2, borderColor: "#DE9034" }]}>
                            <View style={{ width: 6, height: 6, borderRadius: 6 / 2, backgroundColor: '#DE9034' }} />
                        </View>
                        <Text style={[styles.namaTxt, { marginLeft: 8 }]}>{data.alamat.PU.nama}</Text>
                    </View>
                    <Text style={[styles.namaTxt, { fontSize: 7, marginLeft: 18 }]}>{`CP : ${data.alamat.PU.contact}`}</Text>
                    <Text style={[styles.namaTxt, { fontSize: 7, marginLeft: 18 }]}>{data.alamat.PU.alamat}</Text>
                </View>
                <View style={styles.separator} />
                <View style={[styles.flexHorizontal, { marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", justifyContent: 'space-between', paddingLeft: 11, paddingRight: 22, paddingTop: 4, paddingBottom: 3 }]}>
                    <View>
                        <Text style={[styles.jenisTitleTxt, { marginBottom: 5 }]}>{"Armada"}</Text>
                        <View>
                            {
                                data.dataKendaraan.map((value, index) => (
                                    <View key={index} style={styles.flexHorizontal}>
                                        <IconCars />
                                        <View style={[{ marginLeft: 5, justifyContent: 'center' } ]}>
                                            <Text style={styles.detailValueTxt}>{value.jenis}</Text>
                                            <Text style={styles.tglTxt}>{ utils.formatDateTime(value.date, "full-month-time") }</Text>
                                        </View>
                                    </View>
                                ))
                            }
                        </View>
                    </View>
                    <View>
                        <Text style={[styles.jenisTitleTxt, { marginBottom: 5 }]}>{"Jenis Penjemputan"}</Text>
                        <Text style={styles.detailValueTxt}>{data.jenisPengirimian.title}</Text>
                    </View>
                </View>
                <View style={styles.separator} />
                <View style={{ marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderBottomWidth: 1, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: "rgba(0, 0, 0, 0.13)", paddingLeft: 11, paddingRight: 22, paddingTop: 4, paddingBottom: 8 }}>
                    <Text style={[styles.jenisTitleTxt, { marginBottom: 5 }]}>{"Detail Informasi "}</Text>
                    <View style={[styles.flexHorizontal, { justifyContent: 'space-between', }]}>
                        <View style={styles.flexHorizontal}>
                            <IconBox />
                            <View style={{ marginLeft: 11 }}>
                                <Text style={styles.detailValueTxt}>{data.detailInformasi.jenisBarang.nama_barang}</Text>
                                <Text style={styles.detailValueTxt}>{`${data.detailInformasi.nilai} ${valueLabel[data.detailInformasi.satuan ? data.detailInformasi.satuan.toLowerCase() : '']}`}</Text>
                                <Text style={styles.detailValueTxt}>{data.detailInformasi.deskripsi}</Text>
                            </View>
                        </View>
                        {/* <View style={styles.flexHorizontal}>
                            <IconCars />
                            <View style={{ marginLeft: 5 }}>
                                {
                                    data.dataKendaraan.map((value, index) => (
                                        <View key={index}>
                                            <Text style={styles.detailValueTxt}>{value.jenis}</Text>
                                        </View>
                                    ))
                                }
                            </View>
                        </View> */}
                    </View>
                </View>
            </View>
        )
    }

    const renderFromHistory = () => {
        return (
            <View>
                <View style={[styles.flexHorizontal, { backgroundColor: "#C4E0E3", borderTopLeftRadius: 10, borderTopRightRadius: 10, justifyContent: 'space-between', paddingTop: 11, paddingBottom: 14, paddingLeft: 11, paddingRight: 8, marginHorizontal: 12 }]}>
                    <Text style={styles.noPuTxt}>{data.noPU}</Text>
                    <CardView cardElevation={3} cardMaxElevation={3} style={[styles.statusContainer]}>
                        <Text style={styles.statusTxt}>{data.status.replace(/_/g," ")}</Text>
                    </CardView>
                </View>
                <View style={{ marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", paddingVertical: 8, paddingHorizontal: 11 }}>
                    <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                        <View style={[styles.justifyAlignCenter, { width: 10, height: 10, borderWidth: 1, borderRadius: 10 / 2, borderColor: "#DE9034" }]}>
                            <View style={{ width: 6, height: 6, borderRadius: 6 / 2, backgroundColor: '#DE9034' }} />
                        </View>
                        <Text style={[styles.namaTxt, { marginLeft: 8 }]}>{data.alamat.PU.nama}</Text>
                    </View>
                    <Text style={[styles.namaTxt, { fontSize: 7, marginLeft: 18 }]}>{`CP : ${data.alamat.PU.contact}`}</Text>
                    <Text style={[styles.namaTxt, { fontSize: 7, marginLeft: 18 }]}>{data.alamat.PU.alamat}</Text>
                </View>
                <View style={styles.separator} />
                <View style={[styles.flexHorizontal, { marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", justifyContent: 'space-between', paddingLeft: 11, paddingRight: 22, paddingTop: 4, paddingBottom: 3 }]}>
                    <View>
                        <View>
                            <Text style={[styles.jenisTitleTxt, { marginBottom: 3 }]}>{"Armada"}</Text>
                            <View>
                                {
                                    data.dataKendaraan.map((value, index) => (
                                        <View key={index} style={styles.flexHorizontal}>
                                            <IconCars />
                                            <View style={[{ marginLeft: 5, justifyContent: 'center' } ]}>
                                                <Text style={styles.detailValueTxt}>{value.jenis}</Text>
                                                {/* <Text style={styles.tglTxt}>{ utils.formatDateTime(value.date, "full-month-time") }</Text> */}
                                                <Text style={styles.tglTxt}>{ value.date }</Text>
                                            </View>
                                        </View>
                                    ))
                                }
                            </View>
                        </View>
                    </View>
                    <View>
                        <Text style={[styles.jenisTitleTxt, { marginBottom: 3 }]}>{"Jenis Penjemputan"}</Text>
                        <Text style={styles.detailValueTxt}>{data.jenisPengirimian.title}</Text>
                    </View>
                </View>
                <View style={styles.separator} />
                <View style={{ marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", paddingLeft: 11, paddingRight: 22, paddingTop: 4, paddingBottom: 8 }}>
                    <Text style={[styles.jenisTitleTxt, { marginBottom: 3 }]}>{"Detail Informasi "}</Text>
                    <View style={[styles.flexHorizontal, { justifyContent: 'space-between', }]}>
                        <View style={styles.flexHorizontal}>
                            <IconBox />
                            <View style={{ marginLeft: 11 }}>
                                <Text style={styles.detailValueTxt}>{data.detailInformasi.jenisBarang}</Text>
                                <Text style={styles.detailValueTxt}>{`${data.detailInformasi.nilai} ${valueLabel[data.detailInformasi.satuan]}`}</Text>
                                <Text style={styles.detailValueTxt}>{data.detailInformasi.deskripsi}</Text>
                            </View>
                        </View>
                    </View>
                </View>
                <View style={styles.separator} />
                <View style={{ marginHorizontal: 12, borderLeftWidth: 1, borderRightWidth: 1, borderBottomWidth: 1, borderBottomLeftRadius: 10, borderBottomRightRadius: 10, borderColor: "rgba(0, 0, 0, 0.13)", paddingLeft: 11, paddingRight: 22, paddingTop: 4, paddingBottom: 8 }}>
                    <Text style={[styles.jenisTitleTxt, { marginBottom: 3 }]}>{"Informasi Lainnya"}</Text>
                    {
                        data.task.map((value, index) => (
                            <View key={index} style={{ marginBottom: 12 }}>
                                <View style={[styles.flexHorizontal, {justifyContent: 'space-between'}]}>
                                    <View style={{ maxWidth: '40%' }}>
                                        <View style={[styles.flexHorizontal, { alignItems: 'center', marginBottom: 3 }]}>
                                            <IconCars />
                                            <Text style={[styles.detailValueTxt, { marginLeft: 4 }]}>{ `${value.nama_armada} (${value.no_polisi})`}</Text>
                                        </View>
                                        <View style={[styles.flexHorizontal, { alignItems: 'center', marginBottom: 3 }]}>
                                            <IconProfile width={15} height={15} />
                                            <Text style={[styles.tglTxt, { marginLeft: 7 }]}>{ `${value.nama_driver} (driver)` }</Text>
                                        </View>
                                        { value.nama_krani && value.nama_krani !== '' && (
                                            <View style={[styles.flexHorizontal, { alignItems: 'center', marginBottom: 5 }]}>
                                                <IconProfile width={15} height={15} />
                                                <Text style={[styles.tglTxt, { marginLeft: 7 }]}>{ `${value.nama_krani} (krani)` }</Text>
                                            </View>
                                        )}
                                        { value.keterangan && value.keterangan !== '' && (
                                            <View style={{ flexDirection: 'column' }}>
                                                <Text style={[styles.detailValueTxt]}>Catatan Lainnya</Text>
                                                <Text style={styles.tglTxt}>{ value.keterangan }</Text>
                                            </View>
                                        )}
                                    </View>
                                    <View>
                                        <CardView cardElevation={3} cardMaxElevation={3} style={[styles.statusContainer, { alignSelf: 'baseline' }]}>
                                            <Text style={[styles.statusTxt, { color: Colors.StatusTxtColor[value.status] }]}>{value.status.replace(/_/g," ")}</Text>
                                        </CardView>
                                    </View>
                                </View>
                            </View>
                        ))
                    }
                </View>
            </View>
        )
    }

    return (
        <View>
            {
                fromPage === PAGE.ORDERPICKUP ? renderFromOrder() :
                    fromPage === PAGE.MAIN ? renderFromHistory() :
                        null
            }
        </View>
    )
}

DetailScreen.propTypes = {
    data: PropTypes.object,
    noPU: PropTypes.string,
    fromPage: PropTypes.string,
};

export { DetailScreen };