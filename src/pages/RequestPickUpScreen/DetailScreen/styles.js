// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    statusContainer: {
        paddingVertical: 3,
        paddingHorizontal: 23,
        backgroundColor: Colors.white,
        borderRadius: 7,
    },
    statusTxt: {
        ...Fonts.poppinsMedium,
        ...Fonts.subTag,
        textTransform: 'uppercase',
        letterSpacing: 0.05,
        color: "#0066FF",
    },
    noPuTxt: {
        ...Fonts.soraRegular,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    namaTxt: {
        ...Fonts.soraRegular,
        fontSize: 8,
        lineHeight: 12,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    separator: {
        width: '100%',
        height: 3,
        backgroundColor: "#DADADA",
    },
    jenisTitleTxt: {
        ...Fonts.soraRegular,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    tglTxt: {
        ...Fonts.soraRegular,
        fontSize: 8,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    detailValueTxt: {
        ...Fonts.poppinsMedium,
        fontSize: 9,
        lineHeight: 13,
        color: '#6E6E6E',
    }
});

// Make the styles available for ActivityScreens
export default styles;