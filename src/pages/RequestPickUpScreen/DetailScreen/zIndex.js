import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView, SafeAreaView } from 'react-native';

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconArrowTop from '../../../assets/images/ico.ArrowTop.svg';

// PAGE
import PAGE from '../page';

const DetailScreen = ({ data, onPressRequest, onPressBatal }) => {
    const [page, setPage] = useState(0);

    useEffect(() => {
        console.log("data: ", data)
    }, []);

    const renderAlamat = () => {
        return (
            <FlatList
                scrollEnabled={false}
                style={{ flexGrow: 0 }}
                data={data.alamat ? data.alamat : []}
                renderItem={({ item, index }) => (
                    <View style={{ marginBottom: 3 }}>
                        <Text style={styles.titleTxt}>{item.nama}</Text>
                        <Text style={[styles.alamatTxt, { marginTop: 4, marginBottom: 3 }]}>{item.alamat}</Text>
                        <Text style={styles.nomorPUTxt}>{"CP: "} <Text style={styles.alamatTxt}>{item.contact}</Text></Text>
                    </View>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    const renderKendaraan = () => {
        return (
            <FlatList
                scrollEnabled={false}
                style={{ flexGrow: 0 }}
                data={data.dataKendaraan ? data.dataKendaraan : []}
                renderItem={({ item, index }) => (
                    <View style={[styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between' }]}>
                        <Text style={styles.descTxt}>{item.jenis}</Text>
                        <Text style={styles.descTxt}>{`${item.jumlah} x`}</Text>
                    </View>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        )
    }

    return (
        <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
            <View style={{ backgroundColor: 'rgba(196, 224, 227, 0.52)', borderRadius: 10, paddingHorizontal: 16, paddingVertical: 10 }}>
                <View>
                    <Text style={styles.titleTxt}>{"Nomor pickup:"}</Text>
                    <Text style={[styles.nomorPUTxt, { marginTop: 3, marginBottom: 9 }]}>{"PUG0000001"}</Text>
                    {renderAlamat()}
                </View>
                <View style={styles.separator} />
                <View>
                    <Text style={[styles.titleTxt, { marginBottom: 3 }]}>{"Jenis penjemputan"}</Text>
                    {
                        data.jenisPengirimian &&
                        <Text style={[styles.descTxt, { marginTop: 3 }]}>{data.jenisPengirimian.title}</Text>
                    }
                </View>
                <View style={styles.separator} />
                <View>
                    <Text style={[styles.titleTxt, { marginBottom: 3 }]}>{"Mobil pesanan anda"}</Text>
                    {renderKendaraan()}
                </View>
                <View style={styles.separator} />
                <View>
                    <Text style={[styles.titleTxt, { marginBottom: 3 }]}>{"Detail informasi barang"}</Text>
                    {
                        data.detailInformasi &&
                        <>
                            <Text style={[styles.descTxt, { marginBottom: 4 }]}>{data.detailInformasi.jenisBarang}</Text>
                            <Text style={[styles.descTxt, { marginBottom: 4 }]}>{`${data.detailInformasi.berat} Kg`}</Text>
                            <Text style={[styles.descTxt, { marginBottom: 4 }]}>{`${data.detailInformasi.volume} L`}</Text>
                            <Text style={[styles.descTxt]}>{data.detailInformasi.deskripsi}</Text>
                        </>
                    }
                </View>
            </View>
            <View style={[styles.flexHorizontal, { alignSelf: 'flex-end', marginTop: 7, marginBottom: 25 }]}>
                <Button onPress={() => onPressBatal()} text="Batal" width={71} containerStyle={{ backgroundColor: '#343D53', paddingVertical: 8 }} />
                <View style={{ padding: 3 }} />
                <Button onPress={() => onPressRequest(PAGE.DETAIL, {})} text="Order" width={71} containerStyle={{ paddingVertical: 8 }} />
            </View>
        </ScrollView>
    )
}

DetailScreen.propTypes = {
    data: PropTypes.object,
    onPressRequest: PropTypes.func,
    onPressBatal: PropTypes.func,
};

export { DetailScreen };