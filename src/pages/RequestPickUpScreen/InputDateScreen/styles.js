// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    titleTxt: {
        ...Fonts.robotoRegular,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    jenisKendaraanTxt: {
        ...Fonts.notoSansBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    jumlahTxt: {
        ...Fonts.notoSans,
        fontSize: 10,
        fontWeight: '600',
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.57)',
    },
    selectedDateTxt: {
        ...Fonts.robotoRegular,
        fontSize: 13,
        letterSpacing: -0.333,
        color: '#281BBD',
    },
    btnDoneTxt: {
        ...Fonts.robotoBold,
        ...Fonts.description,
        letterSpacing: 0.04,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;