import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView, TouchableWithoutFeedback } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Modal from 'react-native-modal';

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconClock from '../../../assets/images/ico.Clock.svg';
import IconCalendar from '../../../assets/images/ico.Calendar.svg';

// PAGE
import PAGE from '../page';
import { Colors } from '../../../themes';
import utils from '../../../common/utils';

const InputDateScreen = ({ data, onPressRequest }) => {
    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [selectedDate, setSelectedDate] = useState("")
    const [selectedTime, setSelectedTime] = useState("")
    const [showModalDate, setShowModalDate] = useState(false)

    useEffect(() => {

    }, []);

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        if (mode === "date") {
            setSelectedDate(utils.formatDateTime(currentDate, "month-dash"))
        } else if (mode === "time") {
            setSelectedTime(utils.formatDateTime(currentDate, "time"))
        }
        setShow(false)
        setDate(currentDate);
    };

    const showDatepicker = () => {
        setMode('date');
        if (utils.isIOS()) {
            setShowModalDate(true)
        } else {
            setShow(true);
        }
    };

    const showTimepicker = () => {
        setMode('time');
        if (utils.isIOS()) {
            setShowModalDate(true)
        } else {
            setShow(true);
        }
    };

    const checkKendaraan = () => {
        let temp = data && data.dataKendaraan ? data.dataKendaraan : []
        var totalKendaraan = temp.reduce(function (prev, cur) {
            return prev + cur.jumlah;
        }, 0);
        return totalKendaraan
    }

    const renderModalIOS = () => {
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback onPress={() => setShowModalDate(false)}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-end', margin: 0, }}
                isVisible={showModalDate}>
                <View style={[{ backgroundColor: Colors.white, borderTopLeftRadius: 10, borderTopRightRadius: 10 }]}>
                    <View style={{ alignItems: 'flex-end' }}>
                        <TouchableOpacity activeOpacity={1} onPress={() => { onChange(); setShowModalDate(false) }} style={{ paddingVertical: 10, paddingHorizontal: 12, marginRight: 10 }}>
                            <Text style={[styles.btnDoneTxt]}>{"Done"}</Text>
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        display={'spinner'}
                        onChange={onChange}

                    />
                </View>
            </Modal>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ paddingHorizontal: 12, paddingVertical: 16, backgroundColor: 'rgba(196, 224, 227, 0.52)', borderRadius: 10, marginBottom: 10 }}>
                <TouchableOpacity activeOpacity={1} onPress={() => showDatepicker()} style={[styles.flexHorizontal, { alignItems: 'center', borderRadius: 10, borderWidth: 1, borderColor: Colors.black, backgroundColor: Colors.white, paddingVertical: 9, paddingHorizontal: 10, marginBottom: 10 }]}>
                    {
                        selectedDate == "" ?
                            <>
                                <IconCalendar />
                                <Text style={[styles.titleTxt, { marginLeft: 6 }]}>{"Pilih Tanggal Penjemputan"}</Text>
                            </> :
                            <Text style={styles.selectedDateTxt}>{selectedDate}</Text>
                    }
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={1} onPress={() => showTimepicker()} style={[styles.flexHorizontal, { alignItems: 'center', borderRadius: 10, borderWidth: 1, borderColor: Colors.black, backgroundColor: Colors.white, paddingVertical: 9, paddingHorizontal: 10 }]}>
                    {
                        selectedTime == "" ?
                            <>
                                <IconClock />
                                <Text style={[styles.titleTxt, { marginLeft: 6 }]}>{"Pilih Waktu Penjemputan"}</Text>
                            </> :
                            <Text style={styles.selectedDateTxt}>{selectedTime}</Text>
                    }
                </TouchableOpacity>
            </View>
            <View style={{ paddingHorizontal: 12, paddingVertical: 17, backgroundColor: 'rgba(196, 224, 227, 0.52)', borderRadius: 10, marginBottom: 10 }}>
                <FlatList
                    data={data && data.dataKendaraan ? data.dataKendaraan : []}
                    renderItem={({ item, index }) => (
                        <View style={[styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between' }]}>
                            <Text style={styles.jenisKendaraanTxt}>{item.jenis}</Text>
                            <Text style={styles.jumlahTxt}>{`${item.jumlah} x`}</Text>
                        </View>
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={{ padding: 3 }} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
            <View style={[styles.flexHorizontal, { alignItems: 'center', paddingHorizontal: 7, justifyContent: 'space-between' }]}>
                <Text>{`Total Kendaraan Dipesan : ${checkKendaraan()}`}</Text>
                <Button width={100} containerStyle={{ paddingVertical: 8 }} disabled={!(selectedDate != "" && selectedTime != "")} onPress={() => onPressRequest(PAGE.PENJEMPUTAN, date)} text="Pesan" />
            </View>
            {renderModalIOS()}
            {
                show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        display={"default"}
                        onChange={onChange}
                    />
                )
            }
        </View>
    )
}

InputDateScreen.propTypes = {
    data: PropTypes.object,
    onPressRequest: PropTypes.func,
};

export { InputDateScreen };