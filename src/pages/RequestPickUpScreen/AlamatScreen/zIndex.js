import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import CardView from 'react-native-cardview';
import _ from "lodash";

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconChecklist from '../../../assets/images/ico.checkList.svg';

// PAGE
import PAGE from '../page';
import dummy from '../../../common/dummy';
import { Colors } from '../../../themes';

const AlamatScreen = ({ data, onPressRequest }) => {
    const [dataAlamat, setDataAlamat] = useState([]);
    const [selectedAlamat, setSelectedAlamat] = useState({})

    useEffect(() => {
        let temp = dummy.alamat.map(elm => ({ ...elm, selected: false }))
        setDataAlamat(temp)
    }, []);

    const onSelecAlamat = (data, index) => {
        let temp = [...dataAlamat]
        temp[index].selected = !dataAlamat[index].selected
        setDataAlamat(temp)
    }

    const check = () => {
        let temp = _.filter(dataAlamat, ['selected', true]);
        return temp
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <FlatList
                    data={dataAlamat}
                    renderItem={({ item, index }) => (
                        <TouchableOpacity activeOpacity={1} onPress={() => onSelecAlamat(item, index)} style={[styles.containerItem]}>
                            <Text style={[styles.titleTxt, { marginBottom: 10, }]}>{item.nama}</Text>
                            <Text style={styles.alamatTxt}>{item.alamat}</Text>
                            <View style={[styles.flexHorizontal, { justifyContent: 'space-between', alignItems: 'center', marginTop: 5 }]}>
                                <Text style={styles.contactTxt}><Text style={styles.titleTxt}>{"CP : "}</Text>{item.contact}</Text>
                                <CardView cardElevation={3} cardMaxElevation={3} style={[styles.justifyAlignCenter, { height: 21, width: 21, borderRadius: 6, backgroundColor: Colors.white }]}>
                                    {
                                        item.selected && <IconChecklist />
                                    }
                                </CardView>
                            </View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={dataAlamat}
                />
            </View>
            <View style={{ paddingBottom: 10 }}>
                {
                    check().length > 0 &&
                    <View style={{ marginBottom: 5, marginTop: 5, padding: 14, borderRadius: 6, backgroundColor: '#C4E0E3' }}>
                        <Text style={styles.selectedTxt}>{"Total alamat yang terpilih : " + check().length}</Text>
                    </View>
                }
                <Button disabled={check().length <= 0} width={'100%'} onPress={() => onPressRequest(PAGE.ALAMAT, _.filter(dataAlamat, ['selected', true]))} text="Proses" />
            </View>
        </View>
    )
}

AlamatScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
};

export { AlamatScreen };