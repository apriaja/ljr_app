import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import CardView from 'react-native-cardview';
import _ from "lodash";

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';
import { WarningPopUp } from '../../../components/WarningPopUp';

// Assets
import IconChecklist from '../../../assets/images/ico.checkList.svg';

import dummy from '../../../common/dummy';
import { Colors } from '../../../themes';

// Page
import PAGE from '../page';

import IconAddressGreen from '../../../assets/images/ico.AddressGreen.svg';
import IconAddressRed from '../../../assets/images/ico.AddressRed.svg';


const AlamatScreen = ({ data, dataAlamat, onPressRequest, onPressPU, onPressDROP }) => {
    const [showWarning, setShowWarning] = useState(false);

    const handleOnPressRequest = () => {
        if (!onlyPU) {
            if (dataAlamat.PU === null) {
                setShowWarning(true);
            } else {
                onPressRequest(PAGE.ALAMAT, dataAlamat)
            }
        } else {
            onPressRequest(PAGE.ALAMAT, dataAlamat)
        }
    }

    let onlyPU = data.jenisPengirimian && data.jenisPengirimian.title && data.jenisPengirimian.title == "Instant Service" ? true : false
    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <CardView cardElevation={3} cardMaxElevation={3} style={[styles.containerItem]}>
                    <TouchableOpacity onPress={() => onPressPU()} activeOpacity={1}>
                        { 
                            dataAlamat.PU == null ?
                                <View style={[styles.flexHorizontal, { alignItems: 'center', paddingVertical: 13 }]}>
                                    <IconAddressRed width={12} height={12} />
                                    <Text style={[styles.itemTxt, { marginLeft: 8 }]}>{"Pickup di mana?"}</Text>
                                </View> :
                                <View style={{ paddingTop: 13, paddingBottom: 6 }}>
                                    <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                        <IconAddressRed width={12} height={12} />
                                        <Text style={[styles.itemTxt, { marginLeft: 5, color: 'rgba(169, 28, 28, 1)' }]}>{"Pickup"}</Text>
                                    </View>
                                    <View style={{ paddingLeft: 17, marginTop: 7 }}>
                                        <Text style={styles.titleTxt}>{dataAlamat.PU.nama}</Text>
                                        <Text style={[styles.alamatTxt, { marginTop: 3, marginBottom: 3 }]}>{dataAlamat.PU.alamat}</Text>
                                        <Text style={styles.contactTxt}>{`CP:${dataAlamat.PU.contact}`}</Text>
                                    </View>
                                </View>
                        }
                    </TouchableOpacity>
                    {
                        onlyPU &&
                        <>
                            <View style={{ height: 1, width: '100%', backgroundColor: 'rgba(0, 0, 0, 0.2)' }} />
                            <TouchableOpacity onPress={() => onPressDROP()} activeOpacity={1}>
                                {
                                    dataAlamat.DROP == null ?
                                        <View style={[styles.flexHorizontal, { alignItems: 'center', paddingVertical: 13 }]}>
                                            <IconAddressGreen width={12} height={12} />
                                            <Text style={[styles.itemTxt, { marginLeft: 8 }]}>{"Drop Point di mana?"}</Text>
                                        </View> :
                                        <View style={{ paddingTop: 6, paddingBottom: 13 }}>
                                            <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                                <IconAddressGreen width={12} height={12} />
                                                <Text style={[styles.itemTxt, { marginLeft: 5, color: 'background: rgba(33, 173, 31, 1)' }]}>{"Drop Point"}</Text>
                                            </View>
                                            <View style={{ paddingLeft: 17, marginTop: 7 }}>
                                                <Text style={styles.titleTxt}>{dataAlamat.DROP.nama}</Text>
                                                <Text style={[styles.alamatTxt, { marginTop: 3, marginBottom: 3 }]}>{dataAlamat.DROP.alamat}</Text>
                                                <Text style={styles.contactTxt}>{`CP:${dataAlamat.DROP.contact}`}</Text>
                                            </View>
                                        </View>
                                }
                            </TouchableOpacity>
                        </>
                    }
                </CardView>
            </View>
            <View style={{ paddingLeft: 3, paddingRight: 5, paddingVertical: 17, backgroundColor: Colors.white }} >
                <Button width={'100%'} onPress={() => handleOnPressRequest()} text="Berikutnya" />
            </View>

            {
                showWarning && (
                    <View>
                        <WarningPopUp modalVisible={showWarning} setShowWarning={setShowWarning} textWarning={ 'Alamat pickup belum dimasukan' } />
                    </View>
                )
            }
        </View>
    )
}

AlamatScreen.propTypes = {
    data: PropTypes.object,
    onPressPU: PropTypes.func,
    dataAlamat: PropTypes.object,
    onPressDROP: PropTypes.func,
    onPressRequest: PropTypes.func,
};

export { AlamatScreen };