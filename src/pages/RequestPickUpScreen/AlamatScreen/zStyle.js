// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    containerItem: {
        flex: 1,
        paddingTop: 7,
        paddingLeft: 10,
        paddingBottom: 14,
        paddingRight: 12,
        marginBottom: 7,
        backgroundColor: 'rgba(196, 224, 227, 0.52)',
        borderRadius: 10,
    },
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 13,
        fontWeight: '500',
        lineHeight: 18,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    alamatTxt: {
        ...Fonts.notoSans,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    contactTxt: {
        ...Fonts.notoSans,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    selectedTxt: {
        ...Fonts.notoSans,
        fontSize: 13,
        fontWeight: '600',
        lineHeight: 18,
        letterSpacing: -0.333,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;