// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    containerItem: {
        marginLeft: 5,
        marginRight: 6,
        paddingLeft: 10,
        paddingRight: 7,
        paddingTop: 3,
        paddingBottom: 5,
        backgroundColor: Colors.white
    },
    itemTxt: {
        ...Fonts.soraBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: "rgba(0, 0, 0, 0.56)",
    },
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 13,
        fontWeight: '500',
        lineHeight: 18,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    alamatTxt: {
        ...Fonts.notoSans,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    contactTxt: {
        ...Fonts.notoSans,
        fontSize: 12,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
});

// Make the styles available for ActivityScreens
export default styles;