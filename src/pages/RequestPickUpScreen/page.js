const PAGE = {
    MAIN: 'Request Pickup',
    ALAMAT: 'Request Pickup ',
    ORDERPICKUP: "Order PickUp", 
    INPUTINFORMASI: 'Detail Informasi Alamat',
    KENDARAAN: 'Pilih Tipe Kendaraan Alamat',
    PENJEMPUTAN: 'Pilih Waktu Penjemputan',
    DETAIL: 'Detail Request Pickup',
    LISTALAMAT: "List Alamat",
    ADDALAMAT: "Add Alamat",
}
export default PAGE;