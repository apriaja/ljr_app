import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import CardView from 'react-native-cardview';
import _ from "lodash";

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconChecklist from '../../../assets/images/ico.checkList.svg';

import dummy from '../../../common/dummy';
import { Colors } from '../../../themes';

// Page
import PAGE from '../page';

import IconAddressGreen from '../../../assets/images/ico.AddressGreen.svg';
import IconAddressRed from '../../../assets/images/ico.AddressRed.svg';
import { WarningPopUp } from '../../../components/WarningPopUp';

let temp = [
    { title: 'Tambah alamat pickup untuk membuat order pickup' },
    { title: '1 alamat pickup berisi 1 informasi kebutuhan pickup' },
    { title: 'Customer pada melakukan multi pickup untuk 1 nomor pickup' },
]

const OrderPickUp = ({ data, noPU, addAddress, onPressBack, onPressOrder, onPressDetail }) => {
    const [showWarning, setShowWarning] = useState(false);

    const renderEmptyData = () => {
        return (
            <View style={{ backgroundColor: 'background: rgba(196, 224, 227, 0.52)' }}>
                <FlatList
                    contentContainerStyle={{ paddingLeft: 10, paddingRight: 15, paddingTop: 8, paddingBottom: 10 }}
                    data={temp}
                    scrollEnabled={false}
                    renderItem={({ item, index }) => (
                        <View key={index} style={[styles.flexHorizontal]}>
                            <View style={{ flex: 0.1, alignItems: 'center', paddingTop: 5 }}>
                                <View style={{ width: 6, height: 6, borderRadius: 6 / 2, backgroundColor: "#FF7675" }} />
                            </View>
                            <View style={{ flex: 0.9 }}>
                                <Text style={styles.noPUValueTxt}>{item.title}</Text>
                            </View>
                        </View>
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={{ paddingBottom: 7 }} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }

    const renderList = () => {
        return (
            <View style={{}}>
                <FlatList
                    data={data}
                    scrollEnabled={false}
                    renderItem={({ item, index }) => (
                        <View key={index} style={{ backgroundColor: 'background: rgba(196, 224, 227, 0.52)', paddingLeft: 37, paddingRight: 15, paddingTop: 11, paddingBottom: 19 }}>
                            <View style={[styles.flexHorizontal, { justifyContent: 'space-between' }]}>
                                <View>
                                    <Text style={[styles.titleListTxt, { marginBottom: 6 }]}>{`Alamat ${index + 1}`}</Text>
                                    <Text style={[styles.titleListValue, { marginBottom: 4 }]}>{item.alamat.PU.nama}</Text>
                                </View>
                                <View>
                                    <Button width={80} textStyle={styles.detailTxt} containerStyle={{ borderRadius: 8.25, paddingVertical: 9, backgroundColor: "rgba(0, 102, 255, 1)" }} onPress={() => onPressDetail(item)} text="Detail" />
                                </View>
                            </View>
                            <Text style={[styles.titleListTxt, { marginBottom: 5 }]}>{`Jenis Penjemputan`}</Text>
                            <Text style={[styles.titleListValue, { marginBottom: 5 }]}>{item.jenisPengirimian.title}</Text>
                        </View>
                    )}
                    ItemSeparatorComponent={() => (
                        <View style={{ paddingBottom: 6 }} />
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }

    const checkOnPressOrder = () => {
        if (data && data.length > 0) {
            onPressOrder(data);
        } else {
            setShowWarning(true);
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                {
                    data && data.length > 0 ? renderList() : renderEmptyData()
                }
            </View>
            <View style={[{ alignContent: "flex-end" }]}>
                <View style={[styles.containerBtn]}>
                    <TouchableOpacity onPress={() => addAddress()} activeOpacity={1} style={{ paddingHorizontal: 10, paddingVertical: 10, justifyContent: 'center', alignSelf: 'flex-end', marginBottom: 6, marginRight: 25 }}>
                        <Text style={[styles.addAddress]}>{"+Masukan Permintaan"}</Text>
                    </TouchableOpacity>
                    <View style={[styles.flexHorizontal, { overflow: 'hidden', borderRadius: 5 }]}>
                        <TouchableOpacity onPress={() => onPressBack()} activeOpacity={1} style={[styles.justifyAlignCenter, { flex: 0.5, backgroundColor: "rgba(117, 115, 115, 0.57)", paddingHorizontal: 10, paddingVertical: 12 }]}>
                            <Text style={[styles.btnTxt, { color: Colors.white }]}>{"Cancel"}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => checkOnPressOrder()} activeOpacity={1} style={[styles.justifyAlignCenter, { flex: 0.5, backgroundColor: "#2C4074", paddingHorizontal: 10, paddingVertical: 12 }]}>
                            <Text style={[styles.btnTxt, { color: Colors.white }]}>{"Order Sekarang"}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
            {
                showWarning && (
                    <View>
                        <WarningPopUp modalVisible={showWarning} setShowWarning={setShowWarning} textWarning={ 'Masukan alamat untuk melakukan order pickup' } />
                    </View>
                )
            }
        </View>
    )
}

OrderPickUp.propTypes = {
    data: PropTypes.array,
    noPU: PropTypes.string,
    addAddress: PropTypes.func,
    onPressBack: PropTypes.func,
    onPressOrder: PropTypes.func,
    onPressDetail: PropTypes.func,
};

export { OrderPickUp };