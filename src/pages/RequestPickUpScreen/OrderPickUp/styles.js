// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    containerBtn: {
        paddingTop: 9,
        paddingBottom: 15,
        paddingHorizontal: 10,
        backgroundColor: 'rgba(230, 237, 250, 0.46)',
        borderTopLeftRadius: 31,
        borderTopRightRadius: 31,
    },
    btnTxt: {
        ...Fonts.robotoBold,
        fontSize: 13,
        lineHeight: 15,
        letterSpacing: -0.333,
        textAlign: 'center',
    },
    addAddress: {
        ...Fonts.robotoRegular,
        fontSize: 13,
        color: '#0066FF',
        textAlign: 'right',
        textDecorationLine: 'underline',
        textDecorationColor: '#0066FF',
    },
    noPUTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 11,
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.4)',
    },
    noPUValueTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    titleListTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 11,
        letterSpacing: -0.333,
        color: "rgba(0, 0, 0, 0.4)",
    },
    titleListValue: {
        ...Fonts.soraSemiBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    detailTxt: {
        ...Fonts.robotoRegular,
        fontSize: 11,
        color: Colors.white,
    }
});

// Make the styles available for ActivityScreens
export default styles;