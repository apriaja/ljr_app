import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import CardView from 'react-native-cardview';
import _ from 'lodash';

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconKendaraan from '../../../assets/images/ico.Kendaraan.svg';
import IconBtnKendaraan from '../../../assets/images/ico.BtnKendaraan.svg';
import IconIncrement from '../../../assets/images/ico.Increment.svg';
import IconDecrement from '../../../assets/images/ico.Decrement.svg';

// PAGE
import PAGE from '../page';
import dummy from '../../../common/dummy';

const KendaraanScreen = ({ data, onPressRequest }) => {
    const [dataKendaraan, setDataKendaraan] = useState([]);
    const [selectedAlamat, setSelectedAlamat] = useState({})

    useEffect(() => {
        let temp = dummy.jenisKendaraan.map(elm => ({ ...elm, jumlah: 0 }))
        setDataKendaraan(temp)
    }, []);

    const incrementKendaraan = (data, index) => {
        let temp = [...dataKendaraan]
        if (data.id === temp[index].id) {
            temp[index].jumlah++
        }
        setDataKendaraan(temp)
    }

    const decrementKendaraan = (data, index) => {
        let temp = [...dataKendaraan]
        if (data.id === temp[index].id) {
            if (temp[index].jumlah > 0) {
                temp[index].jumlah--
            } else {
                temp[index].jumlah = 0
            }
        }
        setDataKendaraan(temp)
    }

    const getKendaraan = () => {
        let temp = _.filter(dataKendaraan, (item) => { return item.jumlah > 0; });
        return temp
    }

    const check = () => {
        var totalKendaraan = dataKendaraan.reduce(function (prev, cur) {
            return prev + cur.jumlah;
        }, 0);
        return totalKendaraan
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <FlatList
                    data={dataKendaraan}
                    renderItem={({ item, index }) => (
                        <View style={{ backgroundColor: 'rgba(196, 224, 227, 0.52)', borderRadius: 10, marginBottom: 8 }}>
                            <View style={[styles.flexHorizontal, { flex: 1, paddingHorizontal: 9, paddingVertical: 21 }]}>
                                <IconKendaraan />
                                <View style={[{ flexGrow: 1, justifyContent: 'space-between', marginLeft: 15 }]}>
                                    <Text style={styles.jenisKendaraanTxt}>{item.jenis}</Text>
                                    <View style={{ alignSelf: 'stretch', alignItems: 'flex-end', marginTop: -10, marginBottom: -10 }}>
                                        {
                                            item.jumlah > 0 ?
                                                <CardView cardElevation={3} cardMaxElevation={3} style={[styles.flexHorizontal, styles.menuInDec, { alignItems: 'center' }]}>
                                                    <TouchableOpacity activeOpacity={1} onPress={() => decrementKendaraan(item, index)} style={{ marginTop: -12, marginBottom: -12, marginHorizontal: -10, padding: 12 }}>
                                                        <IconDecrement />
                                                    </TouchableOpacity>
                                                    <Text style={[styles.counterTxt, { marginHorizontal: 10 }]}>{item.jumlah}</Text>
                                                    <TouchableOpacity activeOpacity={1} onPress={() => incrementKendaraan(item, index)} style={{ marginTop: -10, marginBottom: -10, marginHorizontal: -10, padding: 10 }}>
                                                        <IconIncrement />
                                                    </TouchableOpacity>
                                                </CardView> :
                                                <Button textStyle={{ fontSize: 11 }} width={60} containerStyle={{ paddingVertical: 8 }} onPress={() => incrementKendaraan(item, index)} text={"Tambah"} />
                                        }
                                    </View>
                                </View>
                            </View>
                            <View style={{ height: 1, width: '100%', backgroundColor: 'rgba(0, 0, 0, 0.18)' }} />
                            <View style={{ paddingHorizontal: 8, marginBottom: 13 }}>
                                <Text style={styles.kapasitasTxt}>{`Kapasitas Truck: ${item.kapasitas}`}</Text>
                            </View>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={dataKendaraan}
                />
            </View>
            <View style={{ paddingBottom: 10, paddingTop: 10 }}>
                <Button disabled={check() <= 0} btnImage={<IconBtnKendaraan />} width={'100%'} onPress={() => onPressRequest(PAGE.KENDARAAN, getKendaraan())} text={`Total Kendaraan Dipesan : ${check()}`} />
            </View>
        </View>
    )
}

KendaraanScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
};

export { KendaraanScreen };