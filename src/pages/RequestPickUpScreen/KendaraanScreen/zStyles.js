// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    jenisKendaraanTxt: {
        ...Fonts.notoSans,
        fontSize: 13,
        fontWeight: '600',
        letterSpacing: -0.333,
        color: Colors.black,
    },
    kapasitasTxt: {
        ...Fonts.notoSans,
        ...Fonts.subTag,
        fontWeight: '500',
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.57)',
    },
    menuInDec: {
        backgroundColor: Colors.white,
        borderRadius: 6,
        padding: 6,
    },
    counterTxt: {
        ...Fonts.notoSans,
        ...Fonts.subTag,
        fontWeight: '600',
        letterSpacing: -0.333,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;