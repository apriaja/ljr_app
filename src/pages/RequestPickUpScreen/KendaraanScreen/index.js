import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, TouchableWithoutFeedback, Platform, Alert } from 'react-native';
import CardView from 'react-native-cardview';
import _ from 'lodash';
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';
import { useSelector, useDispatch } from 'react-redux';

import { setLoading } from '../../../actions';

import api from '../../../services/api';
import Config from 'react-native-config';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../themes';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconKendaraan from '../../../assets/images/ico.Kendaraan.svg';
import IconBtnKendaraan from '../../../assets/images/ico.BtnKendaraan.svg';
import IconIncrement from '../../../assets/images/ico.Increment.svg';
import IconDecrement from '../../../assets/images/ico.Decrement.svg';
import IconCalendar from '../../../assets/images/ico.Calendar2.svg';

import IconKendaraanCarry from '../../../assets/images/mbl_carry_box.svg';
import IconKendaraanCddLongBox from '../../../assets/images/mbl_cdd-long-box.svg';
import IconKendaraanCddStandartBox from '../../../assets/images/mbl_cdd-standart-box.svg';
import IconKendaraanCdeLongBox from '../../../assets/images/mbl_cde-long-box.svg';
import IconKendaraanTrailer from '../../../assets/images/mbl_trailer.svg';
import IconKendaraanTronton from '../../../assets/images/mbl_tronton.svg';
import IconKendaraanColtDieselDouble from '../../../assets/images/mbl_coltdieseldouble.svg';
import IconKendaraanFuso from '../../../assets/images/mbl_fuso.svg';
import IconKendaraanWingBox from '../../../assets/images/mbl_wingbox.svg';

//import IconKendaraanTronton from '../../../assets/images/ico.Kendaraan.svg';


// PAGE
import PAGE from '../page';

// Utils
import utils from '../../../common/utils';

import dummy from '../../../common/dummy';

const KendaraanScreen = ({ data, onPressRequest }) => {
    const [dataKendaraan, setDataKendaraan] = useState([]);
    const [currentItem, setCurrentItem] = useState({});
    const [currentIndex, setCurrentIndex] = useState(0);


    const [date, setDate] = useState(new Date());
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [showModalDate, setShowModalDate] = useState(false)
    
    const dispatch = useDispatch();

    useEffect(() => {
        // let temp = dummy.jenisKendaraan.map(elm => ({ ...elm, jumlah: 0, date: new Date() }))
        // setDataKendaraan(temp)
        async function fetchData() {
            if (dataKendaraan.length == 0) {
                dispatch(setLoading(true))
                const response = await api.fetchAPI(`${Config.API_NGROK}/armada/showAvailableArmadas`, null, 'POST')
                if (response) {
                    setDataKendaraan(response.data.data
                        .filter((item) => item.stock > 0)
                        .map((item) => {
                            return {
                                id: item.id,
                                jenis: item.nama_tipe,
                                kapasitas: item.tonase_kg + ' kg',
                                stock: item.stock,
                                date: new Date(),
                                jumlah: 0
                            }
                        })
                    );
                    dispatch(setLoading(false))
                }
            }
        }
        fetchData();
    }, []);

    const incrementKendaraan = (data, index) => {
        let temp = [...dataKendaraan]

        if (temp[index].jumlah <= 0) {
            temp[index].date = new Date()
        }

        if (data.id === temp[index].id) {
            temp[index].jumlah++
        }
        setDataKendaraan(temp)
    }

    const decrementKendaraan = (data, index) => {
        let temp = [...dataKendaraan]
        if (data.id === temp[index].id) {
            if (temp[index].jumlah > 0) {
                temp[index].jumlah--
            } else {
                temp[index].jumlah = 0
            }

            if (temp[index].jumlah >= 0) {
                temp[index].date = new Date()
            }
        }
        setDataKendaraan(temp)
    }

    const getKendaraan = () => {
        let temp = _.filter(dataKendaraan, (item) => { return item.jumlah > 0; });
        return temp
    }

    const check = () => {
        var totalKendaraan = dataKendaraan.reduce(function (prev, cur) {
            return prev + cur.jumlah;
        }, 0);
        return totalKendaraan
    }


    const showDatepicker = (item, index) => {
        setCurrentIndex(index)
        setCurrentItem(item)
        setDate(item.date)
        setMode('date');
        if (utils.isIOS()) {
            setShowModalDate(true)
        } else {
            setShow(true);
        }
    };

    const onChange = (next, event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');

        let temp = [...dataKendaraan]
        if (currentItem.id === temp[currentIndex].id) {
            temp[currentIndex].date = currentDate
        }

        setDataKendaraan(temp)
        setDate(currentDate);

        if (mode === "date") {
            if (next) {
                setMode('time');
                if (utils.isIOS()) {
                    setShowModalDate(true)
                } else {
                    setShow(true);
                }
            }
        } else if (mode === "time") {
            if (next) {
                setShow(false)
                setShowModalDate(false)
            }
        }
    };

    const beforeOnPress = () => {
        const kendaraan = getKendaraan();
        if (kendaraan[0].date > new Date()) {
            onPressRequest(PAGE.KENDARAAN, kendaraan)
        } else {
            Alert.alert(
                "Tanggal & Jam Pickup Tidak Valid",
                "Mohon isi tanggal dan jam pickup dengan benar"
            );
        }
    };

    const renderModalIOS = () => {
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback onPress={() => setShowModalDate(false)}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-end', margin: 0, }}
                isVisible={showModalDate}>
                <View style={[{ backgroundColor: Colors.white, borderTopLeftRadius: 10, borderTopRightRadius: 10 }]}>
                    <View style={{ alignItems: 'flex-end' }}>
                        <TouchableOpacity activeOpacity={1} onPress={() => { onChange(true, null, null); }} style={{ paddingVertical: 10, paddingHorizontal: 12, marginRight: 10 }}>
                            <Text style={[styles.btnDoneTxt]}>{"Done"}</Text>
                        </TouchableOpacity>
                    </View>
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={mode}
                        minimumDate={new Date()}
                        is24Hour={true}
                        display={'spinner'}
                        onChange={(event, selectedDate) => onChange(false, event, selectedDate)}
                    />
                </View>
            </Modal>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
                <FlatList
                    data={dataKendaraan}
                    renderItem={({ item, index }) => (
                        <View style={{ marginBottom: 9 }}>
                            <View style={[styles.flexHorizontal, { flex: 1, paddingLeft: 32, paddingRight: 18, marginBottom: 10 }]}>
                                <View style={{ borderWidth: 1, borderColor: "#B1C9F7", borderRadius: 5, padding: 10 }}>                                
                                    {item.id=='100' &&//Carry Box
                                        <IconKendaraanCarry />
                                    }
                                    {item.id=='103' &&//WingBox Rangger
                                        <IconKendaraanWingBox />
                                    }
                                    {item.id=='104' &&//Colt Diesel Double (CDD STANDAR BOX)
                                        <IconKendaraanCddStandartBox />
                                    }
                                    {item.id=='105' &&//Colt Diesel Engkel (CDE LONG BOX)
                                        <IconKendaraanCdeLongBox />
                                    }
                                    {item.id=='101' &&//Colt Diesel Double (CDE Box)
                                        <IconKendaraanColtDieselDouble />
                                    } 
                                    {item.id=='102' &&//Colt Diesel Double (CDD Long Box)
                                        <IconKendaraanCddLongBox />
                                    } 
                                    {item.id=='106' &&//Fuso
                                        <IconKendaraanFuso />
                                    } 
                                </View>
                                <View style={[{ width: '100%', flexShrink: 1, justifyContent: 'space-between', marginLeft: 15, marginTop: 5, marginBottom: 4 }]}>
                                    <View>
                                        <Text style={[styles.jenisKendaraanTxt, { marginBottom: 5 }]}>{item.jenis}</Text>
                                        <Text style={styles.kapasitasTxt}>{`Kapasitas Truck: ${item.kapasitas}`}</Text>
                                    </View>
                                    <View style={{ alignSelf: 'stretch', alignItems: 'flex-end' }}>
                                        <CardView cardElevation={3} cardMaxElevation={3} style={[styles.flexHorizontal, styles.menuInDec, { alignItems: 'center' }]}>
                                            <TouchableOpacity activeOpacity={1} onPress={() => decrementKendaraan(item, index)} 
                                                style={[{ marginTop: -12, marginBottom: -12, marginHorizontal: -10, padding: 12 }, 
                                                    item.jumlah == 0 ? { opacity: 0.5 } : { opacity: 1}]}
                                                disabled={ item.jumlah == 0}>
                                                <IconDecrement />
                                            </TouchableOpacity>
                                            <Text style={[styles.counterTxt, { marginHorizontal: 10 }]}>{item.jumlah}</Text>
                                            <TouchableOpacity activeOpacity={1} onPress={() => incrementKendaraan(item, index)} 
                                                style={[{ marginTop: -10, marginBottom: -10, marginHorizontal: -10, padding: 10 }, 
                                                    item.jumlah == item.stock ? { opacity: 0.5 } : { opacity: 1}]}
                                                disabled={ item.jumlah == item.stock }>
                                                <IconIncrement />
                                            </TouchableOpacity>
                                        </CardView>
                                    </View>
                                </View>
                            </View>
                            <TouchableOpacity activeOpacity={1} onPress={() => showDatepicker(item, index)} style={[styles.flexHorizontal, styles.optionContainer, { alignItems: 'center', justifyContent: 'space-between', marginBottom: 11, marginLeft: 23, marginRight: 18 }]}>
                                <Text style={styles.dateTimeTxt}>{item.jumlah > 0 ? utils.formatDateTime(item.date, "comma-minute") : "Tanggal & Jam Pickup"}</Text>
                                <IconCalendar />
                            </TouchableOpacity>
                            <View style={{ height: 2, width: '95%', backgroundColor: '#B1C9F7' }} />
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                    extraData={dataKendaraan}
                />
            </View>
            <View style={[{ alignContent: "flex-end" }]}>
                <View style={[styles.containerBtn]}>
                    <View style={{ paddingLeft: 20, marginBottom: 17 }}>
                        <Text style={[styles.textTotalKendaraan, {}]}>{`Total Kendaraan yang dipesan : `}<Text style={{ color: '#FFC233' }}>{`${check()} armada`}</Text></Text>
                    </View>
                    <Button disabled={check() <= 0} width={'100%'} onPress={() => beforeOnPress()} text={`Berikutnya`} />
                </View>
            </View>
            {renderModalIOS()}
            {
                show && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={date}
                        mode={mode}
                        is24Hour={true}
                        display={"default"}
                        onChange={(event, selectedDate) => onChange(true, event, selectedDate)}
                    />
                )
            }
        </View>
    )
}

KendaraanScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
};

export { KendaraanScreen };