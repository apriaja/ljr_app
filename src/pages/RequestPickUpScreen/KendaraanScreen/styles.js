// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    containerBtn: {
        paddingTop: 25,
        paddingBottom: 17,
        paddingHorizontal: 4,
        backgroundColor: 'rgba(230, 237, 250, 0.46)',
        borderTopLeftRadius: 31,
        borderTopRightRadius: 31,
    },
    textTotalKendaraan: {
        ...Fonts.soraSemiBold,
        fontSize: 12,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    jenisKendaraanTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    kapasitasTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 10,
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.57)',
    },
    menuInDec: {
        backgroundColor: Colors.white,
        borderRadius: 6,
        padding: 6,
    },
    counterTxt: {
        ...Fonts.notoSans,
        fontSize: 10,
        fontWeight: '600',
        letterSpacing: -0.333,
        color: Colors.black,
    },
    optionContainer: {
        paddingVertical: 13,
        paddingHorizontal: 9,
        backgroundColor: Colors.white,
        borderColor: 'rgba(0, 0, 0, 0.5)',
        borderWidth: 1,
        borderRadius: 10,
    },
    dateTimeTxt: {
        ...Fonts.soraSemiBold,
        fontSize: 12,
        letterSpacing: -0.333,
        color: Colors.black,
    },
});

// Make the styles available for ActivityScreens
export default styles;