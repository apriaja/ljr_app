import React, { Component } from 'react'
import { View, Text, BackHandler, TouchableWithoutFeedback } from 'react-native'

import api from '../../services/api';
import Config from 'react-native-config';

// Library
import { connect } from 'react-redux'
import Modal from 'react-native-modal';
import moment from 'moment';

// Component
import { Header, Tab, Button } from '../../components';

// Redux
import { setLoading } from '../../actions';

// Constans
import constants from '../../common/constants';
import utils from '../../common/utils';

// Page
import { MainScreen } from './MainScreen';
import { AlamatScreen } from './AlamatScreen';
import { InputInformationScreen } from './InputInformationScreen';
import { KendaraanScreen } from './KendaraanScreen';
import { InputDateScreen } from './InputDateScreen';
import { ListAlamatScreen } from './ListAlamatScreen';
import { AddAlamatScreen } from './AddAlamatScreen';
import { OrderPickUp } from './OrderPickUp';

import PAGE from './page';

// Style
import styles from './styles'
import { DetailScreen } from './DetailScreen';
import { Colors, Metrics } from '../../themes';

// Navigation
import navigationService from '../../navigation/navigationService';

// Assets
import IconChecklist from '../../assets/images/ico.ChecklistGreen.svg'

let TYPEALAMAT = {
    PU: "PU",
    DROP: 'DROP',
}

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: PAGE.MAIN,
            PAGINATION: [],
            modalSuccess: false,
            data: {},
            typeAlamat: "",
            title: PAGE.MAIN,
            dataAlamat: {
                PU: null,
                DROP: null,
            },
            dataPickUP: [],
            noPU: 'PUG0001',
            popUpError: {
                isShow: false,
                label: ''
            }
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
        this.clearPagination()
        this.pushPagination(PAGE.MAIN)
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    pushPagination = (data) => {
        let temp = this.state.PAGINATION
        if (temp[temp.length - 1] !== data) {
            temp.push(data)
        }
        let title = ""
        if (data == PAGE.LISTALAMAT || data == PAGE.ADDALAMAT) {
            if (this.state.typeAlamat == TYPEALAMAT.PU) {
                title = "Alamat Penjemputan"
            } else {
                title = "Alamat Pengiriman"
            }
        } else if (data == PAGE.INPUTINFORMASI || data == PAGE.KENDARAAN) {
            title = `${data} ke-${this.state.dataPickUP.length + 1}`
        } else {
            title = data
        }
        this.setState({ PAGINATION: temp, page: data, title: title })
    }

    popPagination = (count) => {
        let temp = this.state.PAGINATION

        if (count && temp.length > count) {
            temp = this.state.PAGINATION.slice(0, (temp.length - count))
        } else {
            if (temp.length > 0) {
                temp.pop()
            }
        }

        let title = ""
        if (temp[temp.length - 1] == PAGE.LISTALAMAT || temp[temp.length - 1] == PAGE.ADDALAMAT) {
            if (this.state.typeAlamat == TYPEALAMAT.PU) {
                title = "Alamat Penjemputan"
            } else {
                title = "Alamat Pengiriman"
            }
        } else if (temp[temp.length - 1] == PAGE.INPUTINFORMASI || temp[temp.length - 1] == PAGE.KENDARAAN) {
            title = `${temp[temp.length - 1]} ke-${this.state.dataPickUP.length + 1}`
        } else {
            title = temp[temp.length - 1]
        }

        this.setState({ PAGINATION: temp, page: temp[temp.length - 1], title: title })
    }

    clearPagination = () => {
        this.setState({ PAGINATION: [] })
    }

    clearData = () => {
        this.setState({
            dataAlamat: {
                PU: null,
                DROP: null,
            },
        })
    }

    onBack = () => {
        if (this.state.PAGINATION.length <= 1) {
            this.props.navigation.goBack();
            this.clearPagination();
        } else {
            this.popPagination()
        }
    }

    onSubmit = (from, data) => {
        let temp = { ...this.state.data };
        if (from === PAGE.MAIN) {
            temp.jenisPengirimian = data
            this.setState({ data: temp })
            this.clearData()
            this.pushPagination(PAGE.ORDERPICKUP)
        } else if (from === PAGE.ALAMAT) {
            temp.alamat = data
            this.setState({ data: temp })
            this.pushPagination(PAGE.INPUTINFORMASI)
        } else if (from === PAGE.INPUTINFORMASI) {
            temp.detailInformasi = data
            this.setState({ data: temp })
            this.pushPagination(PAGE.KENDARAAN)
        } else if (from === PAGE.KENDARAAN) {
            temp.dataKendaraan = data
            let final = this.state.dataPickUP
            final.push(temp)
            this.setState({ data: temp, dataPickUP: final })
            this.clearData()
            this.popPagination(3)
            // this.pushPagination(PAGE.PENJEMPUTAN)
        }
        // else if (from === PAGE.PENJEMPUTAN) {
        //     temp.tanggalJemput = data
        //     this.setState({ data: temp })
        //     this.pushPagination(PAGE.DETAIL)
        // } else if (from === PAGE.DETAIL) {
        //     this.setState({ modalSuccess: true })
        // }
    }

    showErrorPopUp = (isShow, label) => {
        this.setState({ showPopUpError: {
            isShow: isShow,
            label: label 
        }})
    }

    renderPage = (page) => {
        let itemToRender = null
        let styleTemp = {}
        switch (page) {
            case PAGE.MAIN:
                // styleTemp = { paddingHorizontal: 23, }
                itemToRender = this.renderMainPage()
                break;
            case PAGE.ALAMAT:
                itemToRender = this.renderInputAlamat()
                break;
            case PAGE.INPUTINFORMASI:
                itemToRender = this.renderInputInformasi()
                break;
            case PAGE.KENDARAAN:
                itemToRender = this.renderSelectKendaraan()
                break;
            case PAGE.PENJEMPUTAN:
                styleTemp = { paddingHorizontal: 23, }
                itemToRender = this.renderInputDate()
                break;
            case PAGE.DETAIL:
                // styleTemp = { paddingHorizontal: 23, }
                itemToRender = this.renderDetail()
                break;
            case PAGE.LISTALAMAT:
                itemToRender = this.renderListAlamat()
                break;
            case PAGE.ADDALAMAT:
                itemToRender = this.renderAddAlamat()
                break;
            case PAGE.ORDERPICKUP:
                itemToRender = this.renderOrderPU()
                break;
        }
        return (
            <View style={[{ flex: 1, marginTop: 11, }, styleTemp]}>
                {itemToRender}
            </View>
        )
    }

    renderMainPage = () => {
        return (
            <MainScreen
                onPressRequest={this.onSubmit}
                goToDetail={(data, noPU) => {
                    this.setState({ selectedDetail: data, noPU: noPU, fromPage: PAGE.MAIN }, () => this.pushPagination(PAGE.DETAIL))
                }}
            />
        )
    }

    renderInputAlamat = () => {
        return (
            <AlamatScreen
                data={this.state.data}
                dataAlamat={this.state.dataAlamat}
                onPressRequest={this.onSubmit}
                onPressPU={() => {
                    this.setState({ typeAlamat: TYPEALAMAT.PU }, () => {
                        this.pushPagination(PAGE.LISTALAMAT)
                    })
                }}
                onPressDROP={() => {
                    this.setState({ typeAlamat: TYPEALAMAT.DROP }, () => {
                        this.pushPagination(PAGE.LISTALAMAT)
                    })
                }}
            />
        )
    }

    renderInputInformasi = () => {
        return (
            <InputInformationScreen
                onPressRequest={this.onSubmit}
                // showErrorPopUp={this.showErrorPopUp}
            />
        )
    }

    renderSelectKendaraan = () => {
        return (
            <KendaraanScreen
                onPressRequest={this.onSubmit}
            />
        )
    }

    renderInputDate = () => {
        return (
            <InputDateScreen
                data={this.state.data}
                onPressRequest={this.onSubmit}
            />
        )
    }

    renderListAlamat = () => {
        return (
            <ListAlamatScreen
                onPressRequest={() => {
                    this.popPagination()
                }}
                gotoAddScreen={() => {
                    this.pushPagination(PAGE.ADDALAMAT)
                }}
                type={this.state.typeAlamat}
                onChooseAlamat={(data) => {
                    let temp = { ...this.state.dataAlamat }
                    if (data.typeAlamat == TYPEALAMAT.DROP) {
                        temp.DROP = data
                    } else if (data.typeAlamat == TYPEALAMAT.PU) {
                        temp.PU = data
                    }
                    this.setState({ dataAlamat: temp })
                    this.popPagination()
                }}
            />
        )
    }

    renderAddAlamat = () => {
        return (
            <AddAlamatScreen
                onPressSave={() => {
                    this.popPagination()
                }}
                type={this.state.typeAlamat}
            />
        )
    }

    renderDetail = () => {
        return (
            <DetailScreen
                data={this.state.selectedDetail}
                noPU={this.state.noPU}
                fromPage={this.state.fromPage}
            // onPressRequest={this.onSubmit}
            // onPressBatal={() => {
            //     this.props.navigation.goBack();
            //     this.clearPagination();
            // }}
            />
        )
    }

    renderOrderPU = () => {
        return (
            <OrderPickUp
                data={this.state.dataPickUP}
                noPU={this.state.noPU}
                addAddress={() => this.pushPagination(PAGE.ALAMAT)}
                onPressBack={() => this.popPagination()}
                onPressOrder={(data) => {
                    this.onSubmitOrderPickup(data)
                }}
                onPressDetail={(data) => {
                    this.setState({ selectedDetail: data, fromPage: PAGE.ORDERPICKUP }, () => this.pushPagination(PAGE.DETAIL))
                }}
            />
        )
    }

    getCustomerData = async () => {
        const id_profile = this.props.profile.id
        const response = await api.fetchAPI(`${Config.API_NGROK}/customer/${id_profile}`, null, 'GET')
        if (response) {
            return response.data.data;
        }
    }

    hitAPI = async (endpoint, body, http_method) => {
        const response = await api.fetchAPI(Config.API_NGROK + endpoint, body, http_method)
        if (response) {
            return response.data.data;
        }
    }

    onSubmitOrderPickup = async (data) => {
        this.props.setLoading(true)

        const id_profile = this.props.profile.id
        // this.getCustomerData()
        this.hitAPI(`/customer/${id_profile}`, null, 'GET').then(result => {
            const request = {
                jenis_service: 'CARGO',
                customer_id: result.id,
                customer_type: 'EXISTING',
                customer_business: result.jenis_customer,
                tanggal_order: utils.formatDateTime(new Date(), "timestamp"),
                unit_id: 1,                     // CONFIRM --> Customer apakah ada unit_id juga
                cabang_id: 1,                   // CONFIRM --> Customer apakah ada cabang_id juga
                created_by: result.name,
                order_by: 'CUST'                // CONFIRM --> Cara menentukan di cust atau bukan
            }

            const detail = [];
            data.forEach((item, index) => {
                const objDetail = {
                    index: index,
                    estimasi: item.detailInformasi.satuan.toLowerCase(),
                    custsentreceipt_id: item.alamat.PU.id,
                    lokasi_pickup: item.alamat.PU.nama,
                    tgl_pickup: utils.formatDateTime(item.dataKendaraan[0].date, "year-month-day-dash"),
                    jam_pickup: utils.formatDateTime(item.dataKendaraan[0].date, "time-colon"),
                    jenis_armada: item.dataKendaraan[0].id,
                    jml_armada: item.dataKendaraan[0].jumlah,
                    jenis_barang: item.detailInformasi.jenisBarang.id,
                    keterangan: item.detailInformasi.deskripsi,
                };
                
                if (item.detailInformasi.satuan.toLowerCase() === 'berat') {
                    objDetail.berat = item.detailInformasi.nilai;
                } else if (item.detailInformasi.satuan.toLowerCase() === 'volume') {
                    objDetail.volume = item.detailInformasi.nilai;
                } else if (item.detailInformasi.satuan.toLowerCase() === 'vallet') {
                    objDetail.vallet = item.detailInformasi.nilai;
                }
                
                detail.push(objDetail);
            })

            setTimeout(async() => {
                request.detail = detail;
                this.hitAPI('/orderPickup', request, 'POST').then(result2 => {
                    console.log('======onSubmitOrderPickup orderPickup', JSON.stringify(result2))
                    if (result2) {
                        navigationService.navigate("SuccessScreen", { from: constants.fromScreen.REQUESTPU, text: "Order Successful!" })
                        this.clearPagination();
                        
                        result2.order_d1.forEach((orderD1, index) => {
                            data[orderD1.index].detailInformasi.listFile.forEach(async dataFile => {
                                const file = new FormData();
                                file.append("no_order_pickup", result2.no_order_pickup);
                                file.append("id_pickup_d1", orderD1.id);
                                file.append("created_by", result.name);
                                file.append('file', dataFile.file, dataFile.file.name); 

                                const response = await api.fetchAPIUpload(Config.API_NGROK + '/orderPickupDoc', file)
                                if (response) {
                                    console.log("========onSubmitOrderPickup orderPickupDoc", JSON.stringify(response));
                                }
                            });
                        });
                    }
                    this.props.setLoading(false);
                }).catch(err => {
                    console.log(err);
                    this.props.setLoading(false);
                })
            }, 1500)
        })
    }

    onPressModalBtn = () => {
        this.setState({ modalSuccess: false })
    }

    renderModalSuccess = () => {
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-start' }}
                onModalHide={() => {
                    this.props.navigation.goBack();
                    this.clearPagination();
                    navigationService.navigate("SuccessScreen", { from: constants.fromScreen.REQUESTPU, text: "Order Successful!" })
                }}
                isVisible={this.state.modalSuccess}>
                <View style={[{ marginTop: Metrics.screenHeight / 4 }]}>
                    <View style={{ alignItems: 'center', marginHorizontal: 30, paddingTop: 7, paddingHorizontal: 30, paddingBottom: 18, borderRadius: 10, backgroundColor: Colors.white }}>
                        <IconChecklist />
                        <Text style={[styles.modalTitleTxt, { alignSelf: 'center', marginBottom: 9, marginTop: 4 }]}>{`Apakah orderan anda sudah sesuai?`}</Text>
                        <View style={[styles.flexHorizontal, { alignSelf: 'center', }]}>
                            <Button width={60} containerStyle={{ paddingVertical: 8 }} onPress={() => this.onPressModalBtn()} text="OK" />
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header horizontal={true} title={this.state.title} onBackPress={() => this.onBack()} />
                <View style={{ flex: 1 }}>
                    {this.renderPage(this.state.page)}
                </View>
                {this.renderModalSuccess()}
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        profile: state.persistReducer.profile
    }
}

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
