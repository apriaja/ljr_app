import PropTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView, Keyboard, TouchableWithoutFeedback } from 'react-native';
import Modal from 'react-native-modal';
import { Picker } from '@react-native-picker/picker';
import { useSelector, useDispatch } from 'react-redux';
import DocumentPicker from 'react-native-document-picker';

import { setLoading } from '../../../actions';
import api from '../../../services/api';
import Config from 'react-native-config';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';

// Themes and Styles
import styles from './styles';
import { Colors, Metrics } from '../../../themes';

// Component
import { Button, TextInputBase, RadioButtonLabel, RadioButtonInput, RadioButton, RadioForm } from '../../../components';

// Assets
import IconUpload from '../../../assets/images/ico.Upload2.svg';
import IconClose from '../../../assets/images/ico.Close.svg';

// PAGE
import PAGE from '../page';

import dummy from '../../../common/dummy';

var radio_props = [
    { label: 'Berat', value: "Berat" },
    { label: 'Kubik', value: "Volume" },
    { label: 'Palet', value: "Vallet" }
];

let valueLabel = {
    Berat: "Kg",
    Volume: "L",
    Vallet: 'Palet'
}

const InputInformationScreen = ({ data, onPressRequest }) => {
    const [value, setValue] = useState("Berat")
    const [jenisBarangItem, setJenisBarangItems] = useState([]);
    const formikRef = useRef();
    const pickerRef = useRef();

    const dispatch = useDispatch();

    useEffect(() => {
        async function fetchData() {
            if (jenisBarangItem.length == 0) {
                dispatch(setLoading(true))
                const response = await api.fetchAPI(`${Config.API_NGROK}/jenisBarang`, null, 'GET')
                if (response) {
                    setJenisBarangItems(response.data.data)
                    dispatch(setLoading(false))
                }
            }
        }
        fetchData();
    }, [])

    beforeOnPressRequest = (values) => {
        const jenisBarang = jenisBarangItem.filter(item => item.id === values.jenisBarang)[0];
        values.jenisBarang = jenisBarang;
        onPressRequest(PAGE.INPUTINFORMASI, values)
    }

    selectFile = async (formikProps) => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.allFiles], // types.images / plainText / audio / pdf
            });
            console.log('res : ' + JSON.stringify(res[0]));
            let temp = [];
            if (formikProps.values.listFile && formikProps.values.listFile.length > 0) {
                temp = [...formikProps.values.listFile];
                temp.splice(formikProps.values.listFile.length-1,1);
            }
            temp[temp.length] = { file: res[0] }
            formikProps.setFieldValue("listFile",temp);
        } catch (err) {
            // setListFile(null);
            if (DocumentPicker.isCancel(err)) {
                alert('Canceled');
            } else {
                alert('Unknown Error: ' + JSON.stringify(err));
                throw err;
            }
        }
    }
    removeFile = (formikProps, index) => {
        let temp = [...formikProps.values.listFile];
        temp.splice(index,1);
        formikProps.setFieldValue("listFile",temp);
    }
    addItem = (formikProps) => {
        let temp = [...formikProps.values.listFile];
        temp.push({ file: null});
        formikProps.setFieldValue("listFile",temp);
    }

    return (
        <Formik
            // ref={formikRef}
            innerRef={formikRef}
            initialValues={{
                jenisBarang: {},
                satuan: "berat",
                nilai: "",
                deskripsi: "",
                listFile: []
            }}
            validationSchema={validationSchema()}
            validateOnBlur={true}
            onSubmit={(values) => beforeOnPressRequest(values)}
        >
            {formikProps => (
                <View style={{ flex: 1 }}>
                    <View style={{ flex: 1, paddingHorizontal: 20, }}>
                        {/* JENIS BARANG */}
                        <View style={ styles.pickerContainer }>
                            <Picker
                                mode="dropdown"
                                ref={pickerRef}
                                selectedValue={formikProps.values.jenisBarang}
                                itemStyle={[{ color: 'blue', backgroundColor: 'white' }]}
                                dropdownIconColor={Colors.black}
                                onValueChange={(itemValue, itemIndex) => formikProps.setFieldValue("jenisBarang", itemValue)}
                            >
                                {
                                    jenisBarangItem.map((elm) => {
                                        return (
                                            <Picker.Item style={styles.pickerItemText} key={elm.id} label={elm.nama_barang} value={elm.id} />
                                        )
                                    })
                                }
                            </Picker>
                        </View>

                        {/* OPTION INFORMASI */}
                        <Text style={[styles.titleTxt, { marginLeft: 7, marginTop: 7 }]}>{"Informasi :"}</Text>
                        <View style={{ marginTop: 11, marginBottom: 14, alignItems: 'center' }}>
                            <RadioForm
                                formHorizontal={true}
                                animation={true}
                            >
                                {
                                    radio_props.map((obj, i) => (
                                        <RadioButton labelHorizontal={true} key={i} >
                                            <RadioButtonInput
                                                obj={obj}
                                                index={i}
                                                isSelected={value === obj.value}
                                                onPress={(data) => { setValue(data); formikProps.setFieldValue("satuan", data) }}
                                                borderWidth={2}
                                                buttonInnerColor={Colors.black}
                                                buttonOuterColor={Colors.black}
                                                buttonSize={8}
                                                buttonOuterSize={16}
                                                buttonStyle={{}}
                                                buttonWrapStyle={{ marginLeft: 16 }}
                                            />
                                            <RadioButtonLabel
                                                obj={obj}
                                                index={i}
                                                labelHorizontal={true}
                                                onPress={(data) => { setValue(data); formikProps.setFieldValue("satuan", data) }}
                                                labelStyle={styles.fontRB}
                                                labelWrapStyle={{}}
                                            />
                                        </RadioButton>
                                    ))
                                }
                            </RadioForm>
                        </View>

                        {/* INPUT */}
                        <View style={{ marginBottom: 7 }}>
                            <TextInputBase
                                width={"82%"}
                                styleText={styles.optionTxt}
                                styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)" }}
                                value={formikProps.values.nilai}
                                keyboardType={'number-pad'}
                                onChangeText={formikProps.handleChange("nilai")}
                                onBlur={formikProps.handleBlur("nilai")}
                                error={formikProps.touched.nilai ? formikProps.errors.nilai : null}
                            />
                            <View style={{ position: 'absolute', top: 0, right: 0 }}>
                                <View style={styles.optionContainer}>
                                    <Text style={[styles.optionTxt, { color: '#1F3E8F' }]}>{valueLabel[value]}</Text>
                                </View>
                            </View>
                        </View>

                        {/* DESC */}
                        <TextInputBase
                            styleText={styles.optionTxt}
                            styleContainer={{ borderColor: "rgba(0, 0, 0, 0.5)" }}
                            isMultiline={true}
                            value={formikProps.values.deskripsi}
                            placeholder="Deskripsi..."
                            keyboardType={'ascii-capable'}
                            onChangeText={formikProps.handleChange("deskripsi")}
                            onBlur={formikProps.handleBlur("deskripsi")}
                            error={formikProps.touched.deskripsi ? formikProps.errors.deskripsi : null}
                        />

                        {/* DOCUMENTS */}
                        <Text style={[styles.titleTxt, { marginLeft: 7, marginTop: 7 }]}>{"Dokumen :"}</Text>
                        {
                            formikProps.values.listFile.length === 0 && addItem(formikProps)
                        }
                        {
                            formikProps.values.listFile.length > 0 && formikProps.values.listFile.map((value, index) => {
                                if (value.file === null) {
                                    return (
                                        <TouchableOpacity
                                            // key={listFile.length}
                                            activeOpacity={0.5}
                                            style={[styles.marginHeight, styles.marginHeight, styles.pickerAOSContainer]}
                                            onPress={() => {selectFile(formikProps)}}
                                            >
                                            <IconUpload width={24} height={24} style={{alignSelf: 'flex-end',margin: 10}} />
                                        </TouchableOpacity>
                                    )
                                } else {
                                    return (
                                        <View style={[styles.flexHorizontal, styles.justifyAlignCenter, styles.documentList]}>
                                            <Text style={{ width: '90%'}}>{value.file.name}</Text>
                                            {/* <TouchableOpacity onPress={() => setShowWarning(false)}> */}
                                            <TouchableOpacity style={{ alignSelf: 'flex-end', flex: 1 }} onPress={() => removeFile(formikProps, index)}>
                                                <IconClose />
                                            </TouchableOpacity>
                                        </View>
                                    )
                                }
                            })
                        }
                        { 
                            formikProps.values.listFile.length > 0 && formikProps.values.listFile[formikProps.values.listFile.length-1].file !== null && (
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    style={[styles.marginHeight, {alignSelf: 'flex-end',padding: 10}]}
                                    onPress={() => {addItem(formikProps)}}
                                    >
                                        <Text style={[styles.marginHeight, {color: '#0066FF', textDecorationLine: 'underline'} ]}>+Tambah</Text>
                                </TouchableOpacity>
                            )
                        }
                    </View>
                    <View style={{ paddingLeft: 4, paddingRight: 4, paddingVertical: 17, backgroundColor: Colors.white }} >
                        <Button width={'100%'} onPress={formikProps.handleSubmit} text="Berikutnya" />
                    </View>
                </View>
            )}
        </Formik>
    )
}

const validationSchema = () => {
    return (
        yup.object().shape({
            jenisBarang: yup
                .string()
                .required("Silahkan masukkan jenis barang")
                .test('isPhone', 'Masukkan jenis barang dengan benar',
                    value => {
                        return value
                    }),
            satuan: yup
                .string()
                .required("Silahkan masukkan jumlah berat barang")
                .test('isPhone', 'Masukkan jumlah berat barang dengan benar',
                    value => {
                        return value
                    }),
            nilai: yup
                .string()
                .required("Silahkan masukkan jumlah nilai barang")
                .test('isPhone', 'Masukkan jumlah nilai barang dengan benar',
                    value => {
                        return value
                    }),
            deskripsi: yup
                .string()
                .required("Silahkan masukkan deskripsi barang")
                .test('isPhone', 'Masukkan deskripsi barang dengan benar',
                    value => {
                        return value
                    }),
        })
    )
}

InputInformationScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
};

export { InputInformationScreen };