// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 11,
        fontWeight: '600',
        lineHeight: 15,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    optionTxt: {
        ...Fonts.notoSans,
        fontSize: 10,
        fontWeight: '500',
        lineHeight: 14,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    optionContainer: {
        paddingVertical: 13,
        paddingHorizontal: 16,
        backgroundColor: Colors.white,
        borderWidth: 1,
        borderRadius: 10,
    }
});

// Make the styles available for ActivityScreens
export default styles;