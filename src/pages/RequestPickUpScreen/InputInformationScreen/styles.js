// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    titleTxt: {
        ...Fonts.soraBold,
        fontSize: 15,
        fontWeight: '900',
        lineHeight: 15,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    optionTxt: {
        ...Fonts.soraBold,
        fontSize: 12,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    optionContainer: {
        paddingVertical: 13,
        paddingHorizontal: 10,
        backgroundColor: "rgba(44, 64, 116, 0.29)",
        borderWidth: 1,
        borderColor: '#2C4074',
        borderRadius: 10,
        minWidth: 55,
        justifyContent: 'center',
        alignItems: 'center'
    },
    fontRB: {
        ...Fonts.soraSemiBold,
        fontSize: 12,
        color: Colors.black,
    },
    pickerContainer: {
        width: '100%',
        backgroundColor: Colors.white,
        borderWidth: 1,
        borderRadius: 10,
        borderColor: "rgba(0, 0, 0, 0.5)",
        padding: 5
    },
    pickerItemText: {
        ...Fonts.soraSemiBold,
        ...Fonts.tag,
        color: Colors.black,
        letterSpacing: -0.333,
        backgroundColor: 'white'
    },
    pickerAOSContainer: {
        maxWidth: '100%',//Metrics.screenWidth / 2,
        borderWidth: 1,
        borderColor: "rgba(0, 0, 0, 0.5)",
        borderRadius: 10,
        color: "rgba(0, 0, 0, 0.6)", 
        paddingVertical: 7, 
        paddingLeft: 0, 
        paddingRight: 2, 
        minWidth: '100%', 
        maxWidth: '100%', 
        alignSelf: 'center'
    },
    documentList: {
        paddingHorizontal: 12, 
        paddingVertical: 3, 
        borderColor: "rgba(0, 0, 0, 0.5)",
        borderRadius: 10,
        borderWidth: 1, 
        marginBottom: 3
    },
    textLink: {
        color: '#0066FF', 
        textDecorationLine: 'underline'
    }
});

// Make the styles available for ActivityScreens
export default styles;