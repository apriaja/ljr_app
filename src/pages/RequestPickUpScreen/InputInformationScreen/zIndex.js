import PropTypes from 'prop-types';
import React, { useState, useEffect, useRef } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView, Keyboard, TouchableWithoutFeedback } from 'react-native';
import Modal from 'react-native-modal';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';

// Themes and Styles
import styles from './styles';

// Component
import { Button, TextInputBase } from '../../../components';

// Assets
import IconArrowTop from '../../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../../assets/images/ico.ArrowBottom.svg';

// PAGE
import PAGE from '../page';
import { Colors, Metrics } from '../../../themes';
import dummy from '../../../common/dummy';

const InputInformationScreen = ({ data, onPressRequest }) => {
    const [dataOption, setDataOption] = useState({})
    const [showModalOption, setShowModalOption] = useState(false)
    const formikRef = useRef();

    useEffect(() => {

    }, []);

    const borderColor = (error) => {
        if (error) {
            return Colors.redSoft
        } else {
            return Colors.black
        }
    }

    const renderOption = (onPressItem) => {
        return (
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback onPress={() => setShowModalOption(false)}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                style={{ justifyContent: 'flex-end', margin: 0, }}
                isVisible={showModalOption}>
                <View style={[{ maxHeight: Metrics.screenHeight / 2, backgroundColor: Colors.white, paddingTop: 10, borderTopLeftRadius: 10, borderTopRightRadius: 10 }]}>
                    <FlatList
                        // style={{ flexGrow: 0 }}
                        data={dataOption.data}
                        renderItem={({ item, index }) => {
                            return (
                                <>
                                    <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item)} style={{ padding: 10 }}>
                                        <Text>{item.title}</Text>
                                    </TouchableOpacity>
                                </>
                            )
                        }}
                        ItemSeparatorComponent={() => (
                            <View style={{ width: "100%", height: 1, backgroundColor: Colors.gray }} />
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </Modal>
        )
    }

    const onPressModalOption = (item) => {
        setShowModalOption(false)
        if (dataOption.from == "berat") {
            formikRef.current.setFieldValue("berat", item.value)
        } else if (dataOption.from == "volume") {
            formikRef.current.setFieldValue("volume", item.value)
        } else if (dataOption.from == 'palet') {
            formikRef.current.setFieldValue("palet", item.value)
        }
    }

    return (
        <Formik
            // ref={formikRef}
            innerRef={formikRef}
            initialValues={{
                jenisBarang: "",
                berat: "",
                volume: "",
                palet: "",
                deskripsi: "",
            }}
            validationSchema={validationSchema}
            validateOnBlur={true}
            onSubmit={(values) => onPressRequest(PAGE.INPUTINFORMASI, values)}
        >
            {formikProps => (
                <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1 }}>
                    <Text style={[styles.titleTxt, { marginBottom: 4, marginLeft: 7 }]}>{"Jenis Barang"}</Text>
                    <TextInputBase
                        styleText={styles.optionTxt}
                        placeholder="Jenis barang.."
                        value={formikProps.values.jenisBarang}
                        keyboardType={'ascii-capable'}
                        onChangeText={formikProps.handleChange("jenisBarang")}
                        onBlur={formikProps.handleBlur("jenisBarang")}
                        error={formikProps.touched.jenisBarang ? formikProps.errors.jenisBarang : null}
                    />
                    <View style={{ padding: 2 }} />
                    <Text style={[styles.titleTxt, { marginBottom: 4, marginLeft: 7 }]}>{"Pilih berat"}</Text>
                    <TouchableOpacity activeOpacity={1} style={[styles.optionContainer, styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between', borderColor: formikProps.touched.berat ? borderColor(formikProps.errors.berat) : Colors.black }]}
                        onPress={() => {
                            setShowModalOption(true)
                            setDataOption({ data: dummy.jumlahBerat, from: "berat" })
                            formikProps.setFieldTouched("berat", true)
                        }}
                    >
                        <Text style={styles.optionTxt}>{formikProps.values.berat == "" ? "Pilih jumlah berat" : `${formikProps.values.berat} Kg`}</Text>
                        <IconArrowBottom />
                    </TouchableOpacity>
                    {
                        formikProps.touched.berat ? formikProps.errors.berat && <Text style={styles.errorFont}>{formikProps.errors.berat}</Text> : null
                    }
                    <View style={{ padding: 2 }} />
                    <Text style={[styles.titleTxt, { marginBottom: 4, marginLeft: 7 }]}>{"Pilih volume"}</Text>
                    <TouchableOpacity activeOpacity={1} style={[styles.optionContainer, styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between', borderColor: formikProps.touched.volume ? borderColor(formikProps.errors.volume) : Colors.black }]}
                        onPress={() => {
                            setShowModalOption(true)
                            setDataOption({ data: dummy.jumlahVolume, from: "volume" })
                            formikProps.setFieldTouched("volume", true)
                        }}
                    >
                        <Text style={styles.optionTxt}>{formikProps.values.volume == "" ? "Pilih jumlah volume" : `${formikProps.values.volume} Liter`}</Text>
                        <IconArrowBottom />
                    </TouchableOpacity>
                    {
                        formikProps.touched.volume ? formikProps.errors.volume && <Text style={styles.errorFont}>{formikProps.errors.volume}</Text> : null
                    }
                    <View style={{ padding: 2 }} />
                    <Text style={[styles.titleTxt, { marginBottom: 4, marginLeft: 7 }]}>{"Pilih palet"}</Text>
                    <TouchableOpacity activeOpacity={1} style={[styles.optionContainer, styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between', borderColor: formikProps.touched.palet ? borderColor(formikProps.errors.palet) : Colors.black }]}
                        onPress={() => {
                            setShowModalOption(true)
                            setDataOption({ data: dummy.jumlahPalet, from: "palet" })
                            formikProps.setFieldTouched("palet", true)
                        }}
                    >
                        <Text style={styles.optionTxt}>{formikProps.values.palet == "" ? "Pilih jumlah palet" : `${formikProps.values.palet} Palet`}</Text>
                        <IconArrowBottom />
                    </TouchableOpacity>
                    {
                        formikProps.touched.palet ? formikProps.errors.palet && <Text style={styles.errorFont}>{formikProps.errors.palet}</Text> : null
                    }
                    <View style={{ padding: 2 }} />
                    <Text style={[styles.titleTxt, { marginBottom: 4, marginLeft: 7 }]}>{"Deskripsi Barang"}</Text>
                    <TextInputBase
                        styleText={styles.optionTxt}
                        isMultiline={true}
                        value={formikProps.values.deskripsi}
                        placeholder="Deskripsi..."
                        keyboardType={'ascii-capable'}
                        onChangeText={formikProps.handleChange("deskripsi")}
                        onBlur={formikProps.handleBlur("deskripsi")}
                        error={formikProps.touched.deskripsi ? formikProps.errors.deskripsi : null}
                    />
                    {
                        showModalOption &&
                        <View>
                            {
                                renderOption(onPressModalOption)
                            }
                        </View>
                    }
                    <View style={{ padding: 6.5 }} />
                    <View style={[styles.flexHorizontal, { alignSelf: 'flex-end' }]}>
                        <Button onPress={formikProps.handleSubmit} text="Berikutnya" />
                    </View>
                </ScrollView>
            )}
        </Formik>
    )
}

const validationSchema = yup.object().shape({
    jenisBarang: yup
        .string()
        .required("Silahkan masukkan jenis barang")
        .test('isPhone', 'Masukkan jenis barang dengan benar',
            value => {
                return value
            }),
    berat: yup
        .string()
        .required("Silahkan masukkan jumlah berat barang")
        .test('isPhone', 'Masukkan jumlah berat barang dengan benar',
            value => {
                return value
            }),
    volume: yup
        .string()
        .required("Silahkan masukkan jumlah volume barang")
        .test('isPhone', 'Masukkan jumlah volume barang dengan benar',
            value => {
                return value
            }),
    palet: yup
        .string()
        .required("Silahkan masukkan jumlah palet barang")
        .test('isPhone', 'Masukkan jumlah palet barang dengan benar',
            value => {
                return value
            }),
    deskripsi: yup
        .string()
        .required("Silahkan masukkan deskripsi barang")
        .test('isPhone', 'Masukkan deskripsi barang dengan benar',
            value => {
                return value
            }),
})

InputInformationScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
};

export { InputInformationScreen };