import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';
import CardView from 'react-native-cardview';
import _ from "lodash";

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconChecklist from '../../../assets/images/ico.checkList.svg';

import dummy from '../../../common/dummy';
import { Colors } from '../../../themes';

import constants from '../../../common/constants';

// Page
import PAGE from '../../AddressScreen/page';

import { AddressListScreen } from '../../AddressScreen/AddressList';
import { AddAddressScreen } from '../../AddressScreen/AddAddress';


const AddAlamatScreen = ({ data, onPressRequest, type }) => {
    return (
        <View style={{ flex: 1 }}>
            <AddAddressScreen
                onPressSave={(values) => {
                    onPressRequest(values)
                }}
                fromPage={constants.fromScreen.REQUESTPU}
                typeAlamat={type}
            />
        </View>
    )
}

AddAlamatScreen.propTypes = {
    data: PropTypes.array,
    type: PropTypes.string,
    onPressRequest: PropTypes.func,
};

export { AddAlamatScreen };