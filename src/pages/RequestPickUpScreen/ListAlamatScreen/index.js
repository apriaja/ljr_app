import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, Alert } from 'react-native';
import CardView from 'react-native-cardview';
import _ from "lodash";
import { useSelector, useDispatch } from 'react-redux';

import { setLoading } from '../../../actions';

import api from '../../../services/api';
import Config from 'react-native-config';

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconChecklist from '../../../assets/images/ico.checkList.svg';

import dummy from '../../../common/dummy';
import { Colors } from '../../../themes';

import constants from '../../../common/constants';

// Page
import PAGE from '../../AddressScreen/page';

import { AddressListScreen } from '../../AddressScreen/AddressList';
import { AddAddressScreen } from '../../AddressScreen/AddAddress';


const ListAlamatScreen = ({ data, onPressRequest, gotoAddScreen, type, onChooseAlamat }) => {

    const { profile } = useSelector((state) => state.persistReducer)
    const [listAlamat, setListAlamat] = useState([]);

    const dispatch = useDispatch();

    useEffect(() => {
        async function fetchData() {
            if (listAlamat.length == 0) {
                dispatch(setLoading(true))
                const id_profile = profile.id
                const response = await api.fetchAPI(`${Config.API_NGROK}/custSendReceipt/showCustomer/${id_profile}`, null, 'GET')
                // const response = await api.fetchAPI(`${Config.API_NGROK}/custSendReceipt/showCustomer/7100000`, null, 'GET')
                
                if (response) {
                    dispatch(setLoading(false))
                    if (response.data.data) {
                        setListAlamat(response.data.data
                            .filter(item => item.jenis_alamat && item.jenis_alamat.toUpperCase() === 'SEND')
                            .map((item) => {
                                const phonePIC = item.phone_pic ? item.phone_pic : '-';
                                const namaPIC = item.nama_pic ? item.nama_pic : '-';
                                const contact = phonePIC === '-' && namaPIC === '-' ? '-' : phonePIC + ' (' + namaPIC + ')';
                                return {
                                    id: item.id,
                                    nama: item.nama_send_receipt,
                                    alamat: item.alamat,
                                    contact: contact
                                }
                            })
                        )
                    } else {
                        Alert.alert(
                            "Mohon maaf ada kesalahan", 
                            "Silahkan direload atau dicoba beberapa saat lagi",
                            [
                                { text: "Reload", onPress: () => fetchData() },
                                { text: "Close"}
                            ]); 
                        return false;
                    }
                }
            }
        }
        fetchData();
    },[]);

    return (
        <View style={{ flex: 1 }}>
            <AddressListScreen
                data={listAlamat}
                onPressAdd={() => {
                    gotoAddScreen()
                }}
                onPressNext={(data) => {
                    onPressRequest()
                    console.log("awdjnawd")
                }}
                fromPage={constants.fromScreen.REQUESTPU}
                typeAlamat={type}
                onSelectAlamat={(data) => {
                    onChooseAlamat(data)
                }}
            />
        </View>
    )
}

ListAlamatScreen.propTypes = {
    data: PropTypes.array,
    type: PropTypes.string,
    gotoAddScreen: PropTypes.func,
    onPressRequest: PropTypes.func,
    onChooseAlamat: PropTypes.func,
};

export { ListAlamatScreen };