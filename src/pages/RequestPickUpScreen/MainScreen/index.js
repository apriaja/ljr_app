import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, Alert } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import { setLoading } from '../../../actions';

import api from '../../../services/api';
import Config from 'react-native-config';

// Themes and Styles
import styles from './styles';

// Component
import { Button, Tab } from '../../../components';

// Assets
import IconArrowTop from '../../../assets/images/ico.ArrowTop.svg';

// Page
import PAGE from '../page';

import { HistoryReqSTTB } from './HistoryScreen';
import dummy from '../../../common/dummy';
import { RequestPUScreen } from './RequestPUScreen';

let TAB = [
    { id: 0, title: 'Request' },
    { id: 1, title: 'Daftar Penjemputan' },
]

const MainScreen = ({ data, onPressRequest, goToDetail }) => {
    const [page, setPage] = useState(0);
    const [historyData, setHistoryData] = useState([]);
    const [refreshTab, setRefreshTab] = useState(false);

    const { profile } = useSelector((state) => state.persistReducer)
    const dispatch = useDispatch();

    useEffect(() => {

    }, []);

    async function fetchDataOrderPickup() {
        if (historyData.length == 0) {
            dispatch(setLoading(true))
            const id_profile = profile.id
            const response = await api.fetchAPI(`${Config.API_NGROK}/orderPickup/getOrderPickupListByCustomerId/${id_profile}`, null, 'GET')
            // const response = await api.fetchAPI(`${Config.API_NGROK}/orderPickup/getOrderPickupListByCustomerId/7100000`, null, 'GET')
            if (response) {
                dispatch(setLoading(false))
                if (response.data.data) {
                    console.log(JSON.stringify(response.data.data))
                    setHistoryData(response.data.data
                        .map((item) => {
                            return {
                                noPu: item.no_order_pickup,
                                status_task: item.nama_status.replace(/ /g,"_"),
                                jenis_service: item.jenis_service,
                                data: item.data ? item.data : null
                            }
                        })
                    );
                    return true;
                } else {
                    Alert.alert(
                        "Mohon maaf ada kesalahan", 
                        "Silahkan direload atau dicoba beberapa saat lagi",
                        [
                            { text: "Reload", onPress: () => fetchDataOrderPickup() },
                            { text: "Close"}
                        ]); 
                    return false;
                }
            }
        } else {
            return true;
        }
    }

    const onSelectedTab = (data) => {
        const setActiveData = {
            '0': () => { setPage(0) },
            '1': () => { setPage(1) },
        };
        
        if (data.id === 1) {
            fetchDataOrderPickup().then(res => {
                if (res) {
                    setActiveData[data.id]();
                } else {
                    setActiveData[0]();
                    setRefreshTab(true);
                    setTimeout(() => {
                        setRefreshTab(false);
                    }, 100)
                }
            });
        } else {
            setActiveData[data.id]();
        }
    }

    const renderPage = (page) => {
        let itemToRender = null
        const setActiveData = {
            '0': () => { itemToRender = renderFirstPage() },
            '1': () => { itemToRender = renderSecondPage() },
        };
        setActiveData[page]();

        return (
            <View style={{ flex: 1, marginTop: 11, }}>
                {itemToRender}
            </View>
        )
    }

    const renderFirstPage = () => {
        return (
            <View style={{ flex: 1, paddingHorizontal: 23, }}>
                <RequestPUScreen
                    onSendPress={onPressRequest}
                />
            </View>
        )
    }

    const renderSecondPage = () => {
        return (
            <HistoryReqSTTB
                data={historyData}
                gotoDetail={(data) => goToDetail(data)}
            />
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={{ paddingHorizontal: 23, }}>
                { !refreshTab && ( <Tab data={TAB} onSelectTab={onSelectedTab} indexToShow={page} /> )}
            </View>
            {renderPage(page)}
        </View>
    )
}

MainScreen.propTypes = {
    data: PropTypes.array,
    onPressRequest: PropTypes.func,
    goToDetail: PropTypes.func,
};

export { MainScreen };