// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../../themes';
import Utils, { isTableted } from '../../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    filterTxt: {
        ...Fonts.notoSansBold,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    statusContainer: {
        paddingVertical: 3,
        paddingHorizontal: 23,
        backgroundColor: Colors.white,
        borderRadius: 7,
    },
    statusTxt: {
        ...Fonts.poppinsMedium,
        ...Fonts.subTag,
        textTransform: 'uppercase',
        letterSpacing: 0.05,
        color: "#0066FF",
    },
    noPuTxt: {
        ...Fonts.soraRegular,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    titleTxt: {
        ...Fonts.soraRegular,
        fontSize: 11,
        lineHeight: 14,
        letterSpacing: -0.333,
        color: "rgba(0, 0, 0, 0.4)",
    },
    valueTxt: {
        ...Fonts.soraRegular,
        fontSize: 13,
        lineHeight: 16,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    separator: {
        width: '100%',
        height: 3,
        backgroundColor: "#DADADA",
    },
    detailTxt: {
        ...Fonts.robotoRegular,
        fontSize: 11,
        color: Colors.white,
    },
});

// Make the styles available for ActivityScreens
export default styles;