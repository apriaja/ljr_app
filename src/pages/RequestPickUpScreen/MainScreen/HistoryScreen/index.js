import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, Alert } from 'react-native';
import CardView from 'react-native-cardview';
import { useSelector, useDispatch } from 'react-redux';

import { setLoading } from '../../../../actions';

import api from '../../../../services/api';
import Config from 'react-native-config';
import utils from '../../../../common/utils';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../../../themes';

// Components
import { Button } from '../../../../components';

// Assets
import IconArrowTop from '../../../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../../../assets/images/ico.ArrowBottom.svg';
import IconFilter from '../../../../assets/images/ico.filter.svg';
import dummy from '../../../../common/dummy';

let filterItem = [
    {
        title: "Semua data",
        value: "all"
    },
    {
        title: "Daftar penjemputan yang belum selesai",
        value: "Sedang Diproses"
    },
    {
        title: "Daftar penjemputan yang gagal",
        value: "Gagal"
    },
    {
        title: "Daftar penjemputan yang telah selesai",
        value: "Selesai"
    },
]

const HistoryReqSTTB = ({ data, gotoDetail }) => {
    const [listItem, setListItem] = useState([]);
    const [showFilter, setShowFilter] = useState(false);
    const dispatch = useDispatch();

    useEffect(() => {
        setShowFilter(false)
        setListItem(data.map(elm => { 
            return { 
                ...elm,
                collaps: true 
            }; 
        }));
    }, []);

    const onPressItem = (item, index) => {
        let temp = [...listItem]
        temp[index].collaps = !listItem[index].collaps
        setListItem(temp)
    }

    const onPressFilter = (item) => {
        setShowFilter(false)
        if (item.value == "all") {
            setListItem(data)
        } else {
            let result = [];
            data.map(elm => {
                elm.status.toLowerCase().includes(item.value.toLowerCase()) && result.push(elm)
            })
            setListItem(result)
        }
    }

    const onPressDetail = async (item, noPu, jenisService) => {
        // get data detailinformasi & kendaraan
        dispatch(setLoading(true))
        getDetail(item.id).then(response => {
            if (response) {
                const isVolume = response.volume !== null;
                const isBerat = response.berat !== null;
                const isPallet = response.pallet !== null;

                item.noPU = response.no_order_pickup;
                item.jenisPengirimian = {
                    title: jenisService.toUpperCase() === "CARGO" ? 'Cargo Service' : 'Instant Service'
                }
                item.detailInformasi = {
                    jenisBarang: response.jenis_barang.nama_barang,
                    satuan: isVolume ? 'volume' : isBerat ? 'berat' : 'vallet',
                    nilai: isVolume ? response.volume : isBerat ? response.berat : response.pallet,
                    deskripsi: response.keterangan
                }
                item.dataKendaraan = [{
                    // status: response.task_no.status_task,
                    jenis: response.tipe_armada.nama_tipe,
                    // kapasitas: response.tipe_armada.tonase_kg,
                    // jumlah: response.jml_armada,
                    date: response.tgl_pickup + " " + response.jam_pickup,
                    // driver: response.task_no.nama_driver,
                    // krani: response.kranis
                }]
                item.task = [{
                    nama_armada: response.task_no.armada.nama_armada,
                    no_polisi: response.task_no.armada.no_polisi,
                    nama_driver: response.task_no.nama_driver,
                    nama_krani: response.kranis,
                    keterangan: response.task_no.keterangan,
                    status: response.task_no.status.nama_status.replace(/ /g,"_")
                }]
                
                gotoDetail({ ...item, noPu })
            }
            dispatch(setLoading(false))
        })
    }

    const getDetail = async (id) => {
        const response = await api.fetchAPI(`${Config.API_NGROK}/orderPickupD/getDetail/${id}`, null, 'GET')
        if (response) {
            if (response.data.data) {
                return response.data.data;
            } else {
                Alert.alert(
                    "Mohon maaf ada kesalahan", 
                    "Silahkan direload atau dicoba beberapa saat lagi",
                    [
                        { text: "Reload", onPress: () => getDetail(id) },
                        { text: "Close"}
                    ]); 
                return false;
            }
        }
    }

    const renderItem = (data, noPu, jenisService) => {
        return (
            <FlatList
                showsVerticalScrollIndicator={false}
                data={data}
                renderItem={({ item, index }) => (
                    <View key={index} style={{ paddingTop: 4, paddingBottom: 18, paddingLeft: 10, paddingRight: 5 }}>
                        <View style={[styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between', marginBottom: 4 }]}>
                            <Text style={[styles.titleTxt]}>{`Alamat ${index + 1}`}</Text>
                            <Text style={[styles.titleTxt]}>{`Tgl Pickup : ${utils.formatDateTime(item.alamat.PU.tgl_pickup, "comma")}`}</Text>
                        </View>
                        <View style={[styles.flexHorizontal, { justifyContent: 'space-between' }]}>
                            <View>
                                <Text style={[styles.valueTxt, { marginBottom: 5 }]}>{item.alamat.PU.nama}</Text>
                                <Text style={[styles.titleTxt, { marginBottom: 6 }]}>{"Jenis Penjemputan"}</Text>
                                <Text style={[styles.valueTxt, { marginBottom: 6 }]}>{jenisService.toUpperCase() === "CARGO" ? 'Cargo Service' : 'Instant Service'}</Text>
                                {
                                    item.status && (<Text style={[styles.titleTxt, { marginBottom: 8 }]}>{"Status"}</Text>)
                                }
                            </View>
                            <View style={{ marginTop: 12 }}>
                                <Button width={80} textStyle={styles.detailTxt} containerStyle={{ borderRadius: 8.25, paddingVertical: 9, backgroundColor: "rgba(0, 102, 255, 1)" }} onPress={() => onPressDetail(item, noPu, jenisService)} text="Detail" />
                            </View>
                        </View>
                        { item.status && (
                            <CardView cardElevation={3} cardMaxElevation={3} style={[styles.statusContainer, { alignSelf: 'baseline' }]}>
                                <Text style={[styles.statusTxt, { color: Colors.StatusTxtColor[item.status.replace(/ /g,"_")] }]}>{item.status.replace(/_/g," ")}</Text>
                            </CardView>
                        )}
                    </View>
                )}
                keyExtractor={(item, index) => index.toString()}
                ItemSeparatorComponent={() => (
                    <View style={styles.separator} />
                )} />
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={[styles.flexHorizontal, { alignSelf: 'flex-end', paddingVertical: 6, marginBottom: 5, marginTop: -9, marginRight: 27 }]}>
                <Text style={[styles.filterTxt, { marginRight: 4 }]}>{"Filter daftar"}</Text>
                <IconFilter />
            </TouchableOpacity>
            <FlatList
                contentContainerStyle={{ paddingBottom: 20 }}
                showsVerticalScrollIndicator={false}
                data={listItem}
                renderItem={({ item, index }) => (
                    <View key={index} style={{ marginHorizontal: 12, overflow: 'hidden', borderWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", borderRadius: 10 }}>
                        <View style={[styles.flexHorizontal, { backgroundColor: "#C4E0E3", justifyContent: 'space-between', paddingTop: 11, paddingBottom: 14, paddingLeft: 11, paddingRight: 8 }]}>
                            <Text style={styles.noPuTxt}>{item.noPu}</Text>
                            <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                <CardView cardElevation={3} cardMaxElevation={3} style={[styles.statusContainer]}>
                                    <Text style={[styles.statusTxt, { color: Colors.StatusTxtColor[item.status_task] }]}>{ item.status_task.replace(/_/g," ") }</Text>
                                </CardView>
                                <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item, index)} style={[styles.justifyAlignCenter, { paddingVertical: 11, paddingHorizontal: 11, marginBottom: -5, marginTop: -5, marginRight: -11 }]}>
                                    {
                                        item.collaps ? <IconArrowBottom /> : <IconArrowTop />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View>
                            {
                                !item.collaps && renderItem(item.data, item.noPu, item.jenis_service)
                            }
                        </View>
                    </View>
                )}
                keyExtractor={(item, index) => index.toString()}
                extraData={listItem}
                ItemSeparatorComponent={() => (
                    <View style={{ paddingBottom: 6 }} />
                )}
            />
            {
                showFilter &&
                <View style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} />
                    <View style={{ backgroundColor: Colors.white, marginTop: 18, paddingHorizontal: 10, paddingHorizontal: 5, alignSelf: 'flex-end', borderRadius: 5, borderWidth: 1, borderColor: "rgba(165, 164, 164, 1)" }}>
                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={filterItem}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity activeOpacity={1} onPress={() => onPressFilter(item)} style={{ paddingVertical: 5 }}>
                                    <Text style={styles.filterTxt}>{item.title}</Text>
                                </TouchableOpacity>
                            )}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            }
        </View>
    )
}

HistoryReqSTTB.propTypes = {
    data: PropTypes.array,
    gotoDetail: PropTypes.func,
};

export { HistoryReqSTTB };