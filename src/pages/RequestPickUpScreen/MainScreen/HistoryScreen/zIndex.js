import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconArrowTop from '../../../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../../../assets/images/ico.ArrowBottom.svg';
import IconFilter from '../../../../assets/images/ico.filter.svg';
import IconProfile from '../../../../assets/images/ico.Profile.svg';
import { Colors } from '../../../../themes';

let StatusTxtColor = {
    "Selesai": '#38DE34',
    "Sedang Diproses": '#DE9034',
    "Gagal": '#DE3434'
}

let filterItem = [
    {
        title: "Semua data",
        value: "all"
    },
    {
        title: "Daftar penjemputan yang belum selesai",
        value: "Sedang Diproses"
    },
    {
        title: "Daftar penjemputan yang gagal",
        value: "Gagal"
    },
    {
        title: "Daftar penjemputan yang telah selesai",
        value: "Selesai"
    },
]

const HistoryReqSTTB = ({ data }) => {
    const [listItem, setListItem] = useState([]);
    const [showFilter, setShowFilter] = useState(false);

    useEffect(() => {
        setShowFilter(false)
        let temp = data.map(elm => ({ ...elm, collaps: true }))
        setListItem(temp)
    }, []);

    const onPressItem = (item, index) => {
        let temp = [...listItem]
        temp[index].collaps = !listItem[index].collaps
        setListItem(temp)
    }

    const onPressFilter = (item) => {
        setShowFilter(false)
        if (item.value == "all") {
            setListItem(data)
        } else {
            let result = [];
            data.map(elm => {
                elm.status.toLowerCase().includes(item.value.toLowerCase()) && result.push(elm)
            })
            setListItem(result)
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={[styles.flexHorizontal, { alignSelf: 'flex-end', paddingVertical: 6, marginBottom: 5, marginTop: -9 }]}>
                <Text style={[styles.filterTxt, { marginRight: 4 }]}>{"Filter daftar"}</Text>
                <IconFilter />
            </TouchableOpacity>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={listItem}
                renderItem={({ item, index }) => renderItem(item, index, onPressItem)}
                keyExtractor={(item, index) => index.toString()}
                extraData={listItem}
            />
            {
                showFilter &&
                <View style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} />
                    <View style={{ backgroundColor: Colors.white, marginTop: 18, paddingHorizontal: 10, paddingHorizontal: 5, alignSelf: 'flex-end', borderRadius: 5, borderWidth: 1, borderColor: "rgba(165, 164, 164, 1)" }}>
                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={filterItem}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity activeOpacity={1} onPress={() => onPressFilter(item)} style={{ paddingVertical: 5 }}>
                                    <Text style={styles.filterTxt}>{item.title}</Text>
                                </TouchableOpacity>
                            )}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            }
        </View>
    )
}

const renderItem = (item, index, onPressItem) => {
    return (
        <View style={{ backgroundColor: "rgba(196, 224, 227, 0.52)", borderRadius: 10, marginBottom: 10 }}>
            <View style={{ paddingHorizontal: 16, paddingTop: 9, paddingBottom: 8 }}>
                <View style={[styles.flexHorizontal, { justifyContent: 'space-between', alignItems: 'center' }]}>
                    <Text style={styles.titleTxt}>{"Nomor pickup:"}</Text>
                    <View style={styles.flexHorizontal}>
                        <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item, index)} style={[styles.justifyAlignCenter, { paddingHorizontal: 10, marginBottom: -10, marginTop: -5, marginRight: -10 }]}>
                            {
                                item.collaps ? <IconArrowBottom /> : <IconArrowTop />
                            }
                        </TouchableOpacity>
                    </View>
                </View>
                <Text style={[styles.nomorPUTxt, { marginTop: 3, marginBottom: 9 }]}>{item.no}</Text>
                <FlatList
                    style={{ flexGrow: 0 }}
                    data={item.alamat ? item.alamat : []}
                    renderItem={({ item, index }) => (
                        <View style={{ marginBottom: 3 }}>
                            <View style={[styles.flexHorizontal, { justifyContent: 'space-between', alignItems: 'center' }]}>
                                <Text style={styles.titleTxt}>{item.nama}</Text>
                                <Text style={[styles.titleTxt, { color: StatusTxtColor[item.statusID] }]}>{item.statusDesc}</Text>
                            </View>
                            <Text style={[styles.alamatTxt, { marginTop: 4, marginBottom: 3 }]}>{item.alamat}</Text>
                            <Text style={styles.nomorPUTxt}>{"CP: "} <Text style={styles.alamatTxt}>{item.contact}</Text></Text>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
            <View style={styles.separator} />
            <View style={[styles.flexHorizontal, { paddingHorizontal: 16, paddingTop: 5, paddingBottom: 10 }]}>
                <Text style={[styles.titleTxt, { marginRight: 9 }]}>{"Tanggal pickup"}</Text>
                <Text style={styles.descTxt}>{item.tglPickUp}</Text>
            </View>
            {
                !item.collaps &&
                <View>
                    <View style={styles.separator} />
                    <View style={{ paddingHorizontal: 16, paddingTop: 3, paddingBottom: 7 }}>
                        <Text style={[styles.titleTxt, { marginBottom: 4 }]}>{"Mobil pesanan anda"}</Text>
                        <FlatList
                            scrollEnabled={false}
                            style={{ flexGrow: 0 }}
                            data={item.mobilPesanan ? item.mobilPesanan : []}
                            renderItem={({ item, index }) => (
                                <View style={[styles.flexHorizontal, { alignItems: 'center', justifyContent: 'space-between' }]}>
                                    <Text style={styles.descTxt}>{item.jenis}</Text>
                                </View>
                            )}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                    <View style={styles.separator} />
                    <View style={{ paddingHorizontal: 16, paddingTop: 2, paddingBottom: 7 }}>
                        <Text style={[styles.titleTxt, { marginBottom: 4 }]}>{"Driver yang ditugaskan"}</Text>
                        <IconProfile />
                    </View>
                </View>
            }
        </View >
    )
}

HistoryReqSTTB.propTypes = {
    data: PropTypes.array,
};

export { HistoryReqSTTB };