import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';
import Modal from 'react-native-modal';

// Component
import { Button, TextInputBase } from '../../../../components';

// Themes and Styles
import styles from './styles';
import { Colors, Metrics } from '../../../../themes';

import constants from '../../../../common/constants';

// Assets
import IconRegular from '../../../../assets/images/ico.PURegular.svg';
import IconDirect from '../../../../assets/images/ico.PUDirect.svg';

// Page
import PAGE from '../../page';
import dummy from '../../../../common/dummy';

const RequestPUScreen = ({ onSendPress }) => (
    <View style={[styles.container]}>
        <FlatList
            data={dummy.jenisPickup}
            renderItem={({ item, index }) => (
                <TouchableOpacity activeOpacity={1} onPress={() => onSendPress(PAGE.MAIN, item)} style={[styles.flexHorizontal, styles.containerItem]}>
                    {
                        item.title === "Instant Service" ? <IconDirect /> : <IconRegular />
                    }
                    <View style={{ flex: 1, marginLeft: 8 }}>
                        <Text style={styles.titleTxt}>{item.title}</Text>
                        <Text style={styles.descTxt}>{item.desc}</Text>
                    </View>
                </TouchableOpacity>
            )}
            keyExtractor={(item, index) => index.toString()}
        />
    </View>
)

RequestPUScreen.propTypes = {
    onSendPress: PropTypes.func,
};

export { RequestPUScreen };