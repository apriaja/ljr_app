// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../../themes';
import Utils, { isTableted } from '../../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    container: {
        flex: 1
    },
    containerItem: {
        flex: 1,
        alignItems: 'center',
        padding: 11,
        marginBottom: 7,
        backgroundColor: 'rgba(196, 224, 227, 0.52)',
        borderRadius: 10,
    },
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 15,
        lineHeight: 20,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    descTxt: {
        ...Fonts.notoSans,
        fontSize: 11,
        lineHeight: 15,
        letterSpacing: -0.333,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;