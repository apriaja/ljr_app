import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import { Text, View, ActivityIndicator, TouchableWithoutFeedback } from 'react-native';

// Library
import { Formik } from 'formik';
import * as yup from 'yup';
import Modal from 'react-native-modal';

// Component
import { Button, TextInputBase } from '../../../components';

// Themes and Styles
import styles from './styles';
import { Colors, Metrics } from '../../../themes';

import constants from '../../../common/constants';

// Assets

const RequestScreen = ({ onSendPress }) => {
    const [sttb, setSttb] = useState({});
    const [modal, setModal] = useState(false);

    const onPressSubmit = (values) => {
        setModal(true)
        setSttb(values)
    }

    const OnPressOK = () => {
        onSendPress(sttb)
        setModal(false)
    }

    return (
        <View>
            <Formik
                initialValues={{ jumlah: "" }}
                validationSchema={validationSchema}
                validateOnBlur={true}
                onSubmit={(values) => onPressSubmit(values)}
            >
                {formikProps => (
                    <View style={[styles.container]}>
                        <View style={[styles.flexHorizontal, { marginBottom: 15, justifyContent: 'space-between' }]}>
                            <Text style={styles.titleTxt}>{"No-order STTB"}</Text>
                            <Text style={styles.titleTxt}>{"Tanggal Order"}</Text>
                        </View>
                        <TextInputBase
                            styleContainer={{ borderColor: 0 }}
                            placeholder="Masukkan jumlah buku STTB"
                            value={formikProps.values.jumlah}
                            keyboardType={'number-pad'}
                            onChangeText={formikProps.handleChange("jumlah")}
                            onBlur={formikProps.handleBlur("jumlah")}
                            error={formikProps.touched.jumlah ? formikProps.errors.jumlah : null}
                        />
                        <Text style={[styles.tncTxt, { marginTop: 6, marginHorizontal: 7 }]}>{"*Request STTB dalam jumlah buku  ( 1 buku : 100 lembar)"}</Text>
                        <View style={{ padding: 8 }} />
                        <View style={[styles.flexHorizontal, { alignSelf: 'flex-end', }]}>
                            <Button onPress={formikProps.handleSubmit} text="Order" />
                        </View>
                    </View>
                )}
            </Formik>
            <Modal
                customBackdrop={(
                    <TouchableWithoutFeedback onPress={() => setModal(false)}>
                        <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,0.4)' }} />
                    </TouchableWithoutFeedback>
                )}
                animationIn='slideInUp'
                animationOut='slideOutDown'
                onBackButtonPress={() => setModal(false)}
                onBackdropPress={() => setModal(false)}
                style={{ justifyContent: 'flex-start' }}
                isVisible={modal}>
                <View style={[{ marginTop: Metrics.screenHeight / 4 }]}>
                    <View style={{ marginHorizontal: 70, paddingTop: 12, paddingHorizontal: 8, paddingBottom: 15, borderRadius: 10, backgroundColor: Colors.white }}>
                        <Text style={[styles.modalTitleTxt, { alignSelf: 'center', marginBottom: 16 }]}>{`Apakah Anda yakin melakukan request ${sttb.jumlah} buku STTB?`}</Text>
                        <View style={[styles.flexHorizontal, { alignSelf: 'center', }]}>
                            <Button width={60} containerStyle={{ paddingVertical: 8, backgroundColor: 'rgba(0, 0, 0, 0.55)' }} onPress={() => setModal(false)} text="Batal" />
                            <View style={{ marginLeft: 1 }} />
                            <Button width={60} containerStyle={{ paddingVertical: 8 }} onPress={() => OnPressOK()} text="Ya" />
                        </View>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

const validationSchema = yup.object().shape({
    jumlah: yup
        .string()
        .required("Silahkan jumlah STTB")
        .test('isPhone', 'Masukkan jumlah STTB dengan benar',
            value => {
                return value && value.match(constants.accNumberCriteria);
            }),
})

RequestScreen.propTypes = {
    onSendPress: PropTypes.func,
};

export { RequestScreen };