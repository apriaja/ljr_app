// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    container: {
        paddingHorizontal: 9,
        paddingVertical: 10,
        borderRadius: 10,
        backgroundColor: 'rgba(196, 224, 227, 0.52)',
    },
    titleTxt: {
        ...Fonts.notoSans,
        ...Fonts.description,
        fontWeight: '600',
        letterSpacing: -0.33,
        color: Colors.black,
    },
    tncTxt: {
        ...Fonts.notoSansBold,
        fontSize: 9,
        letterSpacing: -0.33,
        color: "rgba(0, 0, 0, 0.62)",
    },
    modalTitleTxt: {
        ...Fonts.notoSansBold,
        ...Fonts.tag,
        textAlign: 'center',
        letterSpacing: -0.33,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;