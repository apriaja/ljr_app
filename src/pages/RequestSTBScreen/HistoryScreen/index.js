import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, ScrollView } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconArrowTop from '../../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../../assets/images/ico.ArrowBottom.svg';
import IconFilter from '../../../assets/images/ico.filter.svg';
import { Colors } from '../../../themes';

let StatusTxtColor = {
    "Selesai": '#38DE34',
    "Sedang Diproses": '#DE9034',
    "Gagal": '#DE3434'
}

let filterItem = [
    {
        title: "Semua data",
        value: "all"
    },
    {
        title: "STTB sedang diproses",
        value: "Sedang Diproses"
    },
    {
        title: "STTB gagal diproses",
        value: "Gagal"
    },
    {
        title: "STTB sudah selesai diproses",
        value: "Selesai"
    },
]

const HistorySTTBScreen = ({ data }) => {
    const [listItem, setListItem] = useState([]);
    const [showFilter, setShowFilter] = useState(false);

    useEffect(() => {
        setShowFilter(false)
        let temp = data.map(elm => ({ ...elm, collaps: true }))
        setListItem(temp)
    }, []);

    const onPressItem = (item, index) => {
        let temp = [...listItem]
        temp[index].collaps = !listItem[index].collaps
        setListItem(temp)
    }

    const onPressFilter = (item) => {
        setShowFilter(false)
        if (item.value == "all") {
            setListItem(data)
        } else {
            let result = [];
            data.map(elm => {
                elm.status.toLowerCase().includes(item.value.toLowerCase()) && result.push(elm)
            })
            setListItem(result)
        }
    }

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={[styles.flexHorizontal, { alignSelf: 'flex-end', paddingVertical: 6, marginBottom: 5, marginTop: -9 }]}>
                <Text style={[styles.filterTxt, { marginRight: 4 }]}>{"Filter daftar"}</Text>
                <IconFilter />
            </TouchableOpacity>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={listItem}
                renderItem={({ item, index }) => renderItem(item, index, onPressItem)}
                keyExtractor={(item, index) => index.toString()}
                extraData={listItem}
            />
            {
                showFilter &&
                <View style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }}>
                    <TouchableOpacity activeOpacity={1} onPress={() => setShowFilter(!showFilter)} style={{ position: 'absolute', top: 0, bottom: 0, right: 0, left: 0 }} />
                    <View style={{ backgroundColor: Colors.white, marginTop: 18, paddingHorizontal: 10, paddingHorizontal: 5, alignSelf: 'flex-end', borderRadius: 5, borderWidth: 1, borderColor: "rgba(165, 164, 164, 1)" }}>
                        <FlatList
                            style={{ flexGrow: 0 }}
                            data={filterItem}
                            renderItem={({ item, index }) => (
                                <TouchableOpacity activeOpacity={1} onPress={() => onPressFilter(item)} style={{ paddingVertical: 5 }}>
                                    <Text style={styles.filterTxt}>{item.title}</Text>
                                </TouchableOpacity>
                            )}
                            keyExtractor={(item, index) => index.toString()}
                        />
                    </View>
                </View>
            }
        </View>
    )
}

const renderItem = (item, index, onPressItem) => {
    return (
        <View style={{ backgroundColor: "rgba(196, 224, 227, 0.52)", borderRadius: 10, padding: 10, marginBottom: 10, }}>
            <View style={[styles.flexHorizontal, { marginLeft: 6, justifyContent: 'space-between' }]}>
                <Text style={styles.textHeader}>{item.id}</Text>
                <View style={styles.flexHorizontal}>
                    <Text style={styles.textHeader}>{item.date}</Text>
                    <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item, index)} style={[styles.justifyAlignCenter, { paddingHorizontal: 10, marginBottom: -10, marginTop: -5, marginRight: -10 }]}>
                        {
                            item.collaps ? <IconArrowBottom /> : <IconArrowTop />
                        }
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{ marginTop: 9, marginBottom: 7, height: 1, width: "100%", backgroundColor: "rgba(0, 0, 0, 0.43)" }} />
            <View style={[styles.flexHorizontal, { justifyContent: 'space-between', marginLeft: 6 }]}>
                <Text style={styles.textDesc}>{`${item.jumlah} buku E-STTB`}</Text>
                <Text style={[styles.textDesc, { color: StatusTxtColor[item.status] }]}>{item.status}</Text>
            </View>
            {
                !item.collaps &&
                <View>
                    <View style={{ marginLeft: 6, marginTop: 12 }}>
                        <Text style={styles.textDesc}>{item.cust}</Text>
                        <Text style={styles.textDesc}>{item.alamat}</Text>
                    </View>
                </View>
            }
        </View>
    )
}

HistorySTTBScreen.propTypes = {
    data: PropTypes.array,
};

export { HistorySTTBScreen };