import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { Header, Tab } from '../../components';

// Redux
import { setLoading } from '../../actions';

// Style
import styles from './styles'

// Constans
import constants from '../../common/constants';

// Page
import { RequestScreen } from './RequestScreen';
import { HistorySTTBScreen } from './HistoryScreen';

// Navigation
import navigationService from '../../navigation/navigationService';

import dummy from '../../common/dummy';


let TAB = [
    { id: 0, title: 'Request' },
    { id: 1, title: 'History Request' },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 0,
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    onPressRequest = (data) => {
        console.log("data: ", data)
        this.props.setLoading(true)
        setTimeout(() => {
            this.props.setLoading(false)
            // this.onBack()
            navigationService.navigate("SuccessScreen", { from: constants.fromScreen.REQUESTSTTB })
        }, 2000)
    }

    onSelectedTab = (data) => {
        const setActiveData = {
            '0': () => { this.setState({ page: 0 }) },
            '1': () => { this.setState({ page: 1 }) },
        };
        setActiveData[data.id]();
    }

    renderPage = (page) => {
        let itemToRender = null
        const setActiveData = {
            '0': () => { itemToRender = this.renderFirstPage() },
            '1': () => { itemToRender = this.renderSecondPage() },
        };
        setActiveData[page]();

        return (
            <View style={{ flex: 1, marginTop: 11, }}>
                {itemToRender}
            </View>
        )
    }

    renderFirstPage = () => {
        return (
            <RequestScreen
                onSendPress={this.onPressRequest}
            />
        )
    }
    renderSecondPage = () => {
        return (
            <HistorySTTBScreen
                data={dummy.dataHistorySTTB}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title="Request STTB" onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 9 }}>
                    <Tab data={TAB} onSelectTab={(data) => this.onSelectedTab(data)} />
                    {this.renderPage(this.state.page)}
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
