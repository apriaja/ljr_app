import React, { Component } from 'react';
import { View, Alert, Text, ActivityIndicator } from 'react-native';
import ReduxNavigation from '../../navigation/appNavigation';
import { connect } from 'react-redux';
import API from '../../services/api';
import NavigationService from '../../navigation/navigationService';

// Import Components
// import { MyStatusBar } from '../../components/';

// Themes
import { ApplicationStyles, Colors } from '../../themes';

class RootContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
    }
  }

  componentDidMount() {
    this.handleUnauthorized();
  }

  // Component Lifecycle
  static getDerivedStateFromProps = (newProps, prevState) => {
    console.log("static: ", newProps)
    if (newProps.isLoading != null) {
      return { ...prevState, isLoading: newProps.isLoading };
    } else {
      return { ...prevState }
    }
  };

  handleUnauthorized = () => {
    // const api = API;
    // api.addResponseTransmitter(
    //   () => { this.props.setOffline(true); }, //for callback setoffline
    //   () => {
    //     Alert.alert( //for callback autologin
    //       I18n.t('HomeScreen.attention'),
    //       I18n.t('HomeScreen.relogin'),
    //       [
    //         {
    //           text: I18n.t('HomeScreen.login'), onPress: () => {
    //             this.moveToLogin()
    //           }
    //         }
    //       ],
    //     );
    //     this.handleLogout();
    //   },
    //   () => { this.props.setSystemError(true); }, //for callback setsystemerror
    //   () => { this.moveToMaintenance() }
    // );
  }

  render() {
    return (
      <View style={ApplicationStyles.flexContainer}>
        {/* <MyStatusBar /> */}
        <ReduxNavigation
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
        />
        {
          this.state.isLoading &&
          <View style={{ position: 'absolute', top: 0, bottom: 0, left: 0, right: 0, backgroundColor: 'rgba(0,0,0,0.4)' }}>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
              <ActivityIndicator size="large" color={Colors.redSoft} />
            </View >
          </View>
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    isLoading: state.appReducer.loading
  };
};

const mapDispatchToProps = (dispatch) => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(RootContainer);
