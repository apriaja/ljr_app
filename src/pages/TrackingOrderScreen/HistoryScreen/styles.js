// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../../themes';
import Utils, { isTableted } from '../../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    titleTxt: {
        ...Fonts.notoSans,
        fontSize: 13,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    dateTxt: {
        ...Fonts.notoSansBold,
        fontSize: 11,
        letterSpacing: -0.333,
        color: Colors.black,
    },
    addressTxt: {
        ...Fonts.notoSans,
        ...Fonts.tag,
        letterSpacing: -0.333,
        color: 'rgba(0, 0, 0, 0.66)',
    }
});

// Make the styles available for ActivityScreens
export default styles;