import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconArrowTop from '../../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../../assets/images/ico.ArrowBottom.svg';

const HistoryScreen = ({ data, onPressItem }) => {
    const [listItem, setListItem] = useState([]);

    useEffect(() => {
        let temp = data.map(elm => ({ ...elm, collaps: true }))
        setListItem(temp)
    }, []);

    const onPressItemCollaps = (item, index) => {
        let temp = [...listItem]
        temp[index].collaps = !listItem[index].collaps
        setListItem(temp)
    }

    return (
        <View style={{ flex: 1, marginTop: 5 }}>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={listItem}
                renderItem={({ item, index }) => (
                    <TouchableOpacity activeOpacity={1} onPress={() => onPressItem(item)} style={{ backgroundColor: "rgba(196, 224, 227, 0.52)", borderRadius: 10, marginBottom: 10 }}>
                        <View style={[styles.flexHorizontal, { justifyContent: 'space-between', alignItems: 'center', paddingVertical: 6, paddingHorizontal: 8 }]}>
                            <Text style={styles.titleTxt}>{item.deliveryNumber}</Text>
                            <View style={[styles.flexHorizontal, { justifyContent: 'center' }]}>
                                <Text style={styles.dateTxt}>{item.date}</Text>
                                <TouchableOpacity activeOpacity={1} onPress={() => onPressItemCollaps(item, index)} style={[styles.justifyAlignCenter, { paddingHorizontal: 10, marginBottom: -8, marginTop: -5, marginRight: -10 }]}>
                                    {
                                        item.collaps ? <IconArrowBottom /> : <IconArrowTop />
                                    }
                                </TouchableOpacity>
                            </View>
                        </View>
                        <View style={{ height: 2, width: "100%", backgroundColor: "rgba(0, 0, 0, 0.2)" }} />
                        <View style={{ paddingHorizontal: 8, paddingTop: 2, paddingBottom: 4 }}>
                            <Text style={styles.titleTxt}>{item.sttbNumber}</Text>
                            <View style={{ marginLeft: 5, marginBottom: 8 }}>
                                <Text style={styles.titleTxt}>Dari</Text>
                                <View style={{ marginLeft: 11 }}>
                                    <Text style={[styles.titleTxt, { marginBottom: 5 }]}>{item.from.name}</Text>
                                    <Text style={styles.addressTxt}>{item.from.address}</Text>
                                </View>
                            </View>
                            {
                                !item.collaps &&
                                <View style={{ marginLeft: 5, marginBottom: 8 }}>
                                    <Text style={styles.titleTxt}>Ke</Text>
                                    <View style={{ marginLeft: 11 }}>
                                        <Text style={[styles.titleTxt, { marginBottom: 5 }]}>{item.destination.name}</Text>
                                        <Text style={styles.addressTxt}>{item.destination.address}</Text>
                                    </View>
                                </View>
                            }
                        </View>
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
}

HistoryScreen.propTypes = {
    data: PropTypes.array,
    onPressItem: PropTypes.func,
};

export { HistoryScreen };