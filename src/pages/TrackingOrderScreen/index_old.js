import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'
import _ from 'lodash';

// Navigation
import navigationService from '../../navigation/navigationService';

// Component
import { Header, Tab } from '../../components';

// Style
import styles from './styles'

import { HistoryScreen } from './HistoryScreen';
import { OnProgressScreen } from './OnProgressScreen';
import dummy from '../../common/dummy';

let TAB = [
    { id: 0, title: 'Dalam Proses' },
    { id: 1, title: 'Riwayat' },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            page: 0
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        navigationService.navigate("HomeScreen")
        // this.props.navigation.goBack();
    }

    onSelectedTab = (data) => {
        const setActiveData = {
            '0': () => { this.setState({ page: 0 }) },
            '1': () => { this.setState({ page: 1 }) },
        };
        setActiveData[data.id]();
    }

    renderPage = (page) => {
        let itemToRender = null
        const setActiveData = {
            '0': () => { itemToRender = this.renderFirstPage() },
            '1': () => { itemToRender = this.renderSecondPage() },
        };
        setActiveData[page]();

        return (
            <View style={{ flex: 1, marginTop: 11, }}>
                {itemToRender}
            </View>
        )
    }

    gotoDetail = (data) => {
        navigationService.navigate('TrackingOrderDetailScreen', { data: data })
    }

    renderFirstPage = () => {
        let temp = _.filter(dummy.dataTrackingOrder, { status: 'Sedang Dikirim' })
        return (
            <OnProgressScreen
                data={temp}
                onPressItem={(data) => this.gotoDetail({ ...data, title: 'Sedang Dikirim' })}
            />
        )
    }
    renderSecondPage = () => {
        let temp = _.filter(dummy.dataTrackingOrder, { status: 'Sampai Tujuan' })
        return (
            <HistoryScreen
                data={temp}
                onPressItem={(data) => this.gotoDetail({ ...data, title: 'Sampai Tujuan' })}
            />
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title="Tracking Order" onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 9 }}>
                    <Tab data={TAB} onSelectTab={this.onSelectedTab} />
                    {this.renderPage(this.state.page)}
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
