import PropTypes from 'prop-types';
import React, { useState, useEffect, useCallback } from 'react';
import { FlatList, Text, TouchableOpacity, View, } from 'react-native';
import { Rating, AirbnbRating } from 'react-native-ratings';

// Themes and Styles
import styles from './styles';

// Component
import { TextInputBase, Button } from '../../components';

const RatingInputScreen = ({ data, onSubmitRate, idRate }) => {
    // const [listItem, setListItem] = useState([]);
    const [dataRating, setDataRating] = useState({
        rate: 0,
        feedBack: null,
    });
    
    useEffect(() => {
        // let temp = data.map(elm => ({ ...elm, collaps: true }))
        // setListItem(temp)
    }, []);

    return (
        <View style={{ flex: 1, margin: 25 }}>
            <Text style={{fontWeight: 'bold'}}>Rate pengirimanmu?</Text>
            <Rating
                showRating={false}
                fractions={1}
                startingValue={0}
                minValue={0}
                onFinishRating={(rateVal) => setDataRating({...dataRating, rate: rateVal})}
                jumpValue={.5}
                style={{ paddingVertical: 20, }}
            />
            <TextInputBase
                styleContainer={{ borderColor: '#000', backgroundColor: '#FCFCFC', borderRadius: 5, marginTop: 5, marginBottom: 5 }}
                placeholder="Tulis komentarmu di sini!..."
                isMultiline={true}
                value={null}
                keyboardType={'ascii-capable'}
                onChangeText={(feedBackVal) => setDataRating({...dataRating, feedBack: feedBackVal})}
                // onBlur={formikProps.handleBlur("volume")}
                onBlur={() => {}}
                styleText={{ paddingVertical: 10, paddingLeft: 20 }}
            />
            <View style={[{ alignSelf: 'stretch', marginTop: 15, }]}>
                <Button containerStyle={{borderRadius: 40, alignSelf: 'stretch', height: 50}} 
                    onPress={() => onSubmitRate(idRate, dataRating)} 
                    text="Kirim" 
                    disabled={dataRating.feedBack === null || dataRating.feedBack === ''}    
                />
            </View>
        </View>
    )
}

RatingInputScreen.propTypes = {
    data: PropTypes.array,
    idRate: PropTypes.any,
    onSubmitRate: PropTypes.func,
};

export default RatingInputScreen;