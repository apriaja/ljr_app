import React, { Component } from 'react'
import { FlatList, View, Text, TouchableOpacity, BackHandler, Alert } from 'react-native'
import CardView from 'react-native-cardview';
// alternative from @gorhom/bottom-sheet@^4 (it used reanimated v2 and make error)
import RBSheet from "react-native-raw-bottom-sheet";

// Library
import { connect } from 'react-redux'
import _ from 'lodash';
import moment from 'moment';

// Navigation
import navigationService from '../../navigation/navigationService';

import api from '../../services/api';
import Config from 'react-native-config';
import { setLoading } from '../../actions';
import constants from '../../common/constants';

// Component
import { Header, Tab, Button } from '../../components';
import TimeLineCustom from '../../components/TimeLineCustom';
import RatingInputScreen from './rating';

// Style
import styles from './styles'
import { Fonts, Colors } from '../../themes';

// Assets
import IconArrowTop from '../../assets/images/ico.ArrowTop.svg';
import IconArrowBottom from '../../assets/images/ico.ArrowBottom.svg';

import dummy from '../../common/dummy';

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listItem: null,
            selected: null
        };
        this.onEventPressTimeline = this.onEventPressTimeline.bind(this)
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });

        this.props.setLoading(true)
        // let response = await api.fetchAPI(`${Config.API_NGROK}/orderPickup/tracking?customer_id=7100000`, null, 'GET')
        if (this.props.profile && this.props.profile.id) {
            response = await api.fetchAPI(`${Config.API_NGROK}/orderPickup/tracking?customer_id=${this.props.profile.id}`, null, 'GET')
        } else {
            this.props.setLoading(false)
            Alert.alert(
                "Error",
                "Silahkan login dahulu!"
            );
        }
        
        if (response && response.data.success) {
            console.log('componentDidMount', JSON.stringify(response.data.data))
            this.setState({
                listItem: response.data.data.map(elm => ({ ...elm, collaps: true }))
            })
        }
        this.props.setLoading(false)
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        navigationService.navigate("HomeScreen")
        // this.props.navigation.goBack();
    }

    // indexParent -> top level listItem, index -> index addr
    onPressItem = (indexParent, index = null) => {
        const { listItem } = this.state;
        let temp = [...listItem]
        if (index !== null) {
            temp[indexParent]['dataAlamat'][index].collaps = !listItem[indexParent]['dataAlamat'][index].collaps
        } else {
            temp[indexParent].collaps = !listItem[indexParent].collaps
        }
        this.setState({
            listItem: temp
        });
    }

    onEventPressTimeline(data) {
        this.setState({ selected: data })
        navigationService.navigate('TrackingOrderDetailScreen', {data: data})
    }

    onSubmitRate(idRate, dataRating) {
        alert(JSON.stringify(dataRating))
        this[RBSheet + idRate].close();
    }

    /**
     * 
     * @param {int} indexParent 
     * @param {int} itemP 
     * @returns 
     */
    renderContent = (indexParent, itemP, idPu) => {
        console.log('renderContent', JSON.stringify(itemP))
        const mappedDataTimeLine = itemP.list_shipment.map(
            obj => ({ 
                title: obj.nama_pic_penerima, 
                description: `CP: ${obj.tlp_pic_pengirim} (${obj.nama_pic_pengirim})\n${obj.receipt_address.alamat}`,
                time: obj.tgl_entry.split(' ')[1],
                noPu: idPu,
                noShipmentEntry: obj.no_shipment_entry
            })
        )
        return (
            <View style={{ paddingRight: 5 }}>
                <TimeLineCustom
                    data={mappedDataTimeLine}
                    showTime={false}
                    innerCircleType={'dot'}
                    dotSize={7}
                    circleSize={13}
                    lineWidth={1.5}
                    dashLine={true}
                    circleColor='#DE9034'
                    lineColor='#DE9034'
                    onEventPress={this.onEventPressTimeline}
                />
                {/* {
                    constants.role.USER.includes(this.props.profile.role_id.toString()) && 
                    <Button width={90} containerStyle={{ backgroundColor: false ? "rgba(0, 102, 255, 0.49)" : 'rgba(0, 102, 255, 1)', paddingVertical: 9, margin: 12, marginLeft: 'auto' }}
                        text="Beri Ulasan"
                        onPress={() => this[RBSheet + indexParent + idPu].open()}
                    />
                } */}
                <RBSheet
                    ref={ref => { this[RBSheet + indexParent + idPu] = ref; }}
                    animationType={'fade'}
                    closeOnDragDown
                    dragFromTopOnly
                    height={360}
                    closeDuration={250}
                    customStyles={{
                        container: { borderTopLeftRadius: 20, borderTopRightRadius: 20 },
                        wrapper: { padding: 2 },
                        draggableIcon: { backgroundColor: '#2C4074' }
                    }}
                >
                    <RatingInputScreen 
                        idRate={indexParent + idPu}
                        onSubmitRate={(idRate, dataRating) => this.onSubmitRate(idRate, dataRating)}
                    />   
                </RBSheet>
            </View>
        );
    }

    render() {
        const { listItem } = this.state;
        return (
            <View style={styles.container}>
                <Header title="Tracking Order" onBackPress={() => this.onBack()} />
                <View style={{ flex: 1, padding: 7 }}>
                    <FlatList
                        contentContainerStyle={{ paddingHorizontal: 3, }}
                        data={listItem}
                        renderItem={({ item, index }) => (
                            <View style={{ overflow: 'hidden', borderWidth: 1, borderColor: "rgba(0, 0, 0, 0.13)", borderRadius: 10, }}>
                                {/* accordion */}
                                <TouchableOpacity activeOpacity={1} onPress={() => this.onPressItem(index)}>
                                    <View style={[styles.flexHorizontal, { alignItems: 'center', backgroundColor: "#C4E0E3", justifyContent: 'space-between', paddingTop: 11, paddingBottom: 14, paddingLeft: 11, paddingRight: 8 }]}>
                                        <View style={[styles.flexVertical]}>
                                            <Text style={styles.noPuTxt}>{item.no_order_pickup}</Text>
                                            <Text style={{ ...Fonts.soraRegular, fontSize: 12, marginTop: 4, color: Colors.black }}>{moment(item.tanggal_order).format("DD MMM YYYY")}</Text>
                                        </View>
                                        <View style={[styles.flexHorizontal, { alignItems: 'center' }]}>
                                            <CardView cardElevation={3} cardMaxElevation={3} cornerRadius={7} style={[styles.statusContainer, { marginLeft: 10, }]}>
                                                <Text style={[styles.statusTxt, { color: Colors.StatusTxtColor[item.status.nama_status.split(" ").join("_")] }]}>{item.status.nama_status}</Text>
                                            </CardView>
                                            <View style={[styles.justifyAlignCenter, { paddingVertical: 11, paddingHorizontal: 11, marginBottom: -5, marginTop: -5, marginRight: -11 }]}>
                                                {item.collaps ? <IconArrowBottom /> : <IconArrowTop />}
                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                {/* end accordion */}
                                {
                                    !item.collaps && this.renderContent(index, item, item.no_order_pickup)
                                }
                            </View>
                        )}
                        extraData={listItem}
                        ItemSeparatorComponent={() => (<View style={{ width: '100%', padding: 3, }} />)}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        profile: state.persistReducer.profile,
    }
}

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
