// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
  ...applicationStyles,
  ...Fonts,
  container: {
    flex: 1,
    backgroundColor: Colors.white
  },
  statusTxt: {
    ...Fonts.poppinsMedium,
    ...Fonts.subTag,
    textTransform: 'uppercase',
    letterSpacing: 0.05,
    color: "#0066FF",
  },
  statusContainer: {
    paddingVertical: 3,
    paddingHorizontal: 23,
    backgroundColor: Colors.white,
    borderRadius: 7,
  },
  cover: {
    backgroundColor: "rgba(0,0,0,.5)",
  },
});

// Make the styles available for ActivityScreens
export default styles;