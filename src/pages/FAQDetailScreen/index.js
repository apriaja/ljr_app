import React, { Component } from 'react'
import { View, Text, BackHandler } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { Header } from '../../components';

// Style
import styles from './styles'

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.navigation.getParam('data') ? this.props.navigation.getParam('data') : {}
        };
    }

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title={this.state.data.title} onBackPress={() => this.onBack()} />
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
