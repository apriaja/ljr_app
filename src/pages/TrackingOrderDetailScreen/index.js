import React, { Component } from 'react'
import { View, Text, BackHandler, FlatList, Image } from 'react-native'
import Dash from 'react-native-dash';
import Timeline from 'react-native-timeline-flatlist'

// Library
import { connect } from 'react-redux'
import api from '../../services/api';
import Config from 'react-native-config';
import { setLoading } from '../../actions';
import moment from 'moment';

// Component
import { Header } from '../../components';

// Style
import styles from './styles'
import { Colors } from '../../themes';

// Assets
import IconPickup from '../../assets/images/ico.BoxPickup.svg';
import IconWarehouse from '../../assets/images/ico.Warehouse.svg';
import OnProcess from '../../assets/images/Tracking/ico.OnProcess.svg';
import Ok from '../../assets/images/Tracking/ico.Ok.svg';
import Reject from '../../assets/images/Tracking/ico.Reject.svg';
import ShipmentEntry from '../../assets/images/Tracking/ico.ShipmentEntry.svg';
import Trouble from '../../assets/images/Tracking/ico.Trouble.svg';


let listTrackingIcon = [
    {
        label: 'OnProcess',
        logo: <OnProcess />
    },
    {
        label: 'Ok',
        logo: <Ok />
    },
    {
        label: 'Reject',
        logo: <Reject />
    },
    {
        label: 'ShipmentEntry',
        logo: <ShipmentEntry />
    },
    {
        label: 'Trouble',
        logo: <Trouble />
    },
]

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.navigation.getParam('data') ? this.props.navigation.getParam('data') : {},
            listItem: []
        };
        this.renderDetail = this.renderDetail.bind(this)
    }

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });

        this.props.setLoading(true)
        const response = await api.fetchAPI(`${Config.API_NGROK}/task/tracking`, {
            no_shipment_entry: this.state.data.noShipmentEntry,
            no_order_pickup: this.state.data.noPu
        }, 'POST')
        if (response.data.success) {
            this.setState({
                listItem: response.data.data
            })
        }
        this.props.setLoading(false)
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    renderDetail = (rowData, sectionID, rowID) => {
        let title = <Text style={[styles.title, {fontSize: 14}]}>{moment(rowData.created_at).format("HH:mm A")}</Text>
        var desc = null
        let icon = null

        if (['TERIMA', 'OK', 'NO TIME', 'NOT HOME', 'CLOSE ON ARRIVAL', 'ARRIVAL TO CUSTOMER', 'CUSTOMER CLOSED', 'RESCHEDULED', 'SHIPMENT NOT READY', 'ARRIVAL ORIGIN', 'SELESAI HANDOVER', 'ARRIVAL AFTER HANDOVER', 'DIKONFIRMASI OPS DONE'].includes(rowData.status.nama_status)) {
            icon = listTrackingIcon.filter(e => e.label === 'Ok')[0]['logo']
        } else if (['ON PROCESS', 'ON THE WAY'].includes(rowData.status.nama_status)) {
            icon = listTrackingIcon.filter(e => e.label === 'OnProcess')[0]['logo']
        } else if (rowData.status.nama_status == 'SHIPMENT ENTRY') {
            icon = listTrackingIcon.filter(e => e.label === 'ShipmentEntry')[0]['logo']
        } else if (rowData.status.nama_status == 'ARMADA TROUBLE') {
            icon = listTrackingIcon.filter(e => e.label === 'Trouble')[0]['logo']
        } else if (['PICKUP CANCEL', 'REJECT'].includes(rowData.status.nama_status)) {
            icon = listTrackingIcon.filter(e => e.label === 'Reject')[0]['logo']
        }

        if (rowData.status.keterangan)
            desc = (
                <View style={{ flex: 1, flexDirection: 'row', }}>
                    { icon }
                    <Text style={{marginLeft: 10}}>{rowData.status.keterangan}</Text>
                </View>
            )

        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{marginRight: 10}}>{title}</View>
                {desc}
            </View>
        )
    }

    renderMainPage = () => {
        return (
            <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 9 }}>
                <View style={{ flexDirection: "row", justifyContent: 'space-between', alignItems: 'center', marginTop: 15 }}>
                    <View style={{ alignItems: 'center', }}>
                        <View style={[styles.shadowBox, { backgroundColor: "#DBE4FF", width: 80, height: 80, borderRadius: 20, flexDirection: "column", justifyContent: 'center', alignItems: 'center', }]}>
                            <IconPickup height="50%" width="50%" />
                        </View>
                        <Text style={{ marginTop: 10, color: '#6E6E6E' }}>Pickup</Text>
                    </View>
                    <Dash dashColor='#DBE4FF' dashLength={6} dashGap={4} dashThickness={3} style={{ flex: 1, height: 1, margin: 12, }} />
                    <View style={{ alignItems: 'center', }}>
                        <View style={[styles.shadowBox, { backgroundColor: "#DBE4FF", width: 80, height: 80, borderRadius: 20, flexDirection: "column", justifyContent: 'center', alignItems: 'center', }]}>
                            <IconWarehouse height="50%" width="50%" />
                        </View>
                        <Text style={{ marginTop: 10, color: '#6E6E6E' }}>Warehouse</Text>
                    </View>
                </View>

                <View style={{ borderWidth: 1, marginTop: 15, padding: 10, borderRadius: 10, minHeight: '60%', borderColor: '#CCD1E4' }}>
                    <FlatList
                        data={this.state.listItem}
                        renderItem={({ item, index }) => (
                            <View style={styles.flexVertical}>
                                <Text>{moment(item.tanggal).format("MMM DD, YYYY")}</Text>
                                <Timeline
                                    data={item.task_details}
                                    detailContainerStyle={{ marginTop: -15 }}
                                    circleSize={10}
                                    descriptionStyle={{ color: 'gray' }}
                                    circleColor={'#281BBD'}
                                    lineColor={'#281BBD'}
                                    options={{
                                        style: { paddingTop: 10 }
                                    }}
                                    renderDetail={this.renderDetail}
                                    showTime={false}
                                // onEventPress={this.onEventPressTimeline}                            
                                />
                            </View>
                        )}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>

            </View>
        )
    }

    render() {
        const { title, noPu } = this.state.data;
        return (
            <View style={styles.container}>
                <Header title={noPu} description={title} onBackPress={() => this.onBack()} />
                {this.renderMainPage()}
            </View>
        )
    }
}

const mapStateToProps = state => {
    return {
        profile: state.persistReducer.profile,
    }
}

const mapDispatchToProps = (dispatch) => ({
    setLoading: (loading) => dispatch(setLoading(loading)),
});

export default connect(mapStateToProps, mapDispatchToProps)(index)
