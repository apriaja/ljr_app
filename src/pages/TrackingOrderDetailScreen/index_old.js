import React, { Component } from 'react'
import { View, Text, BackHandler, FlatList } from 'react-native'

// Library
import { connect } from 'react-redux'

// Component
import { Header } from '../../components';

// Style
import styles from './styles'
import { Colors } from '../../themes';

export class index extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.navigation.getParam('data') ? this.props.navigation.getParam('data') : {}
        };
    }

    componentDidMount() {
        console.log("data: ", this.state.data)
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            this.onBack(); // works best when the goBack is async
            return true;
        });
    }

    componentWillUnmount = () => {
        this.backHandler.remove();
    }

    onBack = () => {
        this.props.navigation.goBack();
    }

    renderMainPage = () => {
        return (
            <View style={{ flex: 1, paddingHorizontal: 23, marginTop: 9 }}>
                <View style={[styles.flexHorizontal, { justifyContent: 'space-between', marginBottom: 4, }]}>
                    <Text style={styles.textStyle}>{'Nomor delivery'}</Text>
                    <Text style={styles.textStyle}>{this.state.data.deliveryNumber}</Text>
                </View>
                <View style={[styles.flexHorizontal, { justifyContent: 'space-between', marginBottom: 8, }]}>
                    <Text style={styles.textStyle}>{'Nomor STTB'}</Text>
                    <Text style={styles.textStyle}>{this.state.data.sttbNumber}</Text>
                </View>
                <FlatList
                    data={this.state.data.orderTracking}
                    renderItem={({ item, index }) => (
                        <View style={styles.flexHorizontal}>
                            <View style={{ flex: 0.16, marginBottom: 5 }}>
                                <Text style={[styles.textStyle]}>{item.date}</Text>
                            </View>
                            <View style={{ alignSelf: index == 0 ? 'flex-end' : 'flex-start', height: index == 0 ? '85%' : this.state.data.orderTracking.length - 1 == index ? '25%' : '100%', width: 1, backgroundColor: this.state.data.orderTracking.length > 1 ? "rgba(31, 28, 28, 0.25)" : Colors.white }}>
                                <View style={[{ position: 'absolute', top: 0, marginLeft: -3, marginTop: index == 0 ? 0 : 5, width: 7, height: 7, borderRadius: 7 / 2, backgroundColor: index === 0 ? '#281BBD' : 'rgba(0, 0, 0, 0.62)' }]} />
                            </View>
                            <View style={{ flex: 0.8 }}>
                                <Text style={[styles.textStyle, { marginLeft: 10 }]}>{item.status}</Text>
                            </View>
                        </View>
                    )}
                    keyExtractor={(item, index) => index.toString()}
                />
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>
                <Header title={this.state.data.title} onBackPress={() => this.onBack()} />
                {this.renderMainPage()}
            </View>
        )
    }
}

const mapStateToProps = (state) => ({

})

const mapDispatchToProps = {

}

export default connect(mapStateToProps, mapDispatchToProps)(index)
