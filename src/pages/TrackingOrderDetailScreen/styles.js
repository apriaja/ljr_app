// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
  ...applicationStyles,
  ...Fonts,
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },
  textStyle: {
    ...Fonts.notoSans,
    fontSize: 13,
    letterSpacing: -0.333,
    color: Colors.black,
  },
  shadowBox: {
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 20,  
    elevation: 15 
  }
});

// Make the styles available for ActivityScreens
export default styles;