//Async Storage Library
import AsyncStorage from '@react-native-community/async-storage';
import { call, put, takeLatest } from 'redux-saga/effects';
import api from '../services/api';
import {
  LOGIN,
} from '../actions/types';
import {
  loginSuccess,
  loginFailed,
  setProfile,
} from '../actions/index';

import Utils from '../common/utils';
import Config from 'react-native-config';

/* eslint-disable import/prefer-default-export */
function* workerLogin(api, params) {
  try {
    console.log("Url Login apri : ",Config.API_NGROK);
    const response = yield call(api.loginAPI, params.payload);
    if (response.status === 200) {
      console.log("brhasil")
      yield put(loginSuccess(response.data));
      yield put(setProfile(response.data.user))
      yield call(api.setAuthToken, response.data.access_token);
    } else {
      console.log("ggl",response)
      yield put(loginFailed(response.data))
    }
    console.log("workerLogin user : ", response.data)
  } catch (error) {  
    console.log("workerLogin user: ", error)
  }
}

export const watcherAuth = [
  takeLatest(LOGIN, workerLogin, api)
]