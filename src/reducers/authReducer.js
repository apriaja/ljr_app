import {
   LOGIN,
   LOGIN_SUCCESS,
   LOGIN_FAILED,
   LOGOUT_COMPLETE,
} from '../actions/types';

const INITIAL_STATE = {
   loginResponse: null,
   duringActions: null,
};


export default (state, action) => {
   if (typeof state === 'undefined' || action.type === LOGOUT_COMPLETE) {
      return INITIAL_STATE
   }
   switch (action.type) {
      case LOGIN:
         return { ...state, duringActions: action.type, loginResponse: null }
      case LOGIN_SUCCESS:
         return { ...state, duringActions: action.type, loginResponse: action.payload }
      case LOGIN_FAILED:
         return { ...state, duringActions: action.type, loginResponse: action.payload }
      default:
         return state;
   }
};