import {
    ADD_REQUEST_PU, REMOVE_REQUEST_PU,
} from '../actions/types';

const INITIAL_STATE = {
    duringActions: null,
    requestPU: null,
};


export default (state, action) => {
    if (typeof state === 'undefined') {
        return INITIAL_STATE
    }
    switch (action.type) {
        case ADD_REQUEST_PU:
            return { ...state, duringActions: action.type, requestPU: action.payload }
        case REMOVE_REQUEST_PU:
            return { ...state, duringActions: action.type, requestPU: null }
        default:
            return state;
    }
};