import {
    LOADING
} from '../actions/types';

const INITIAL_STATE = {
    loading: false
};


export default (state, action) => {
    if (typeof state === 'undefined') {
        return INITIAL_STATE
    }
    switch (action.type) {
        case LOADING:
            return { ...state, loading: action.payload }
        default:
            return state;
    }
};