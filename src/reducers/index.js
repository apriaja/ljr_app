import { combineReducers } from 'redux';
import { LOGOUT_COMPLETE } from '../actions/types';

import AuthReducer from './authReducer';
import AppReducer from './appReducer';
import RequestPUReducer from './requestPUReducer';
import PersistReducer from './persistReducer';



const appReducer = combineReducers({
   authentication: AuthReducer,
   appReducer: AppReducer,
   reqPUReducer: RequestPUReducer,
   persistReducer: PersistReducer,
})

const rootReducer = (state, action) => {
   if (action.type === LOGOUT_COMPLETE) {
      // state = undefined
   }
   return appReducer(state, action)
}

export default rootReducer;