import {
    SET_PROFILE
} from '../actions/types';

const INITIAL_STATE = {
    profile: null,
    duringActions: null,
};


export default (state, action) => {
    if (typeof state === 'undefined') {
        return INITIAL_STATE
    }
    switch (action.type) {
        case SET_PROFILE:
            return { ...state, duringActions: action.type, profile: action.payload }
        default:
            return state;
    }
};