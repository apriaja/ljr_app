import { all } from 'redux-saga/effects'

import { watcherAuth } from '../sagas/authSaga'

export default function* reduxSaga() {
    yield all([
        ...watcherAuth,
    ])
}