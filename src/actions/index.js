import {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_FAILED,
  LOGOUT,
  LOGOUT_SUCCESS,
  LOGOUT_FAILED,
  LOGOUT_COMPLETE,
  LOADING,
  SET_PROFILE,
} from './types';

export const setLoading = isLoading => {
  return {
    type: LOADING,
    payload: isLoading,
  };
};

export const loginActions = value => {
  return {
    type: LOGIN,
    payload: value,
  };
};

export const loginSuccess = response => {
  return {
    type: LOGIN_SUCCESS,
    payload: response,
  };
};

export const loginFailed = response => {
  return {
    type: LOGIN_FAILED,
    payload: response,
  };
};

export const logoutActions = data => {
  return {
    type: LOGOUT,
    payload: data,
  };
};

export const logoutForceActions = data => {
  return {
    type: LOGOUT_FORCE,
    payload: data,
  };
};

export const logoutSuccess = text => {
  return {
    type: LOGOUT_SUCCESS,
    payload: text,
  };
};

export const logoutFailed = text => {
  return {
    type: LOGOUT_FAILED,
    payload: text,
  };
};

export const logoutComplete = () => {
  return {
    type: LOGOUT_COMPLETE,
  };
};

export const setProfile = (data) => {
  return {
    type: SET_PROFILE,
    payload: data,
  };
};