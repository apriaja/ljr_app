// Loading
export const LOADING = 'LOADING'

// Login
export const LOGIN = 'login';
export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_FAILED = 'login_failed';

// Logout
export const LOGOUT = 'logout';
export const LOGOUT_FORCE = 'logout_force';
export const LOGOUT_SUCCESS = 'logout_success';
export const LOGOUT_FAILED = 'logout_failed';
export const LOGOUT_COMPLETE = 'logout_complete';

// Request PU
export const ADD_REQUEST_PU = 'ADD_REQUEST_PU';
export const REMOVE_REQUEST_PU = "REMOVE_REQUEST_PU";

// Profile
export const SET_PROFILE = "SET_PROFILE";