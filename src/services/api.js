import apisauce from 'apisauce';
import Config from 'react-native-config';
// import { fetch } from 'react-native-ssl-pinning';
import constants from '../common/constants';
import { appVersion } from '../common/utils';
import { Platform } from 'react-native';
import _ from 'lodash';

const create = (baseURL = '') => {
  const api = apisauce.create({
    baseURL,
    headers: {
      'Cache-Control': 'no-cache',
      'App-Version': appVersion,
      OS: Platform.OS,
    },
    timeout: 30000,
  });

  let headersTemp = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'App-Version': appVersion,
    OS: Platform.OS,
  };

  const setAuthToken = async userAuth => {
    api.setHeader('access-token', userAuth);
    headersTemp = {
      ...headersTemp,
      'access-token': userAuth,
    };
  };

  let callbackSetOffline = () => { };
  let callbackAlertRelogin = () => { };
  let callbackSystemError = () => { };
  let callbackMaintenance = () => { };

  const addResponseTransmitter = async (setOffline = () => { }, alertRelogin = () => { }, systemError = () => { }, maintenance = () => { }) => {
    callbackSetOffline = setOffline;
    callbackAlertRelogin = alertRelogin;
    callbackSystemError = systemError;
    callbackMaintenance = maintenance;
  };
  const config = {
    timeoutInterval: 30000,
    // sslPinning: { certs: [''] },
  };

  const responseParse = async response => {
    try {
      let systemError = {
        message: 'unsuccessful',
        err_code: 'systemError',
      };
      
      // let bodyString = await response.json()
      let bodyString = await response.json() || response
      // console.log("response : ",bodyString);
      
      let responseParser = await Object.assign({
        data: _.isEmpty(bodyString) ? systemError : bodyString,
        status: response.status,
        headers: response.headers,
      });
      // console.log("msg API", JSON.stringify(bodyString))
      return responseParser;
    } catch (er) {
      return await Object.assign({
        data: {
          message: 'unsuccessful',
          err_code: 'systemError:'+er,
        },
        status: response.status,
        headers: response.headers,
      });
    }
  };
  const post_methode = 'POST';
  const get_methode = 'GET';
  const delete_methode = 'DELETE';
  const patch_methode = 'PATCH';

  const loginAPI = body =>
  fetch(Config.API_NGROK + '/auth/login', {
    method: post_methode,
    body: body ? JSON.stringify(body) : '{}',
    ...config,
    headers: headersTemp,
  }).then(response => responseParse(response))
    .catch(err => responseParse(err));



  const http_methods = ['GET', 'POST', 'PUT', 'DELETE', 'PATCH'];

  // general purpose, user define
  const fetchAPI = (url, body, http_method) => {
        
      console.log("Fetching data...", url, `(${http_method})`)

      if (!http_methods.includes(http_method.toUpperCase())) {
          throw responseParse("Not Supported HTTP Method - REST API")
      }

      let defaultConfig = {
          method: http_method,
          ...config,
          headers: headersTemp
      }

      if (!['GET',].includes(http_method.toUpperCase())) {
          defaultConfig = {
              method: http_method,
              body: body ? JSON.stringify(body) : '{}',
              ...config,
              headers: headersTemp
          }
      }

      console.log("Fetching data...", defaultConfig);

      return fetch(url, defaultConfig)
        .then(response => responseParse(response))
        .catch(err => responseParse(err));
  }

  // general purpose, user define
  const fetchAPIUpload = (url, bodyFormData) => {
        
    console.log("Fetching data upload...", url, `(POST)`)

    let defaultConfig = {
        method: 'post',
        body: bodyFormData,
        redirect: 'follow'
    }

    console.log("Fetching data upload...", defaultConfig);

    return fetch(url, defaultConfig)
      .then(response => responseParse(response))
      .catch(err => responseParse(err));
  }

  return {
    api,
    setAuthToken,
    addResponseTransmitter,
    loginAPI,
    fetchAPI,
    fetchAPIUpload
  };
};
export default create();
