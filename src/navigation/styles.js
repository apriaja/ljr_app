// Import libraries
import { StyleSheet } from 'react-native';

// Themes
import { Colors, ApplicationStyles, Fonts } from '../themes';
import { isTableted, hasNotch } from '../common/utils';

const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
	...applicationStyles,
	...Fonts,
})

export const styleBar = {
	bottomBar: {
		tabBarOptions: {
			labelStyle: {
				alignSelf: 'center',
			},
			style: {
				// backgroundColor: Colors.white,
				// backgroundColor: 'blue',
				height: isTableted ? 59 : 50,
				alignItems: 'center',
				justifyContent: 'center',
			},
			tabStyle: {
				flexDirection: 'column',
				alignItems: 'center',
				justifyContent: 'center',
			},
			allowFontScaling: false,
		},
	},
};


// Make the styles available for Header styles
export default styles;
