import React from 'react';
import { Text, View } from 'react-native'

// React Native Library for Navigation
import { createAppContainer, getActiveChildNavigationOptions } from 'react-navigation';
import { createStackNavigator } from "react-navigation-stack"
import { createBottomTabNavigator } from 'react-navigation-tabs'

// Styles
import { styleBar } from './styles';

// Components
import { TabLabel } from '../components';

// Navigation
import HomeScreen from '../pages/HomeScreen';
import ProfileScreen from '../pages/ProfileScreen';
import RequestSTBScreen from '../pages/RequestSTBScreen';
import TrackingOrderScreen from '../pages/TrackingOrderScreen';
import InitialScreen from '../pages/InitialScreen';
import MainScreen from '../pages/MainScreen';
import LoginScreen from '../pages/LoginScreen';
import FAQScreen from '../pages/FAQScreen';
import FAQDetailScreen from '../pages/FAQDetailScreen';
import RequestPickUpScreen from '../pages/RequestPickUpScreen';
import DaftarSTTBScreen from '../pages/DaftarSTTBScreen';
import LiveChatScreen from '../pages/LiveChatScreen';
import TrackingOrderDetailScreen from '../pages/TrackingOrderDetailScreen';
import SignatureScreen from '../pages/SignatureScreen';
import SuccessScreen from '../pages/SuccessScreen';
import AddressScreen from '../pages/AddressScreen';


// Assets
import IconHomeSelected from '../assets/images/Tab/ico.HomeSelected.svg';
import IconHomeNotSelected from '../assets/images/Tab/ico.HomeNotSelected.svg';
import IconTrackerSelected from '../assets/images/Tab/ico.TrackerSelected.svg';
import IconTrackerNotSelected from '../assets/images/Tab/ico.TrackerNotSelected.svg';
import IconProfileSelected from '../assets/images/Tab/ico.ProfileSelected.svg';


// Stack Navigator
const StackHome = createStackNavigator(
    {
        HomeScreen: {
            screen: HomeScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        FAQScreen: {
            screen: FAQScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        FAQDetailScreen: {
            screen: FAQDetailScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        DaftarSTTBScreen: {
            screen: DaftarSTTBScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        LiveChatScreen: {
            screen: LiveChatScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        }
    },
    {
        initialRouteName: 'HomeScreen',
        headerMode: 'screen',
        headerBackTitle: null,
        headerLeft: null,
    },
);

// Stack Navigator
const StackTracking = createStackNavigator(
    {
        TrackingOrderScreen: {
            screen: TrackingOrderScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        TrackingOrderDetailScreen: {
            screen: TrackingOrderDetailScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
    },
    {
        initialRouteName: 'TrackingOrderScreen',
        headerMode: 'screen',
        headerBackTitle: null,
        headerLeft: null,
    },
);

const TabMenu = createBottomTabNavigator(
    {
        Beranda: {
            screen: StackHome,
            navigationOptions: ({ navigation }) => ({
                tabBarIcon: ({ focused }) => (
                    focused ? <IconHomeSelected /> : <IconHomeNotSelected />
                ),
                tabBarLabel: ({ focused }) => (
                    <TabLabel text={"Home"} focused={focused} />
                ),
            })
        },
        TrackingOrder: {
            screen: StackTracking,
            navigationOptions: ({ navigation }) => ({
                tabBarIcon: ({ focused }) => (
                    focused ? <IconTrackerSelected /> : <IconTrackerNotSelected />
                ),
                tabBarLabel: ({ focused }) => (
                    <TabLabel text={"Tracking Order"} focused={focused} />
                ),
            })
        },
        Profile: {
            screen: ProfileScreen,
            navigationOptions: {
                tabBarIcon: ({ focused }) => (
                    focused ? <IconProfileSelected /> : <IconProfileSelected />
                ),
                tabBarLabel: ({ focused }) => (
                    <TabLabel text={"Profile"} focused={focused} />
                ),
            },
        },
    },
    {
        tabBarPosition: 'bottom',
        tabBarOptions: styleBar.bottomBar.tabBarOptions,
        swipeEnabled: true,
        backBehavior: 'none',
        initialRouteName: 'Beranda',
    },
);

TabMenu.navigationOptions = ({ navigation, screenProps }) => {
    const childOptions = getActiveChildNavigationOptions(navigation, screenProps);
    return {
        title: childOptions.title,
        headerShown: false,
    };
};

const RootStack = createStackNavigator(
    {
        HomeTab: {
            screen: TabMenu,
        },
        MainScreen: {
            screen: MainScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        LoginScreen: {
            screen: LoginScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        InitialScreen: {
            screen: InitialScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        SignatuseScreen: {
            screen: SignatureScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        RequestSTBScreen: {
            screen: RequestSTBScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        SuccessScreen: {
            screen: SuccessScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        AddressScreen: {
            screen: AddressScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
        RequestPickUpScreen: {
            screen: RequestPickUpScreen,
            navigationOptions: {
                headerShown: false,
                gestureEnabled: false,
            },
        },
    },
    {
        initialRouteName: 'InitialScreen',
        mode: 'card',
        headerMode: 'none',
    },
);

// App Container
const AppNavigation = createAppContainer(RootStack);

// Make the AppIndex available to other parts of the application for Navigating between screens
export default AppNavigation;