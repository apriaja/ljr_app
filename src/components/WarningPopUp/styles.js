// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
	...applicationStyles,
	...Fonts,
	fontLabel: {
		...Fonts.robotoBold,
		fontSize: 13,
		letterSpacing: -0.015,
		paddingBottom: 4,
	},
  modalStyle: {
    margin: 10,
    backgroundColor: "#EB5757",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    maxWidth: Metrics.screenWidth - 20
  },
  modalTextStyle: {
    flexWrap: 'wrap', 
    marginLeft: 10, 
    marginRight: 10,
    maxWidth: Metrics.screenWidth / 1.5
  }
});

// Make the styles available for ActivityScreens
export default styles;