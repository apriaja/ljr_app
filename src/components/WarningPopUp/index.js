import React, { useState, useRef } from 'react';
import { TouchableOpacity, Text, Modal, View, Pressable } from 'react-native';

import styles from './styles';
import IconUnlike from '../../assets/images/ico.Unlike.svg';
import IconClose from '../../assets/images/ico.Close.svg';

const WarningPopUp = ({ showWarning, setShowWarning, textWarning }) => {

  return (
    <View style={styles.centered}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={showWarning}
        onRequestClose={() => {
          setShowWarning(false);
        }}
      >
        <View style={[ styles.centered, { flex: 1 }]}>
          <View style={[ styles.modalStyle, styles.flexHorizontal ]}>
            <IconUnlike />
            <Text style={[ styles.modalTextStyle ]}>
              { textWarning }
            </Text>
            <TouchableOpacity onPress={() => setShowWarning(false)}>
              <IconClose />
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </View>
  )

}

export { WarningPopUp };