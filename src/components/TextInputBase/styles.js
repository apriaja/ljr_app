// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    textInputTxt: {
        ...Fonts.robotoRegular,
        fontSize: 15,
        color: Colors.black,
    },
    textInputContainer: {
        width: '100%',
        backgroundColor: Colors.white,
        borderWidth: 1,
        borderRadius: 10,
    },
    multiLineStyle: {
        minHeight: 100,
        maxHeight: 150,
        textAlignVertical: 'top',
        paddingVertical: 10,
    }
});

// Make the styles available for ActivityScreens
export default styles;