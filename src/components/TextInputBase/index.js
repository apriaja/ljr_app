import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { Text, TextInput, TouchableOpacity, View, } from 'react-native';

// Themes and Styles
import styles from './styles';
import { Colors } from '../../themes';

// Assets
// import IconBackButton from '../../assets/images/ico.BackButton.svg'

const TextInputBase = ({ value, placeholder, onChangeText, isPassword, onBlur, error, styleContainer, isMultiline, keyboardType, styleText, image, width }) => {
    const [isPass, setIsPass] = useState(false);
    const [isMulti, setIsMulti] = useState(false);

    useEffect(() => {
        setIsPass(isPassword)
    }, [isPassword]);

    useEffect(() => {
        setIsMulti(isMultiline)
    }, [isMultiline]);

    return (
        <>
            <View style={[styles.textInputContainer, { width: width ? width : "100%",  borderColor: error ? Colors.redSoft : Colors.black, paddingTop: isMulti ? 10 : 0, }, styleContainer]}>
                <TextInput
                    value={value}
                    onChangeText={(value) => onChangeText(value)}
                    secureTextEntry={isPass}
                    style={[styleText ? styleText : styles.textInputTxt, isMulti && styles.multiLineStyle, { paddingVertical: 13, paddingHorizontal: 16, marginRight: image ? 25 : 0 }]}
                    placeholder={placeholder}
                    underlineColorAndroid="transparent"
                    multiline={isMulti}
                    onBlur={(e) => onBlur(e)}
                    keyboardType={keyboardType ? keyboardType : "ascii-capable"}
                />
                {
                    image && <View style={{ position: 'absolute', top: 0, right: 0, bottom: 0, justifyContent: 'center', alignItems: 'center', marginRight: 11 }}>
                        {image}
                    </View>
                }
            </View>
            {
                error && <Text style={styles.errorFont}>{error}</Text>
            }
        </>
    )
}

TextInputBase.propTypes = {
    value: PropTypes.any,
    placeholder: PropTypes.string,
    isPassword: PropTypes.bool,
    isMultiline: PropTypes.bool,
    onChangeText: PropTypes.func,
    onBlur: PropTypes.func,
    error: PropTypes.string,
    styleContainer: PropTypes.object,
    keyboardType: PropTypes.string,
    styleText: PropTypes.object,
    image: PropTypes.any,
    width: PropTypes.string,
};

export { TextInputBase };