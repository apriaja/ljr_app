// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const style = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    radioForm: {
    },
    radioWrap: {
        flexDirection: 'row',
        marginBottom: 5,
    },
    radio: {
        justifyContent: 'center',
        alignItems: 'center',
        width: 30,
        height: 30,
        alignSelf: 'center',
        borderColor: '#2196f3',
        borderRadius: 30,
    },
    radioLabel: {
        paddingLeft: 16,
        // lineHeight: 20,
    },
    radioNormal: {
        borderRadius: 10,
    },
    radioActive: {
        width: 20,
        height: 20,
        backgroundColor: '#2196f3',
    },
    labelWrapStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        alignSelf: 'center'
    },
    labelVerticalWrap: {
        flexDirection: 'column',
        paddingLeft: 10,
    },
    labelVertical: {
        paddingLeft: 0,
    },
    formHorizontal: {
        flexDirection: 'row',
    },
});

// Make the styles available for ActivityScreens
export default style;