import React from 'react';
import { Text } from 'react-native';

import { Colors } from '../../themes';

import styles from './styles';

const TabLabel = props => (
    <Text numberOfLines={1} ellipsizeMode="tail" style={[styles.fontLabel, { color: props.focused ? Colors.DarkModerateBlue : 'rgba(0, 0, 0, 0.55)' }]} >
        {props.text}
    </Text>
);

export { TabLabel };