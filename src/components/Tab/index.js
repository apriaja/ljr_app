import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { FlatList, Text, TouchableOpacity, View, } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconBackButton from '../../assets/images/ico.BackButton.svg'

const Tab = ({ data, onSelectTab, indexToShow }) => {
    const [tabSelected, setTabSelected] = useState({});

    useEffect(() => {
        if (indexToShow) {
            onPressTab(data[indexToShow]);
        } else {
            onPressTab(data[0])
        }
    }, []);

    const onPressTab = (data) => {
        setTabSelected(data)
        onSelectTab(data)
    }

    return (
        <View>
            <FlatList
                contentContainerStyle={{ justifyContent: 'space-between', backgroundColor: '#DEDEDE', borderRadius: 20, padding: 2 }}
                numColumns={data.length}
                data={data}
                renderItem={({ item, index }) => (
                    <TouchableOpacity activeOpacity={1} onPress={() => onPressTab(item)} style={[styles.justifyAlignCenter, item.id === tabSelected.id ? styles.tabSelected : styles.tabNotSelected, { flex: 1 / data.length }]}>
                        <Text style={[item.id === tabSelected.id ? styles.textSelected : styles.textNotSelected]}>{item.title}</Text>
                    </TouchableOpacity>
                )}
                keyExtractor={(item, index) => index.toString()}
            />
        </View>
    )
}

Tab.propTypes = {
    data: PropTypes.array,
    onSelectTab: PropTypes.func,
};

export { Tab };