// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    textSelected: {
        ...Fonts.notoSansBold,
        ...Fonts.description,
        letterSpacing: -0.33,
        color: '#FFFAFA'
    },
    textNotSelected: {
        ...Fonts.notoSansBold,
        ...Fonts.description,
        letterSpacing: -0.33,
        color: '#2C4074'
    },
    tabSelected: {
        backgroundColor: '#2C4074',
        paddingVertical: 10,
        borderRadius: 20
    },
    tabNotSelected: {
        paddingVertical: 10,
        borderRadius: 20
    }
});

// Make the styles available for ActivityScreens
export default styles;