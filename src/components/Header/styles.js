// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    headerContainer: {
        paddingTop: applicationStyles.headerHeight.paddingTop,
        // height: applicationStyles.headerHeight.height,
        // width: Metrics.screenWidth,
        // alignItems: 'center',
        paddingLeft: 15,
        backgroundColor: Colors.white
    },
    titleTxt: {
        ...Fonts.notoSansBold,
        ...Fonts.subContent,
        letterSpacing: -0.33,
        color: Colors.black,
    }
});

// Make the styles available for ActivityScreens
export default styles;