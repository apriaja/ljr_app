import PropTypes from 'prop-types';
import React, { useState, useRef, useEffect } from 'react';
import { Text, TouchableOpacity, View, } from 'react-native';

// Themes and Styles
import styles from './styles';

// Assets
import IconBackButton from '../../assets/images/ico.BackButton.svg'

const Header = ({ title, onBackPress, horizontal }) => {
    const [style1, setStyle1] = useState({});
    const [style2, setStyle2] = useState({ padding: 15, alignSelf: 'baseline' });
    const [textStyle, setTextStyle] = useState({ marginLeft: 10, marginTop: 3 });

    useEffect(() => {
        if (horizontal) {
            setStyle1({ ...styles.flexHorizontal, alignItems: 'center' })
            setStyle2({ paddingLeft: 15, paddingRight: 10, paddingVertical: 15 })
            setTextStyle({})
        }
    }, [horizontal]);

    return (
        <View style={[styles.headerContainer, style1]}>
            <TouchableOpacity activeOpacity={1} onPress={() => onBackPress()} style={[style2]}>
                <IconBackButton />
            </TouchableOpacity>
            <Text style={[styles.titleTxt, textStyle]}>{title}</Text>
        </View>
    )
}

Header.propTypes = {
    title: PropTypes.string,
    horizontal: PropTypes.bool,
    onBackPress: PropTypes.func,
};

export { Header };