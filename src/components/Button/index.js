import PropTypes from 'prop-types';
import React, { useState, useRef } from 'react';
import { Text, TouchableOpacity, View, } from 'react-native';
import _ from 'lodash'

// Themes and Styles
import styles from './styles';

// Assets
import IconBackButton from '../../assets/images/ico.BackButton.svg'
import { Colors } from '../../themes';

const Button = ({ width, text, onPress, containerStyle, disabled, btnImage, textStyle }) => {
    let bgColor = disabled == null ? '#2C4074' : disabled ? Colors.gray : '#2C4074'
    return (
        <TouchableOpacity disabled={disabled == null ? false : disabled} activeOpacity={1} onPress={() => onPress()}
            style={[styles.justifyAlignCenter, styles.flexHorizontal, styles.btnContainer, { backgroundColor: bgColor, width: width }, !width && styles.padding, containerStyle]}
        >
            {
                btnImage && <View style={{ marginRight: 10 }}>
                    {btnImage}
                </View>
            }
            <Text style={[styles.titleTxt, !_.isEmpty(textStyle) && textStyle]}>{text}</Text>
        </TouchableOpacity>
    )
}

Button.propTypes = {
    width: PropTypes.any,
    text: PropTypes.string,
    onPress: PropTypes.func,
    disabled: PropTypes.bool,
    containerStyle: PropTypes.object,
    textStyle: PropTypes.object,
    btnImage: PropTypes.any,
};

export { Button };