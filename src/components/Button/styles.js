// Import Libraries
import { StyleSheet } from 'react-native';

// Themes
import { ApplicationStyles, Fonts, Colors, Metrics } from '../../themes';
import Utils, { isTableted } from '../../common/utils'


// Create Stylesheet
const { ...applicationStyles } = ApplicationStyles;
const styles = StyleSheet.create({
    ...applicationStyles,
    ...Fonts,
    padding: {
        paddingLeft: 20,
        paddingRight: 20,
        alignSelf: 'baseline',
    },
    btnContainer: {
        // backgroundColor: '#2C4074',
        paddingVertical: 12,
        borderRadius: 8,
    },
    titleTxt: {
        ...Fonts.robotoBold,
        fontSize: 13,
        letterSpacing: 0.04,
        color: Colors.white,
    }
});

// Make the styles available for ActivityScreens
export default styles;