const Colors = {
  white: '#FFFFFF',
  black: '#000000',
  green: '#4CAF50',
  yellow: '#FFDD00',
  purple: '#B41E8E',
  gray: '#E0E0E0',
  DarkModerateBlue: "#487596",
  redSoft: '#D63C66',
  StatusTxtColor: {
    "ARRIVAL_ORIGIN": '#1D671B',
    "DONE": '#1D671B',
    "ON_PROCESS": '#367BF5',
    "WAITING_LIST": '#367BF5',
    "ON_THE_WAY": '#367BF5',
    "FAILED": '#F81010'
  }
};

// Make the Colors available for the application
export { Colors };
