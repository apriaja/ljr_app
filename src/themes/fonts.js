import { isTableted } from "../common/utils";

// Font Sizes
const size = {
  superHeadline: isTableted ? 62 : 56,
  headline: isTableted ? 42 : 36,
  midHeadline: isTableted ? 38 : 32,
  subHeadline: isTableted ? 32 : 26,
  title: isTableted ? 30 : 24,
  subTitle: isTableted ? 28 : 22,
  content: isTableted ? 26 : 20,
  subContent: isTableted ? 24 : 18,
  description: isTableted ? 22 : 16,
  subDescription: isTableted ? 20 : 14,
  tag: isTableted ? 18 : 12,
  subTag: isTableted ? 16 : 10,
  small: isTableted ? 14 : 8
};

const fontFamily = {
  robotoRegular: 'Roboto-Regular',
  robotoBold: 'Roboto-Bold',
  robotoMediumItalic: 'Roboto-MediumItalic',
  notoSans: 'NotoSans',
  notoSansBold: 'NotoSans-Bold',
  poppinsSemBold: 'Poppins-SemiBold',
  poppinsMedium: 'Poppins-Medium',
  soraSemiBold: 'Sora-SemiBold',
  soraMedium: 'Sora-Medium',
  soraRegular: 'Sora-Regular',
  soraBold: 'Sora-Bold',
}

// Font Size Classes
const Fonts = {
  robotoRegular: {
    fontFamily: fontFamily.robotoRegular
  },
  robotoBold: {
    fontFamily: fontFamily.robotoBold
  },
  robotoMediumItalic: {
    fontFamily: fontFamily.robotoMediumItalic
  },
  notoSans: {
    fontFamily: fontFamily.notoSans
  },
  notoSansBold: {
    fontFamily: fontFamily.notoSansBold
  },
  poppinsSemBold: {
    fontFamily: fontFamily.poppinsSemBold
  },
  poppinsMedium: {
    fontFamily: fontFamily.poppinsMedium
  },
  soraMedium: {
    fontFamily: fontFamily.soraMedium
  },
  soraSemiBold: {
    fontFamily: fontFamily.soraSemiBold
  },
  soraRegular: {
    fontFamily: fontFamily.soraRegular
  },
  soraBold: {
    fontFamily: fontFamily.soraBold
  },
  superHeadline: {
    fontSize: size.superHeadline
  },
  headline: {
    fontSize: size.headline
  },
  midHeadline: {
    fontSize: size.midHeadline
  },
  subHeadline: {
    fontSize: size.subHeadline
  },
  title: {
    fontSize: size.title
  },
  subTitle: {
    fontSize: size.subTitle
  },
  content: {
    fontSize: size.content
  },
  subContent: {
    fontSize: size.subContent
  },
  description: {
    fontSize: size.description
  },
  subDescription: {
    fontSize: size.subDescription
  },
  tag: {
    fontSize: size.tag
  },
  subTag: {
    fontSize: size.subTag
  },
  small: {
    fontSize: size.small
  }
};

// Make the Fonts available for the application
export { Fonts };
